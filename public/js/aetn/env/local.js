var network = null;
//var domain = 'localplayer.aetndigital.com';
var domain = 'local.appletv.aetndigital.com';
if( aetn.group === 'history' ) {
  network = 'HISTORY';
  aetn.globalContext = {
    brand : 'HISTORY and H2',
    watchlist : {
      queuekey: 'history_watchlist',
      progresskey: 'history_progresslist',
      siteName: 'history'
    },
    adobePass :  {
      localStorageKey : 'history_uuid',
      endPoint: 'https://api.auth-staging.adobe.com',
      requestor_id: 'HISTORY',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'qa2.history.com/activate',
      auth_retry_max: 40
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '64dd4e8fa3bf2515d1e43acfaf49e3f0',
      debug: true,
      activityMonitorKey: '64524e36b96b6f547b3e9dce101bf8bd'
    },
    omniture : {
      brand: 'History',
      suiteIds : 'aetnappletvhistorydev,aetnwatchglobaldev',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'HIS'
    },
    decisioningData: {
      adconfig: "staging"
    },
    mvpdHashConfigUrl : 'http://public-content-aetn.s3.amazonaws.com/watch-app/dev/mvpd_config_fyi.json',
    mpxFeedPath : 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles/episode/history',
    urlPath: 'http://' + domain +  '/history'
  };

} else if( aetn.group === 'ae' ) {

  network = 'AETV';
  aetn.globalContext = {
    brand : 'A&E',
    watchlist : {
      queuekey: 'aetv_watchlist',
      progresskey: 'aetv_progresslist',
      siteName: 'ae'
    },
    adobePass :  {
      localStorageKey : 'aetv_uuid',
      endPoint: 'https://api.auth-staging.adobe.com',
      requestor_id: 'AETV',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'qa2.aetv.com/activate',
      auth_retry_max: 40
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '42a1ef88dfa8c2cab31be18ec3a2691a',
      debug: true,
      activityMonitorKey: '64524e36b96b6f547b3e9dce101bf8bd'
    },
    omniture : {
      brand : 'A&E',
      suiteIds : 'aetnappletvaedev,aetnwatchglobaldev',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'AETV'
    },
    decisioningData: {
      adconfig: "staging"
    },
    mvpdHashConfigUrl : 'http://public-content-aetn.s3.amazonaws.com/watch-app/dev/mvpd_config_ae.json',
    //mvpdHashConfigUrl: 'https://watchapprokuaemvpduser:Whtu8NSB@mobile-a.akamaihd.net/configs/watchapp/Roku/AE/mvpd_config.json',
    mpxFeedPath : 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles2/episode/ae',
    urlPath: 'http://' + domain +  '/ae'
  };

} else if( aetn.group === 'mlt' ) {

  network = 'MLT';
  aetn.globalContext = {
    brand : 'Lifetime',
    watchlist : {
      queuekey: 'mlt_watchlist',
      progresskey: 'mlt_progresslist',
      siteName: 'mlt'
    },
    inProcessVideosList : {
      key: 'mlt_inprocessvideo'
    },
    adobePass : {
      localStorageKey: 'mlt_uuid',
      endPoint: 'https://api.auth-staging.adobe.com',
      requestor_id: 'LIFETIME',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'qawww.mylifetime.com/activate',
      auth_retry_max: 40
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '47d6d65de637a1f71b71e4cbb74b7545',
      debug: true,
      activityMonitorKey: '64524e36b96b6f547b3e9dce101bf8bd'
    },
    omniture : {
      brand : 'Lifetime',
      suiteIds : 'aetnappletvlifetimedev,aetnwatchglobaldev',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'MYL'
    },
    decisioningData: {
      adconfig: "staging"
    },
    mvpdHashConfigUrl : 'http://public-content-aetn.s3.amazonaws.com/watch-app/dev/mvpd_config_mlt.json',
    //mvpdHashConfigUrl : 'https://watchapprokumltmvpduser:DYB4mvtL@mobile-a.akamaihd.net/configs/watchapp/Roku/MLT/mvpd_config.json',
    mpxFeedPath: 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles/episode/mlt',
    urlPath: 'http://' + domain +  '/mlt'
  };

} else if( aetn.group === 'fyi' ) {

  network = 'FYI';
  aetn.globalContext = {
    brand : 'FYI',
    watchlist : {
      queuekey: 'aetv_watchlist',
      progresskey: 'aetv_progresslist',
      siteName: 'fyi'
    },
    adobePass :  {
      localStorageKey : 'fyi_uuid',
      endPoint: 'https://api.auth-staging.adobe.com',
      requestor_id: 'FYI',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'www.fyi.tv/activate',
      auth_retry_max: 40
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: 'c660aa95f92995258cd2e11daf5234f4',
      debug: true,
      activityMonitorKey: '64524e36b96b6f547b3e9dce101bf8bd'
    },
    omniture : {
      brand : 'FYI',
      suiteIds : 'aetnappletvfyidev,aetnwatchglobaldev',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'FYI'
    },
    decisioningData: {
      adconfig: "staging"
    },
    mvpdHashConfigUrl : 'http://public-content-aetn.s3.amazonaws.com/watch-app/dev/mvpd_config_fyi.json',
    mpxFeedPath : 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles/episode/fyi',
    urlPath: 'http://' + domain +  '/fyi'
  };

}
aetn.globalContext.tpTokenAuthUrl = 'https://servicesaetn-a.akamaihd.net/jservice/video/components/get-signed-signature';
aetn.globalContext.visitorIDKey = 'visitorID';
aetn.globalContext.krux = {
                            disable: false,
                            server: 'http://beacon.krxd.net/'
                          };
