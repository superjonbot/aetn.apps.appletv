'use strict';

var seeking = false;
var lastTime = -1;
var currentBreak = null;
var transportControlsVisible;
var playerState = null;
var breakPlayed = false;

if ( atv.player ) {

  atv.player.willStartPlaying = function() {
    if (atv.sessionStorage.getItem('hasInterstitials')) {
      var loadedStream = new mDialog.ATVPlayer.loadedStream();
      TextViewController.initiateView( 'counter' );
      TextViewController.hideView( 'counter', 0 );
        
      lastTime = -1;
      if (loadedStream) {
        loadedStream.startFeature();
      }
    }
   
  };

  atv.player.playerStateChanged = function(newState, timeIntervalSec) {
    require(['aetn/appletv/analytics/videotracking'],
      function(VideoTracker){
        playerState = newState;
      
        switch (newState) {
        
        case atv.player.states.FastForwarding:
          seeking = true;
          break;
        case atv.player.states.Rewinding:
          seeking = true;
          break;
        case atv.player.states.Loading:

          /*
          Check nearestPreviousAdBreak on atv.player.states.Loading, 
          if the break has not been consumed, snapback to the 
          startTime of the Adbreak
          */

          if (seeking === true) {
            breakPlayed = false;

            if (!atv.sessionStorage.getItem('hasInterstitials')) {
              var vt = new VideoTracker();
              vt.seekUpdate(mDialog.ATVPlayer.streamTimeWithoutAds(timeIntervalSec), atv.sessionStorage.getItem('lastTime'));
            }else {

              var nearestPreviousAdBreak = mDialog.ATVPlayer.nearestPreviousAdBreak(timeIntervalSec);
              //save fast forward to point
              if (!nearestPreviousAdBreak['consumed']) {
                atv.sessionStorage.setItem('FastForwardTo', timeIntervalSec);
                atv.player.playerSeekToTime( nearestPreviousAdBreak['startTime'] );
                atv.sessionStorage.setItem('BeforeSeekPosition', atv.sessionStorage.getItem('lastTime'));
              }
            }
          }
          break;
        
        case atv.player.states.Playing:
          seeking = false;
          break;
        case atv.player.states.Stopped:
          aetnUtils.kruxHeartbeats = 0;
          break;
        }
      });
  };


  atv.player.didStopPlaying = function(){
    atv.localStorage.setItem('noProgressList', 'false');
    require(['aetn/appletv/ui/watchlist'],
      function(WatchList){
        var watchList = new WatchList('com.aetn.videoplayer', 'progress');
        watchList.updateVideoInWatchlist(atv.sessionStorage.getItem('currentVideo'),  atv.sessionStorage.getItem('lastTime'));
        watchList.saveShowVideoPlayedInfo(atv.sessionStorage.getItem('currentVideo'), atv.sessionStorage.getItem('lastTime'));
      });
    if (atv.sessionStorage.getItem('hasInterstitials')) {
      mDialog.ATVPlayer.didStopPlaying();
    }
    if ( TextViewController.getView( 'counter' ) ) {
      TextViewController.removeView( 'counter' );
    }

    //Send back to the page where video play was originated
    var gotoUrl = atv.sessionStorage.getItem('SignInReferrerUrl');
    if (gotoUrl) {
      atv.sessionStorage.removeItem('SignInReferrerUrl');
      atv.loadURL(gotoUrl);
    }
  };

  atv.player.playerTimeDidChange = function(timeIntervalSec) {

    require(['aetn/appletv/analytics/videotracking'],
      function(VideoTracker){
        var vt = new VideoTracker();
        var currentTime = Math.floor(timeIntervalSec);

        if (!seeking && currentTime !== lastTime && playerState === 'Playing') {
          lastTime = currentTime;
          currentBreak = null;
          var currentTimeWithNoAd = atv.player.convertGrossToNetTime(currentTime);
          atv.sessionStorage.setItem('lastTime', currentTimeWithNoAd);

          if (atv.sessionStorage.getItem('hasInterstitials')) {
            mDialog.ATVPlayer.timeUpdate (
              currentTime, function(breakInfo, timeRemaining) {
                currentBreak = breakInfo;
                if (timeRemaining === 1) {
                  breakPlayed = true;
                }
                TextViewController.updateMessage( 'ADVERTISEMENT '+ timeRemaining +'s REMAINING' );
                if (currentBreak.startTime === currentTime && TextViewController.visibility() === 'hidden' ) {
                  TextViewController.showView( 'counter', 0 );
                  if (transportControlsVisible === false) {
                    atv.setTimeout( function() {
                      TextViewController.hideView( 'counter', 0 );
                    }, 4000);
                  }
                }
              });

            if ( !currentBreak ) {
              if ( TextViewController.visibility() === 'visible' ) {
                TextViewController.hideView( 'counter', 0 );
              }

              var fastForwardTo = atv.sessionStorage.getItem('FastForwardTo');
              if (fastForwardTo !== null  && breakPlayed === true) {
                atv.player.playerSeekToTime(fastForwardTo);
                vt.seekUpdate(fastForwardTo, atv.sessionStorage.getItem('BeforeSeekPosition'));

                atv.sessionStorage.removeItem('FastForwardTo');
                atv.sessionStorage.removeItem('BeforeSeekPosition');
              }else {
                vt.timeUpdate(currentTimeWithNoAd);
              }
            }
          } else {
             vt.timeUpdate(currentTimeWithNoAd);
             //quit video at 2 min if it is in retail mode
             if ( atv.localStorage.getItem('mvpd') === 'isInRetailDemoMode') {
                if (currentTime >= 120) {
                  atv.player.stop();
                }
             }
          }
        }
      });
  };

  atv.player.playerShouldHandleEvent = function(event, timeIntervalSec) {
    var allowEvent = true;
    switch (event) {

    case atv.player.events.FFwd:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.Rew:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.SkipFwd:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.SkipBack:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;

    }
    return allowEvent;
  };

  var currentPlaylistPosition = 0;

  /**
 * This function is called whenever more assets are required by the player. The implementation
 * should call callback.success or callback.failure at least once as a result. This function
 * will not be called again until the invocation from the last call invokes callback.success
 * or callback.failure. Also, this function will not be called again for a playback if callback.success
 * is called with null argument, or callback.failure is called.
 * Calling any of the callback functions more than once during the function execution has no effect.
 */
  atv.player.loadMoreAssets = function(callback) {

    console.log(" == Load more assets has been called == " );
    //set current playlist position, and clear playlist streams from storage
    currentPlaylistPosition = 0;
    atv.sessionStorage.removeItem("playlistStreams");

    //save eventDatas before retriving other mdialog streams for the new assets since it will override the eventsData
    var currentVideoStream = atv.sessionStorage.getItem("mDialog-loadedStream");
    
    var metadataItem = retrieveMetadataItem( "relatedPlaybackVideos" );

    if (metadataItem instanceof atv.Element ) {
        playlist = metadataItem.getElementsByTagName('httpLiveStreamingVideoAsset');
        console.log("playlist size=", playlist.length);

        var counter = 0;
        var fullURLs = [];
        var interstitialsURLs = [];
        var mdialogStreams = [];

        function getFullMediaUrlForShortformVideo(mediaURL, index) {
          var ajax = new ATVUtils.Ajax({
            url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(mediaURL),
            headers: {},
            success: function(xhr) {
              fullURLs[index] = mediaURL + '&sig=' + xhr.responseText;
              console.log("full valid url = ", fullURLs[index]);
              counter++;                                          
            },
            failure: function(status, xhr) {
              counter++;
              console.log('getSignedSignature Failure Status: ', status, xhr);
              fullURLs[index] = null;
            }
          });
        }

        playlist.forEach(function(item, index){
          var video = {};
          video.thePlatformId = item.getElementByTagName("videoID").textContent;
          video.playURL_HLS = item.getElementByTagName("mediaURL").textContent;
          video.isBehindWall = 'false';

          //get mdialog video stream url
          mDialog.ATVPlayer.loadStreamForKey(
            video.thePlatformId,
            aetnUtils.setupStreamContext(),
                function(loadedStream) {
                  //console.log("loadedStream = ", JSON.stringify(loadedStream));
                  var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
                  //console.log("====successful geting mdialog stream ", manifestURL);
                  interstitialsURLs[index] = loadedStream['interstitialsURL'];
                  mdialogStreams[index] = loadedStream;
                  getFullMediaUrlForShortformVideo(manifestURL, index);

                },
                function() {
                  interstitialsURLs[index] = null;
                  getFullMediaUrlForShortformVideo(video.playURL_HLS, index);
                }
              );
        });

        var t = atv.setInterval(function() {
          
          if (counter == playlist.length) {
              atv.clearInterval(t);
              var docStr = '<?xml version="1.0" encoding="UTF-8"?><playlistAssets>';
              var mdialogStreamArray = [];
          
              //update elements with the new fullURLs
              for (var i = 0; i < playlist.length; i++) {
                if (fullURLs[i]) {
                  docStr += '<httpLiveStreamingVideoAsset>';
                  docStr += '<mediaURL><![CDATA[' + fullURLs[i] + ']]></mediaURL>';
                  docStr += '<title>' + playlist[i].getElementByTagName("title").textContent + '</title>';
                  docStr += '<description>' + playlist[i].getElementByTagName("description").textContent + '</description>';
                  docStr += '<image>' + playlist[i].getElementByTagName("image").textContent + '</image>';
                  if (interstitialsURLs[i]) 
                    docStr += '<eventGroup>' + interstitialsURLs[i] + '</eventGroup>';
                  docStr += '</httpLiveStreamingVideoAsset>';
                  if (mdialogStreams[i])
                    mdialogStreamArray.push(mdialogStreams[i]);
                }
              }
              docStr += '</playlistAssets>'

              var playlistDoc = atv.parseXML(docStr);
              newAssets = playlistDoc.rootElement.getElementsByTagName('httpLiveStreamingVideoAsset');
              console.log("add ", newAssets.length, " more assets to play list ====", newAssets[0].getElementByTagName("mediaURL") );
              //update storage with current video stream
              atv.sessionStorage.setItem("mDialog-loadedStream", currentVideoStream);

              //save the mdialog stream data
              //atv.sessionStorage.setItem("playlistStreams", mdialogStreamArray);
              callback.success(newAssets);

          }
        }, 100);

    } else {
      callback.success(null);
    }

  };

  


  /**
   * In order to access the relatedPlayback document we need it to be
   * somewhere globally accessible. For demonstration I am setting it
   * as a global variable.
   **/
  var currentRelatedPlaybackDocument;

  /**
   * Up Next / Postplay
   * atv.player.loadRelatedPlayback
   *
   * This function is called just before the Up Next (postplay) screen
   * will appear. It provides you the opportunity to set or change the
   * UpNext asset, and/or add additional options for the user to select.
   * These will appear on the postplay screen below the currently playing
   * asset.
   */
  atv.player.loadRelatedPlayback = function(upNextAsset, callback) {
    console.log(" == Load post playback options called with upNextAsset: "+ upNextAsset +" : "+ (upNextAsset instanceof atv.Element) +" == ");

    var relatedPlaybackID = retrieveMetadataItem('relatedPlaybackID');

    console.log( " == Do we have a relatedPlaybackID: "+ relatedPlaybackID +" == " );

    if (relatedPlaybackID instanceof atv.Element) {
      console.log( " == We will be loading the related content from this url: "+ relatedPlaybackID.textContent +" == " );

      var videoId = relatedPlaybackID.textContent;
      console.log("video id = ", videoId);

      var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.urlPath + '/video/json/' + videoId,
          header: {},
          success: function(xhr) {
            //console.log(xhr.responseText);
            var video = JSON.parse(xhr.responseText);

              //&amp; in the player url will cause problem, unescape those
            video.mediaURL = video.mediaURL.replace(/&amp;/g, '&');
            atv.sessionStorage.setItem('currentVideo', video);

            require(['aetn/appletv/analytics/videotracking'], function(VideoTracker){
              var vt = new VideoTracker();
              vt.prepareChapterEvents();
            });

            mDialog.ATVPlayer.loadStreamForKey(
              video.thePlatformId,
              aetnUtils.setupStreamContext(),
              function(loadedStream) {
                var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
                video.interstitialsURL = loadedStream['interstitialsURL'];
                loadFullMediaUrl(video, manifestURL, callback);

                //callback.success(loadFullMediaUrl(video, manifestURL, makeUpDocumentForRelatedPlayback));
              },
              function() {
                loadFullMediaUrl(video, video.playURL_HLS, callback);
                //callback.success(loadFullMediaUrl(video, video.playURL_HLS, makeUpDocumentForRelatedPlayback));
              }
            );

            
          },
          failure: function(status, xhr) {
            console.log('Fail to retrieve video data', status, xhr);
          }
        });


    } else if( upNextAsset instanceof atv.Element ) {
      /**
       * For the UpNext example that uses the loadMoreAssets playlist
       * we do not want to change the current asset, so we pass an empty
       * <relatedPlayback> document to callback.success.
       */
      console.log( " == We have an upNextAsset: "+ upNextAsset.ownerDocument.serializeToString() +" == " );
      callback.success( atv.parseXML( '<atv><body><relatedPlayback></relatedPlayback></body></atv>'));
    } else {
      /**
       * When there are no more asset to be display we pass
       * null to callback.success.
       */
      console.log( " == No relatedPlaybackID, so we're sending null == " );
      callback.success(null);
    }
  };

  function loadFullMediaUrl(video, mediaURL, callback) {
    console.log("===get the full url for media url, then call callback", callback);
    var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(mediaURL),
          headers: {},
          success: function(xhr) {
            console.log("signature is back: ", xhr.responseText);
            video.fullURL = mediaURL + '&sig=' + xhr.responseText;
            if (video.isBehindWall === 'true') {
              aetnUtils.authorizeVideo(video, video.fullURL, function(updatedVideo){
                callback.success(makeUpDocumentForRelatedPlayback(updatedVideo));
              });
            } else  {
              callback.success(makeUpDocumentForRelatedPlayback(video));
            }
          },
          failure: function(status, xhr) {
            console.log('getSignedSignature Failure Status: ', status, xhr);
            callback.success(null);
          }
        });
  }

  function makeUpDocumentForRelatedPlayback(video) {
    
    //console.log("in makeup Document For related playback")
    var document = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
              <body>\
              <relatedPlayback>\
                <upNextItem id="com.aetn.videorelatedplayback">\
                  <httpLiveStreamingVideoAsset id="' + video.thePlatformId + '" indefiniteDuration="false">\
                    <mediaURL><![CDATA[' + video.fullURL + ']]></mediaURL>\
                    <title>' + video.title + '</title>\
                    <description>' + video.description + '</description>\
                    <image>' + video.thumbnailImageURL + '</image>';
    if (video.interstitialsURL) {
      document += '<eventGroup>' + video.interstitialsURL + '</eventGroup>';
    }
    if (video.nextVideoId) {
      document += '<upNextPresentationTime>' + (video.totalVideoDuration/1000 - 30) + '</upNextPresentationTime>\
                    <myMetadata>\
                      <relatedPlaybackID>' + video.nextVideoId + '</relatedPlaybackID>\
                    </myMetadata>';
    }
      document += '</httpLiveStreamingVideoAsset></upNextItem>' +                          
             ' </relatedPlayback>\
            </body>\
            </atv>';
    //console.log(document);
    return atv.parseXML(document);

  }

  function onRelatedPlaybackOptionSelected(id) {
  console.log(" == ITEM WITH ID "+ id +" has been selected.");
}

function loadAssetURL(url) {
  console.log( " == Loading alternate video: "+ url +" == " );
  var ajax = new ATVUtils.Ajax({
    "url": url,
    "success": function(xhr) {
      var xml = xhr.responseXML,
        videoType = atv.sessionStorage.getItem('videoType'),
        videoElement = xml.rootElement.getElementByTagName(videoType);

      console.log( " == We have successfully loaded the XML file: "+ xhr.responseText +" == " );
      console.log( " == Here is our video element: "+ videoElement +" == " );
      atv.player.changeToAsset(videoElement);
    }
  })
}

function retrieveMetadataItem( itemName ) {
  var metadata = atv.player.asset.getElementByTagName('myMetadata'),
    item;
  if( metadata instanceof atv.Element ) {
    item = metadata.getElementByTagName( itemName );
  }
  return item;
}

function customOnSelect(msg, videoType) {
  console.log(" == HANDLING THE UPNEXT ONSELECT FOR TRACKING: "+ msg +" == ");
  var videoElement = currentRelatedPlaybackDocument.rootElement.getElementByTagName(videoType);

  if( videoElement )
  {
    atv.player.changeToAsset(videoElement);
  }
}

// atv.player.currentAssetChanged
// Called when the current asset changes to the next item in a playlist.
atv.player.currentAssetChanged = function() {
  console.log(" == ASSET LENGTH: currentAssetChanged to position: ", currentPlaylistPosition, "== ");
  /*var streams = atv.sessionStorage.getItem("playlistStreams");
  if (streams) {
    console.log("++++++++++++++ Found streams data and ", streams[currentPlaylistPosition]);
    //save stream to storage to prepare for next asset play
    atv.sessionStorage.setItem("mDialog-loadedStream", JSON.stringify(streams[currentPlaylistPosition]));
    console.log("+++++++++++++++++finish saving data=", JSON.stringify(streams[currentPlaylistPosition]));
  }
  currentPlaylistPosition++;*/
}



   /*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be displayed
  */
  
  atv.player.onTransportControlsDisplayed = function( animationDuration ) {
    transportControlsVisible = true;
    if ( TextViewController.getView( 'counter' ) && currentBreak && playerState === 'Playing') {
      TextViewController.showView( 'counter', animationDuration);
    }
  };

  /*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be hidden
  */
  
  atv.player.onTransportControlsHidden = function( animationDuration ) {
    transportControlsVisible = false;
    if ( TextViewController.getView( 'counter' ) && currentBreak && playerState === 'Playing' ) {
      TextViewController.hideView( 'counter', animationDuration );
    }
  };

}

var TextViewController = ( function() {

  var __config = {},
  __views = {};
  
  function SetConfig(property, value) {
    if(property) {
      __config[ property ] = value;
    }
  }
  
  function GetConfig(property) {
    if(property) {
      return __config[ property ];
    } else {
      return false;
    }
  }
  
  function SaveView( name, value ) {
    if( name ) {
      __views[ name ] = value;
    }
  }
  
  function GetView( name ) {
    if(name) {
      return __views[ name ];
    } else {
      return false;
    }
  }
  
  function RemoveView( name ) {
    if( GetView( name ) ) {
      delete __views[ name ];
    }
  }
  
  function HideView( name, timeIntervalSec ) {
    
    var animation = {
      'type': 'BasicAnimation',
      'keyPath': 'opacity',
      'fromValue': 1,
      'toValue': 0,
      'duration': timeIntervalSec,
      'removedOnCompletion': false,
      'fillMode': 'forwards',
      'animationDidStop': function(finished) {
        console.log('Animation Finished:', finished);
      }
    },
    viewContainer = GetView( name );
    
    SetConfig('visibility', 'hidden' );
                      
    if( viewContainer ) {
      viewContainer.addAnimation( animation, name );
    }
  }
  
  function ShowView( name, timeIntervalSec ) {
    
    var animation = {
      'type': 'BasicAnimation',
      'keyPath': 'opacity',
      'fromValue': 0,
      'toValue': 1,
      'duration': timeIntervalSec,
      'removedOnCompletion': false,
      'fillMode': 'forwards',
      'animationDidStop': function(finished) {
        console.log('Animation Finished:', finished);
      }
    },
    viewContainer = GetView( name );
    
    SetConfig('visibility', 'visible' );
                      
    if( viewContainer ) {
      viewContainer.addAnimation( animation, name );
    }
  }
  
  function UpdateMessage( message ) {
    
    var messageView = GetConfig( 'messageView' ),
        seconds = GetConfig( 'numberOfSeconds' );
    
    if(messageView && message) {

      messageView.attributedString = {
        'string': message,
        'attributes': {
          'pointSize': 22.0,
          'color': {
            'red': 1,
            'blue': 1,
            'green': 1
          }
        }
      };

    }

  }
  
  function InitiateView( name ) {

    var viewContainer = new atv.View(),
        message = new atv.TextView(),
        screenFrame = atv.device.screenFrame,
        width = screenFrame.width,
        height = screenFrame.height * 0.07;
    
    // Setup the View container.
    viewContainer.frame = {
      'x': screenFrame.x,
      'y': screenFrame.y + screenFrame.height - height,
      'width': width,
      'height': height
    };
    
    viewContainer.backgroundColor = {
      'red': 0.188,
      'blue': 0.188,
      'green': 0.188,
      'alpha': 0.7
    };
    
    viewContainer.alpha = 1;
    
    var topPadding = viewContainer.frame.height * 0.35,
        horizontalPadding = viewContainer.frame.width * 0.05;
    
    // Setup the message frame
    message.frame = {
      'x': horizontalPadding,
      'y': 0,
      'width': viewContainer.frame.width - (2 * horizontalPadding),
      'height': viewContainer.frame.height - topPadding
    };
    
    // Save the initial number of seconds as 0
    SetConfig( 'numberOfSeconds', 0 );
    
    // Update the overlay message
    //var messageTimer = atv.setInterval( __updateMessage, 1000 );
    //SetConfig( 'messageTimer', messageTimer )
    
    // Save the message to config
    SetConfig('messageView', message );
    SetConfig('animation', 'complete' );
                      
    UpdateMessage();
    
    // Add the sub view
    viewContainer.subviews = [ message ];
    
    // Paint the view on Screen.
    atv.player.overlay = viewContainer;
    
    SaveView( name, viewContainer );
  }
  
  function Visibility() {
    return GetConfig( 'visibility' );
  }
  
  return {
    'initiateView': InitiateView,
    'hideView': HideView,
    'showView': ShowView,
    'saveView': SaveView,
    'getView': GetView,
    'removeView': RemoveView,
    'setConfig': SetConfig,
    'getConfig': GetConfig,
    'updateMessage': UpdateMessage,
    'visibility': Visibility
  };
})();