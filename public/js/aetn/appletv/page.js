"use strict";

atv.onPageLoad = function(pageId) {

  require(['aetn/appletv/ui/watchlist',
           'aetn/appletv/video/detail',
           'aetn/appletv/analytics/omniture',
           'aetn/appletv/analytics/krux',
           'aetn/appletv/ui/progressbar',
           'aetn/appletv/tve/adobepass',
           'aetn/appletv/tve/activate'], function(
            Watchlist,
            VideoDetail,
            Omniture,
            Krux,
            ProgressBar,
            Adobepass
          ) {

      switch(pageId) {

      case 'com.aetn.shows':
        new Omniture({pageName:'Shows'});
        new Krux({type: 'screen', section: 'Featured'});
        break;

      case 'com.aetn.h2shows':
        new Omniture({pageName: 'H2Shows'});
        break;

      case 'com.aetn.topics':
        new Omniture({pageName: 'Topics'});
        break;

      case 'com.aetn.movies':
        new Omniture({pageName: 'Movies'});
        break;

      case 'com.aetn.settings':
        new Omniture({pageName: 'Settings'});
        break;

      case 'com.aetn.activation':
        new Omniture({pageName: 'Signin'});
        break;

      case 'com.aetn.deactivate':
        new Omniture({pageName: 'SignOut'});
        break;

      case 'com.aetn.watchlist':
        new Watchlist(pageId, 'progress');
        new Watchlist(pageId, 'queue');
        new Omniture({pageName: 'Watchlist'});
        break;

      case 'com.aetn.show-navigation':
        new Omniture({
          pageName: 'ShowDetail',
          pageDetail: document.getElementById('com.aetn.show-navigation').getElementByTagName('title').textContent.replace(/[ ']/g, '')
        });
        break;

      case 'com.aetn.season':
        new Omniture({
          pageName: 'SeasonDetail',
          pageDetail: document.getElementById('com.aetn.season').getElementByTagName('title').textContent.replace(/[ ']/g, '') +
                      ':' +
                      document.getElementById('com.aetn.season').getElementByTagName('subtitle').textContent.replace(/[ ']/g, '')
        });
        new Krux({type: 'screen', section: 'Shows' });
        
        //var locks = document.getElementById('com.aetn.season').getElementsByTagName('lock');
        //var mvpd = atv.localStorage.getItem('mvpd');
        //if (mvpd && mvpd.length > 0) {
        //  for (var i = 0; i < locks.length; i++) {
        //    locks[i].removeFromParent();
        //  }
        //}

        var pageElement = document.getElementById('com.aetn.season');

        var showName = pageElement.getElementByTagName('simpleHeader')
                               .getAttribute('accessibilityLabel')
                               .replace(/[ ']/g, '');
        var progressBar = new ProgressBar();
        progressBar. updateShowProgressBars(pageElement);
        break;

      case 'com.aetn.searchResults':
        //Placeholder
        break;

      default:
        if (pageId.search('com.aetn.videodetail') !== -1 ) {
          var videoId = (pageId.split("_"))[1];

          new VideoDetail(pageId, '' + pageId.match('[0-9]+'));
          new Omniture({
            pageName: 'EpisodeDetail',
            pageDetail: (pageId.split("_"))[2] +
                        ":" +
                        document.getElementById(pageId).getElementByTagName('title').textContent.replace(/[ ']/g, '')
          });
       //   new Krux({type: 'screen', section: 'Topics' });

     //     var progressBarEle = document.getElementById(videoId).getElementByTagName('progressBar');
          var progressBar = new ProgressBar();
          progressBar.updateProgressBarOnVideoDetailPage(parseInt(videoId));
        }
        break;
      }
    });
};


atv.onPageBuried = function(pageId) {

  console.log('onPageBuried:', pageId);
  atv._debugDumpControllerStack();

  if( atv.sessionStorage.getItem('stackCtr') !== null ) {
    console.log('onPageBuried if stackCtr');
    atv.sessionStorage.setItem('stackCtr', atv.sessionStorage.getItem('stackCtr') + 1);
  } else {
    console.log('onPageBuried else stackCtr');
    atv.sessionStorage.setItem('stackCtr', 0);
  }

  console.log('onPageBuried stackCtr', atv.sessionStorage.getItem('stackCtr') );
};

atv.onPageExhumed = function(pageId) {

  console.log('onPageExhumed:', pageId);
  atv._debugDumpControllerStack();
  
  if( atv.sessionStorage.getItem('stackCtr') !== null && atv.sessionStorage.getItem('stackCtr') > 0) {
    console.log('onPageExhumed if stackCtr');
    atv.sessionStorage.setItem('stackCtr', atv.sessionStorage.getItem('stackCtr') - 1);
  } else {
    console.log('onPageExhumed else stackCtr');
    atv.sessionStorage.setItem('stackCtr', 0);
  }
  
  console.log('onPageExhumed stackCtr', atv.sessionStorage.getItem('stackCtr') );
  if (atv.sessionStorage.getItem('stackCtr') === 0) {
    console.log('Top of the stack reached');
  }
};
