'use strict';

if( !aetnUtils ) { var aetnUtils = {}; }

aetnUtils.loadURLWithMVPD = function(link) {
  if (atv.localStorage.getItem('mvpd')) {
    link += '/' + atv.localStorage.getItem('mvpd');
  }
  atv.loadURL(link);
};

aetnUtils.loadAndSwapURLWithMVPD = function(link) {
  if (atv.localStorage.getItem('mvpd')) {
    link += '/' + atv.localStorage.getItem('mvpd');
  }
  atv.loadAndSwapURL(link);
};

aetnUtils.loadVideo = function(thePlatformId, isBehindWall, swapCurrentScreen) {
  console.log('loadVideo', thePlatformId, isBehindWall, swapCurrentScreen);
  if (atv.sessionStorage.getItem('SignInReferrer')){
    atv.sessionStorage.removeItem('SignInReferrer');
  }

  if(atv.sessionStorage.getItem('SignInReferrerUrl')) {
    atv.sessionStorage.removeItem('SignInReferrerUrl');
  }
  if (isBehindWall === 'true' && !atv.localStorage.getItem('mvpd')) {
    aetnUtils.initiateSignIn(thePlatformId);
  } else {
    require(['aetn/appletv/video/videoplayer'], function(VideoPlayer){
      new VideoPlayer(thePlatformId, swapCurrentScreen);
    });
  }
};

aetnUtils.loadFeaturedVideo = function(index, thePlatformId, isBehindWall) {
  require(['aetn/appletv/analytics/omniture'],
    function(Omniture){
      new Omniture({
        pageName: 'Feature'
      , position: index
      });
    });
  aetnUtils.loadVideo(thePlatformId, isBehindWall);
};

aetnUtils.initiateSignIn = function(thePlatformId, signInReferrerUrl) {
  atv.sessionStorage.setItem('SignInReferrer', thePlatformId);
  if (!signInReferrerUrl) {
    signInReferrerUrl = aetn.globalContext.urlPath + '/video/' + thePlatformId;
  }
  atv.sessionStorage.setItem('SignInReferrerUrl', signInReferrerUrl);
  atv.loadURL(aetn.globalContext.urlPath + '/tve/activate');
};

aetnUtils.loadFeaturedURL = function(index, url) {
  require(['aetn/appletv/analytics/omniture'],
    function(Omniture){
      new Omniture({
        pageName: 'Feature'
      , position: index
      });
    });
  aetnUtils.loadURLWithMVPD(url);
};

aetnUtils.initialAuthCheck = function() {

  if (atv.device.isInRetailDemoMode) {
    require(['aetn/appletv/analytics/omniture'], function(Omniture) {
      atv.localStorage.setItem('mvpd','isInRetailDemoMode');
      new Omniture({
        pageName: 'Authentication'
      });
    });
    return;
  }

  require(['aetn/appletv/tve/adobepass', 'aetn/appletv/analytics/omniture'],
    function(Adobepass, Omniture) {
      var adobepass = new Adobepass();
      adobepass.getAuthenticationToken(function(token){
        if (token) {
          atv.localStorage.setItem('mvpd',token.mvpd);
          aetnUtils.getMvpdHash(token.mvpd, function(data){
            if (data)
              atv.localStorage.setItem('mvpdHash', data);
          });

          new Omniture({pageName: 'Authentication'});
        }
      }, function(status, xhr) {
        if (atv.localStorage.getItem('mvpd')) {
          atv.localStorage.removeItem('mvpd');
          new Omniture({
            pageName: 'Authentication'
          });
        }
      });
    });
};

aetnUtils.initialWatchlistCheck = function() {

  require(['aetn/appletv/ui/watchlist'], function(Watchlist) {

    if (atv.device.isInRetailDemoMode) {
      atv.loadURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
      return;
    }

    var progressList = new Watchlist('', 'progress');
    var queueList = new Watchlist('', 'queue');

    var hasProgressList = progressList.validateAndUpdateWatchlist();
    var hasQueueList = queueList.validateAndUpdateWatchlist();

    if (hasProgressList || hasQueueList) {
      atv.loadURL(aetn.globalContext.urlPath + '/nav');
    } else {
      atv.loadURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
    }

  });
};

aetnUtils.loadAndSwapNav = function() {

  require(['aetn/appletv/ui/watchlist'], function(Watchlist) {
    var progresslist = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
    var queuelist = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);

    

    if ( (!progresslist || progresslist.length === 0) && (!queuelist || queuelist.length === 0 )) {
      atv.loadAndSwapURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
    } else {
      atv.loadAndSwapURL(aetn.globalContext.urlPath + '/nav');
    }
    console.log('loadAndSwapNav', atv.sessionStorage.getItem('authStateChanged'));
  });
};

//Visitor ID As String
aetnUtils.getVisitorID = function() {
  var visitorID = atv.localStorage.getItem(aetn.globalContext.visitorIDKey);
  if (!visitorID) {
    //implement generating UUID, and save to localStorage.
    var s = [];
    var hexDigits = '0123456789abcdef';
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4';  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23];

    visitorID = s.join('');
    atv.localStorage.setItem(aetn.globalContext.visitorIDKey, visitorID);
  }
  return visitorID;
};

aetnUtils.handleVolatileReload = function (pageID, doc, reloadUrl, seriesName) {

   console.log('sectionVolatileReload', 
    'authStateChanged: ', 
    atv.sessionStorage.getItem('authStateChanged'),
    'videoItemStateChanged: ', 
    atv.sessionStorage.getItem('videoItemStateChanged'));


  switch(pageID) {


    case 'com.aetn.settings' :
        aetnUtils.settingsVolatileReload(doc);
      break;

    case 'com.aetn.topics' :
    case 'com.aetn.movies' :
    case 'com.aetn.shows' :
    case 'com.aetn.h2shows' :
      aetnUtils.sectionVolatileReload(doc);
      break;

    case 'com.aetn.watchlist' :
      aetnUtils.watchlistVolatileReload(reloadUrl);
      break;

    case 'com.aetn.season' :
      aetnUtils.seasonVolatileReload(reloadUrl, doc, seriesName);
      break;
    
    case 'com.aetn.videodetail' :
        //if (atv.sessionStorage.getItem('authStateChanged') === 'true') {          
          aetnUtils.loadAndSwapURLWithMVPD(reloadUrl);
        //}
      break;

    default :
        if (atv.sessionStorage.getItem('authStateChanged') === 'true') {          
          aetnUtils.loadAndSwapURLWithMVPD(reloadUrl);
        }
      break;

  }
  event.cancel();
};

aetnUtils.seasonVolatileReload = function(reloadUrl, doc, seriesName) {
  if (atv.sessionStorage.getItem('videoItemStateChanged') === 'true' || atv.sessionStorage.getItem('authStateChanged') === 'true') {
    var reloadUrlFull;
    var seriesnameKey = seriesName.replace(/[' ]/g, '');
    //console.log("==================seriesname", seriesnameKey);
    var lastVideoForShow = atv.localStorage.getItem(seriesnameKey);
    if (lastVideoForShow) {
      
      var info =  lastVideoForShow.season + '-' + 
                  lastVideoForShow.thePlatformId + '-' +
                  ((lastVideoForShow.finished) ? 'new' : 'current');
      reloadUrlFull = reloadUrl + encodeURIComponent(seriesName) + '/' + info;
      atv.sessionStorage.setItem('videoItemStateChanged', 'false');
      //console.log("before season reload");
      aetnUtils.loadAndSwapURLWithMVPD(reloadUrlFull);
    }
  } 
};

aetnUtils.watchlistVolatileReload = function(reloadUrl) {

  var progressList = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
  var queueList = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);
  var reloadUrlFull = null;

  var hasProgressList = (progressList && progressList.length > 0 && atv.localStorage.getItem('noProgressList') === 'false')? true : false;
  var hasQueueList = (queueList && queueList.length > 0)? true : false;

  if (hasProgressList && hasQueueList) {
    console.log("has both list");
    reloadUrlFull = reloadUrl;
  } else if (hasProgressList) {
    console.log("has progress list");
    reloadUrlFull = reloadUrl + '/queueempty';
  } else if (hasQueueList) {
    console.log("has queue list");
    reloadUrlFull = reloadUrl + '/progressempty';
  } 

  if (reloadUrlFull) {
    if (atv.localStorage.getItem('mvpd')) {
      reloadUrlFull += '/' + atv.localStorage.getItem('mvpd');
    }

    var ajax = new ATVUtils.Ajax({url: reloadUrlFull,
      success: function(xhr){
         return atv.loadAndSwapXML(xhr.responseXML);
      },
      failure: function(){
        console.log("failure retrieve xml");
      }});
  }
}

aetnUtils.sectionVolatileReload = function(doc) {
  console.log('sectionVolatileReload', atv.sessionStorage.getItem('authStateChanged'));

  if (atv.sessionStorage.getItem('authStateChanged') !== 'true') {
      event.cancel();
      return;
  } 
  
  var img = null;
  if (atv.localStorage.getItem('mvpd')) {

    var mvpd = atv.localStorage.getItem('mvpd');
    
    img = doc.getElementById("brand_logo");

    var newLogo720 = img.getAttribute("src720").replace('logo@',  mvpd + '@');
    var newLogo1080 = img.getAttribute("src1080").replace('logo@',  mvpd + '@');

    img.setAttribute("src720", newLogo720);
    img.setAttribute("src1080", newLogo1080);

  } else {

    doc.getElementById("brand_logo").setAttribute("src720", aetn.globalContext.urlPath + 'logo@720.png');
    doc.getElementById("brand_logo").setAttribute("src1080", aetn.globalContext.urlPath + 'logo@1080.png');

  }

  atv.sessionStorage.setItem('authStateChanged', 'false');
};

aetnUtils.settingsVolatileReload = function(doc) {

  console.log('settingsVolatileReload', atv.sessionStorage.getItem('authStateChanged'));

  if (atv.sessionStorage.getItem('authStateChanged') !== 'true') {
      event.cancel();
      return;
  }

  var activation = null,
      label = null,
      text = null;

  if (atv.localStorage.getItem('mvpd')) {
    var mvpd = atv.localStorage.getItem('mvpd');

    activation = doc.getElementById("activation");
    if (activation) {
      label = activation.getElementByName("label");
      if (label && mvpd) {
        text = 'Sign Out';
        label.textContent = text;
      }
      activation.setAttribute("accessibilityLabel", "Sign out");
      activation.setAttribute("onSelect", "atv.loadURL(aetn.globalContext.urlPath + '/tve/deactivate');");
    }
  } else {

    activation = doc.getElementById("activation");
    if (activation) {
      label = activation.getElementByName("label");
      if (label) {
        text = 'Sign in to Provider';
        label.textContent = text;
      }
      activation.setAttribute("accessibilityLabel", "Sign in to Provider");
      activation.setAttribute("onSelect", "atv.loadURL(aetn.globalContext.urlPath + '/tve/activate');");
    }
  }

  atv.sessionStorage.setItem('authStateChanged', 'false');

};

aetnUtils.getMvpdHash = function(mvpdId, cb) {

  var ajax = new ATVUtils.Ajax({url: aetn.globalContext.mvpdHashConfigUrl,
      success: function(xhr){
        var hashCode = null;
        var configList = JSON.parse(xhr.responseText);
        for (var i = 0; i < configList.mvpdWhitelist.length; i++) {
            if (configList.mvpdWhitelist[i].mvpd === mvpdId) {
              foundProvider = true;
              hashCode = (configList.mvpdWhitelist[i].freewheelKeyValues)? configList.mvpdWhitelist[i].freewheelKeyValues.mvpdHash : null;
              break;
            }
        }
        cb(hashCode);
         
      },
      failure: function(){
        console.log("failure retrieve mvpd hash config file");
        cb(null);
      }});
};

aetnUtils.setupStreamContext = function() {
  var streamContext = {
    trackingData: {
      VISITOR_ID: aetnUtils.getVisitorID()
    }
  };

  var decisioningData = aetn.globalContext.decisioningData;
  var hash = atv.localStorage.getItem('mvpdHash');
  if (hash) {
    if (decisioningData == null)
      decisioningData = {};

      decisioningData._fw_ae = hash;
  }

  if (decisioningData) {
    streamContext.decisioningData = decisioningData;
  }

  return streamContext;

};

aetnUtils.authorizeVideo = function(video, mediaURL, cb) {
  require(['aetn/appletv/tve/adobepass'], function(Adobepass){
      var adobepass = new Adobepass();
      adobepass.getAuthorization(video,
          function(resource) {
            adobepass.getShortMediaToken(video,
              function(shortToken) {
                shortToken = encodeURIComponent(ATVUtils.Base64.decode(shortToken));
                //fullURL += '&auth=' + shortToken;
                video.fullURL = mediaURL + '&auth=' + shortToken;
                cb(video);
              },
              function(message) {
                var xml = aetnUtils.drawFailureMessage(message);
                atv.loadAndSwapXML(xml);
              }
            );
          },
          function(message) {
            var xml = aetnUtils.drawFailureMessage(message);
            atv.loadAndSwapXML(xml);
          }
      );
  });
};

       

aetnUtils.drawFailureMessage = function(message) {

        var errorXML = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
                <body>\
                  <dialog id="com.aetn.error">\
                    <title>Error</title>\
                    <description><![CDATA[ ' + message + ' ]]></description>\
                  </dialog>\
                </body>\
              </atv>';
        return atv.parseXML(errorXML);
};


aetnUtils.kruxHeartbeats = 0;

