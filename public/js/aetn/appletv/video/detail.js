define(['klass','aetn/appletv/ui/watchlist'], function(klass, Watchlist){

  return klass({

    initialize: function(pageId, videoId) {
      console.log('video/detail.js initialized: ', pageId, videoId);

      var watchlist = new Watchlist(pageId);

      var watchlistBtn = document.getElementById('add');
      var title = watchlistBtn.getElementByTagName('title');

      if ( watchlist.getVideoFromWatchlist(videoId) ) {
        title.textContent = 'Remove';
      } else {
        title.textContent = 'Add';
      }
    }
    
  });

});