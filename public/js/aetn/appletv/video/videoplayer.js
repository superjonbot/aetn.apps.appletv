'use strict';

define(['klass',
        'aetn/appletv/tve/adobepass',
        'aetn/appletv/ui/watchlist',
        'aetn/appletv/analytics/videotracking',
        'underscore'],

  function (klass,
            Adobepass,
            WatchList,
            VideoTracker) {

    return klass({
      initialize: function (videoId, swapCurrentScreen) {

        var self = this;
        this.proxy = new atv.ProxyDocument();
        this.proxy.onCancel = function() {
          if (ajax) {
            ajax.cancelRequest();
          }
        };

        if (swapCurrentScreen) {
          this.swapCurrentScreen = swapCurrentScreen;
        } else {
          this.swapCurrentScreen = swapCurrentScreen;
          this.proxy.show();
        }

        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.urlPath + '/video/json/' + videoId,
          header: {},
          success: function(xhr) {
            var video = JSON.parse(xhr.responseText);

              //&amp; in the player url will cause problem, unescape those
            video.mediaURL = video.mediaURL.replace(/&amp;/g, '&');
            atv.sessionStorage.setItem('currentVideo', video);
            self.loadVideoStream();
          },
          failure: function(status, xhr) {
            console.log('Fail to retrieve video data', status, xhr);
          }
        });
      },
      
      loadVideoStream: function () {
        
        var self = this;

        this.watchList = new WatchList('com.aetn.video-player', 'progress');
        this.video = atv.sessionStorage.getItem('currentVideo');
        
        
        var streamContext = aetnUtils.setupStreamContext();

        //console.log("streamContext=", JSON.stringify(streamContext.decisioningData));

        var vt = new VideoTracker();
        vt.prepareChapterEvents();

        if (atv.device.isInRetailDemoMode) {
          this.loadStreamfromMPX();
        } else {
          mDialog.ATVPlayer.loadStreamForKey(
            self.video.thePlatformId,
            streamContext,
            _.bind(this.loadStreamfromMDialog, self),
            _.bind(this.loadStreamfromMPX, self)
          );
        }

      },

      loadStreamfromMDialog: function (loadedStream) {

        var self = this;
        console.log('Loading from mDialog');

        var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
        this.video.interstitialsURL = loadedStream['interstitialsURL'];
        this.video.mediaURL = manifestURL;
        //load player with full url with signature and adobe token if is pay video (uncomment when the mdialog server is ready)       
        this.loadPlayerWithFullMediaURL(this.video, function(updatedVideo) {
          if (self.swapCurrentScreen) {
            atv.loadAndSwapXML(self.drawPlayer(self.video, updatedVideo.fullURL, updatedVideo.interstitialsURL));
          } else {
            self.proxy.loadXML(self.drawPlayer(self.video, updatedVideo.fullURL, updatedVideo.interstitialsURL));
          }
        });

      },

      loadStreamfromMPX: function(){

        var self = this;
        console.log('Loading from MPX');
        

        this.loadPlayerWithFullMediaURL(this.video, function(updatedVideo) {
          if (self.swapCurrentScreen) {
            atv.loadAndSwapXML(self.drawPlayer(self.video, updatedVideo.fullURL));
          } else {
            self.proxy.loadXML(self.drawPlayer(self.video, updatedVideo.fullURL));
          }
        });
      },

      loadPlayerWithFullMediaURL: function(video, cb) {

        var self = this;
        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(video.mediaURL),
          headers: {},
          success: function(xhr) {
            video.fullURL = video.mediaURL + '&sig=' + xhr.responseText;
            if (video.isBehindWall === 'true') {
              aetnUtils.authorizeVideo(video, video.fullURL, cb);
            } else  {
              cb(video);
            }
          },
          failure: function(status, xhr) {
            console.log('getSignedSignature Failure Status: ', status, xhr);
          }
        });
      },

/*      authorizeVideo: function(video, fullURL, cb) {

        var self = this;
        var adobepass = new Adobepass();
        
        adobepass.getAuthorization(video,
          function(resource) {
            adobepass.getShortMediaToken(video,
              function(shortToken) {
                shortToken = encodeURIComponent(ATVUtils.Base64.decode(shortToken));
                fullURL += '&auth=' + shortToken;
                cb(fullURL);
              },
              function(message) {
                var xml = self.drawFailureMessage(message);
                atv.loadAndSwapXML(xml);
              }
            );
          },
          function(message) {
            var xml = self.drawFailureMessage(message);
            atv.loadAndSwapXML(xml);
          }
        );
      },

      drawFailureMessage: function(message) {

        var errorXML = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
                <body>\
                  <dialog id="com.aetn.error">\
                    <title>Error</title>\
                    <description><![CDATA[ ' + message + ' ]]></description>\
                  </dialog>\
                </body>\
              </atv>';
        return atv.parseXML(errorXML);
      },*/

      drawPlayer: function(video, mediaURL, interstitialsURL) {

        var savedVideo = this.watchList.getVideoFromWatchlist(video.thePlatformId);
        var thumbnail = video.modalImageURL ? video.modalImageURL : video.thumbnailImageURL;

        var player = '<?xml version="1.0" encoding="UTF-8"?>\
            <atv>\
              <body>\
                <videoPlayer id="com.aetn.video-player">\
                  <httpLiveStreamingVideoAsset id="' +  video.thePlatformId +'" indefiniteDuration="false">\
                    <mediaURL><![CDATA[' + mediaURL + ']]></mediaURL>\
                    <title><![CDATA[' + video.title + ']]></title>\
                    <description><![CDATA[' + video.description + ']]></description>\
                    <image>' + thumbnail + '</image>';

        if (interstitialsURL) {
          player += '<eventGroup>' + interstitialsURL + '</eventGroup>';
          if (savedVideo && savedVideo.stopTime > 10 && savedVideo.finished === 'false') {
            player += '<bookmarkNetTime>' + savedVideo.stopTime + '</bookmarkNetTime>';
          }
        } else {
          if (savedVideo && savedVideo.stopTime > 10 && savedVideo.finished === 'false') {
            player += '<bookmarkTime>' + savedVideo.stopTime + '</bookmarkTime>';
          }
        }

        /*if (video.longForm === 'true') {
            if (video.nextVideoId) {
              var upnextTime = video.totalVideoDuration/1000 - 30;
              player += '<upNextPresentationTime>' + upnextTime + '</upNextPresentationTime>';
              player += '<myMetadata>\
                          <relatedPlaybackID>' + video.nextVideoId + '</relatedPlaybackID>\
                        </myMetadata>';
            }
        } else {
            if (video.remainingVideos) {
              player += '<myMetadata><relatedPlaybackVideos>';
              for (var i = 0; i < video.remainingVideos.length; i++) {
                player += '<httpLiveStreamingVideoAsset id="PLAYLISTVIDE' + (i + 1) + '">';
                player += '<mediaURL><![CDATA[' + video.remainingVideos[i].playURL_HLS + ']]></mediaURL>';
                player += '<videoID>' + video.remainingVideos[i].id + '</videoID>';
                player += '<title>' + video.remainingVideos[i].title + '</title>';
                player += '<description>' + video.remainingVideos[i].description + '</description>';
                player += '<image>' + video.remainingVideos[i].image + '</image>';
                player += '</httpLiveStreamingVideoAsset>';
              }
              player += '</relatedPlaybackVideos></myMetadata>';
            }
        }*/
            
        player += '\
                  </httpLiveStreamingVideoAsset> \
                </videoPlayer>\
              </body>\
            </atv>';
        if (interstitialsURL) {
          atv.sessionStorage.setItem('hasInterstitials', true);
        } else {
          atv.sessionStorage.setItem('hasInterstitials', false);
        }

//console.log(player);

        return atv.parseXML(player);
      }
    });
  });