"use strict";

define(['klass', 'aetn/appletv/ui/watchlist'], function (klass, Watchlist) {
  return klass({
	initialize: function() {
	},

	updateShowProgressBars: function( parentElement ) {

	  var episodes = parentElement.getElementsByTagName("twoLineEnhancedMenuItem");
	  var totalPercentage = 0;
      var missingBars = 0;
      var locked;

	  if (episodes) {
		console.log('Updating progress bars');
		var watchlist = new Watchlist('', 'progress');

		for (var i = 0; i < episodes.length; i += 1) {
			console.log("i =", i);
		  var episode = episodes[i];
		  var accessories = episode.getElementByName('accessories');

		  if( accessories ) {
		  	locked = accessories.getElementByName('lock');
		  }
		
		  console.log("before update individual bar");
		  totalPercentage += this.updateProgressBar(watchlist, episode, accessories, locked);
		}
	      
	   /* if (totalPercentage <= 0) {
	        console.log("No watchlist recorded - removing progress bars");
	        for (var i = 0; i < episodes.length; i += 1) {
	            var episode = episodes[i];
	            var accessories = episode.getElementByName("accessories");
	            var progressBar = accessories.getElementByName("progressBar");
	            if (progressBar) {
	              progressBar.removeFromParent();
	            }
	        }
	    } else if (missingBars > 0) {
	        console.log("Progress bars were removed, but history was recorded - re-adding progress bars");
	        for (var i = 0; i < episodes.length; i += 1) {
	          var episode = episodes[i];
	          var accessories = episode.getElementByName("accessories");
	          var progressBar = accessories.getElementByName("progressBar");
	          if (!progressBar) {
	            progressBar = document.makeElementNamed("progressBar");
		        accessories.appendChild(progressBar);
				totalPercentage += this.updateProgressBar(watchlist, episode, accessories, progressBar);
	          }
	        }
	    }*/
	  }
	},


	updateProgressBar: function(watchlist, episode, accessories, locked) {
	  // parse the video id from the element id
	  var videoId = parseInt(episode.getAttribute('id').substring('menu_item_'.length), 10);
	  var percentage = 0;
		
	  if (videoId && accessories) {
			console.log("Video '" + videoId + "', check progress bar");

			var video = watchlist.getVideoFromWatchlist(videoId);
			if (video) {
				console.log("Video " + videoId + " in watch list");

				var time = (video.stopTime) ? video.stopTime : null;
				var duration = (video.duration) ? video.duration : null;

				if (!time || !duration) {
					console.log("Video duration or progress unknown, show unplayed progress bar");
				} else {
					console.log("Video - progress: " + time + ", duration: " + duration);
	                percentage = Math.min(Math.floor((100*time) / duration), 100);
	                
	                if ( !locked ) {
	                	var pbar = document.makeElementNamed("progressBar");
	                	pbar.setAttribute( "percentage", percentage );
		        		accessories.appendChild(pbar);
		        	}

	                	//progressBar.setAttribute("percentage", percentage);
	                 /*else {
	                	progressBar = document.makeElementNamed("progressBar");
	                    progressBar.setAttribute("percentage", percentage);
	    				accessories.appendChild(progressBar);
	                }*/
	                
				}

			} else {
				// set progress bar to empty
				console.log("Video " + videoId + " NOT in watchlist");
				
			}
	  }
	  return percentage;
	},

	updateProgressBarOnVideoDetailPage: function(videoId) {
		var watchlist = new Watchlist('', 'progress');
		var percentage = 0;
		var progressBar, row;
		var rows = document.getElementById("video_detail_rows");

		var video = watchlist.getVideoFromWatchlist(videoId);
		if (video) {
		    console.log("Video " + videoId + " in watch list");

			var time = (video.stopTime) ? video.stopTime : null;
			var duration = (video.duration) ? video.duration : null;

			if (!time || !duration) {
		   	  console.log("Video duration or progress unknown, show unplayed progress bar");
			} else {
			  console.log("Video - progress: " + time + ", duration: " + duration);
	          percentage = Math.min(Math.floor((100*time) / duration), 100);

				if ( rows ) {
					row = document.makeElementNamed("row");
					progressBar = document.makeElementNamed("progressBar");
					progressBar.setAttribute( "percentage", percentage );
					row.appendChild ( progressBar );
					rows.appendChild(row);
				}

	          if (progressBar) {
	            progressBar.setAttribute("percentage", percentage);
	          } 
	                
			}

		} else {
			// set progress bar to empty
			console.log("Video " + videoId + " NOT in watchlist");
		}

	}

  });
});