define(['klass','underscore'], function(klass){

  return klass({
    
    watchlistKey : null,
    listElement : null,

    initialize: function(pageId, listType) {
      this.listType = listType;
      this.watchlistKey = (listType === 'progress')? aetn.globalContext.watchlist.progresskey : aetn.globalContext.watchlist.queuekey;
      this.listElement = (listType === 'progress')? 'inprogress' : 'queue';
      if (pageId === 'com.aetn.watchlist') {
        this.renderUI((listType === 'progress')? true : false);
      }
    },

    renderUI: function(isProgressList){
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      var queue = document.getElementById(this.listElement);

      if (!this.checkIfWatchlistEmpty(watchlist, isProgressList)) {
        var oldQueue = queue.getElementsByTagName('sixteenByNinePoster');
        _.each(oldQueue, function(element){
          element.removeFromParent();
        });

        watchlist.reverse();

        for(var i = 0; i < watchlist.length; i++) {
          if (this.watchlistKey === aetn.globalContext.watchlist.progresskey && watchlist[i].finished === 'true')
            continue;

          var newChild = document.makeElementNamed('sixteenByNinePoster');
          newChild.setAttribute('id',  i );
          newChild.setAttribute('alwaysShowTitles', 'true');
          newChild.setAttribute('onSelect', 'aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + "/video/" + ' + watchlist[i].id + ')');
          newChild.setAttribute('onPlay', 'aetnUtils.loadVideo("' + watchlist[i].id + '", "' + watchlist[i].isBehindWall + '")');

          var titleElement = document.makeElementNamed('title');
          titleElement.textContent = (watchlist[i].seriesName)? watchlist[i].seriesName : '';
          newChild.appendChild(titleElement);
          
          var subtitleElement = document.makeElementNamed('subtitle');
          subtitleElement.textContent = watchlist[i].title;
          newChild.appendChild(subtitleElement);

          var image = document.makeElementNamed('image');
          image.setAttribute('src720', watchlist[i].image);
          image.setAttribute('src1080', watchlist[i].image);
          newChild.appendChild(image);
          queue.appendChild(newChild);
        }
      }
    },

    handleWatchlistButtonClicked: function(videoTitle, thePlatformId, image, seriesName, isBehindWall) {

      try {
        var button = document.getElementById('add');
        var title = button.getElementByTagName('title');
      
        if (title.textContent === 'Add') {
          this.addVideoToWatchlist(thePlatformId, {
            title: videoTitle
          , id: thePlatformId
          , image: image
          , seriesName: seriesName
          , isBehindWall: isBehindWall
          , finished : 'false'
          });
          title.textContent = 'Remove';
          button.setAttribute('accessibilityLabel', 'Remove from watch list');
        } else {
          this.removeVideoFromWatchlist(thePlatformId);
          title.textContent = 'Add';
          button.setAttribute('accessibilityLabel', 'Add to watch list');
        }
      
      } catch(error){
        console.log('Caught exception trying to toggle DOM element: ' + error);
      }
    },

    getVideoFromWatchlist: function(thePlatformId) {
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      if (watchlist) {
        return _.findWhere(watchlist, {
          id : thePlatformId
        });
      }
      return null;
    },

    updateVideoInWatchlist: function(video, stopTime) {
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      if (watchlist === null || watchlist === undefined) {
        watchlist = [];
      }

      var matchedVideo = _.findWhere(watchlist, {
        id : video.thePlatformId
      });

      if (matchedVideo) {
        var matchedVideoIndex = _.indexOf(watchlist, matchedVideo);
        if (matchedVideoIndex > -1) {
          watchlist.splice(matchedVideoIndex, 1);
        }
      }

      var durationInMin = Math.round(video.totalVideoDuration / 60000);
      var saveBookmark = true;

      if (durationInMin < 20 && (video.totalVideoDuration/1000 - stopTime) < 15) {
        saveBookmark = false;
      } else if ((durationInMin <= 65 && durationInMin > 20) && video.totalVideoDuration/1000 - stopTime < 120) {
        saveBookmark = false;
      } else if (durationInMin > 65 && video.totalVideoDuration/1000 - stopTime < 360) {
        saveBookmark = false;
      }

      //this.checkAndSaveIfStateChanged();
      watchlist.push({
          id: video.thePlatformId
        , title: video.title
        , image: video.stillImageURL
        , seriesName: video.seriesName
        , isBehindWall: video.isBehindWall
        , stopTime: stopTime
        , finished: (saveBookmark) ? 'false' : 'true'
        , duration: video.totalVideoDuration / 1000
      });

      atv.localStorage.setItem(this.watchlistKey, watchlist);
      this.checkAndSaveIfStateChanged();
    },

    addVideoToWatchlist: function(thePlatformId, value) {
      this.checkAndSaveIfStateChanged();
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      
      if (watchlist === null || watchlist === undefined) {
        watchlist = [];
      }

      if (value !== null && value !== undefined) {
        watchlist.push(value);
      } else {
        console.log('Caught exception trying to toggle DOM element');
        return false;
      }

      atv.localStorage.setItem(this.watchlistKey, watchlist);
    },

    removeVideoFromWatchlist: function(thePlatformId){
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      
      var matchedVideo = _.findWhere(watchlist, {id : thePlatformId} );
      var matchedVideoIndex = _.indexOf(watchlist, matchedVideo);

      if (matchedVideoIndex > -1) {
        watchlist.splice(matchedVideoIndex, 1);
      }

      atv.localStorage.removeItem(this.watchlistKey);
      atv.localStorage.setItem(this.watchlistKey, watchlist);
      this.checkAndSaveIfStateChanged();
    },

    checkAndSaveIfStateChanged: function(actionType) {
      //after removing an item, if the lists are empty, state changed.
      var progressList = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
      var queueList = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);

      if ( this.checkIfWatchlistEmpty(progressList, true) && this.checkIfWatchlistEmpty(queueList, false)) {
        atv.sessionStorage.setItem( 'navbarStateChanged', 'true');
      } 
    },

    checkIfWatchlistEmpty: function(list, isContinueWatch) {
      if (list === null || list.length === 0)
        return true;
      
      if (isContinueWatch) {
        for (var i = 0; i < list.length; i++) {
          if (list[i].finished === 'false') {
            atv.localStorage.setItem('noProgressList', 'false');
            return false;
          }
        }
        atv.localStorage.setItem('noProgressList', 'true');
        return true;
      }
      
      return false;
    },

    validateAndUpdateWatchlist: function() {
      var self = this;
      var list = atv.localStorage.getItem(self.watchlistKey);
      var finishedList = [];
    
      if (list && list.length > 0) {
        var ids = '';
        counter = 0;
        for (var i = 0; i < list.length; i++) {
          if (this.listType === 'progress' || this.listType !== 'progress') {
            if (!list[i].finished || list[i].finished === 'false') {
              if (counter > 0)  ids += ',';
              ids += list[i].id;
              counter = counter + 1;
            } else {
              finishedList.push(list[i]);
            }
            
          }
        }

        console.log("validateAndUpdateWatchlist", ids);

        if (!ids)
          return false;

        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.mpxFeedPath + '?title_id=' + ids,
          headers: {},
          success: function(xhr) {
            var validVideoJson = JSON.parse(xhr.responseText);
            var validVideos = validVideoJson.Items;

            //if there are less validVideos than what's on the watchlist, remove
            if (validVideos.length < list.length) {
              var validVideoIds = [];
              for (var j = 0; j < validVideos.length; j++) {
                validVideoIds.push(validVideos[j].thePlatformId);
              }

              var updateList = [];
              for (var k = 0; k < list.length; k++) {
                if (validVideoIds.indexOf(list[k].id) > -1) {
                  updateList.push(list[k]);
                }
              }

              updateList = updateList.concat(finishedList);
              //console.log("updated ", self.watchlistKey, "=", updateList.length);
              atv.localStorage.setItem(self.watchlistKey, updateList);
            }
          },
          failure: function(status, xhr){
            console.log("Cannot get response for", aetn.globalContext.mpxFeedPath + ids, status, xhr);
          }
        });
        return true;
      } else {
        return false;
      }

    },

    //if a video is a show episode, save it's season and episode number, and if the video is complete information
    saveShowVideoPlayedInfo: function(video, endTime) {
      if (video.tvNtvMix === 'Show' && video.chapters && video.chapters.length > 0) {
        var finished = false;
        var duration = video.totalVideoDuration / 1000;
        if ((duration - endTime)/(duration) < 0.02) {
          finished = true;
        }
        var season = (video.season)? video.season : '0';
        var lastVideoData = {
          season: season
        , thePlatformId: video.thePlatformId
        , finished: finished
        };        

        //if (!atv.localStorage.getItem(video.seriesNameForAnalytics) || finished) {
          atv.sessionStorage.setItem('videoItemStateChanged', 'true');
        //} 

        atv.localStorage.setItem(video.seriesNameForAnalytics, lastVideoData);

      }
    }

  });

});