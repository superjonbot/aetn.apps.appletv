"use strict";

define(['klass'], function (klass) {

  return klass({

    initialize: function () {
    },

    handleNavigate: function (event, pageId) {

      function indexForNavItemWithId (navItems, navId) {

        var currentIndex = 0;
        navItems.forEach( function( item, index ) {
          if( navId === item.getAttribute( 'id' ) ) {
            currentIndex = index;
          }
        });

        return currentIndex;
      }

      event.onCancel = function() {
        // declare an onCancel handler to handle cleanup if the user presses the menu button before the page loads.
        ajax.cancelRequest();
      };

      // The navigation item ID is passed in through the event parameter.
      var navId = event.navigationItemId,
          //Use the event.navigationItemId to retrieve the appropriate URL information. 
          //This can be retrieved from the document navigation item.
          navigationItems = document.evaluateXPath( '//navigationItem', document ),
          navigationItem = document.getElementById( navId ),
          docUrl = navigationItem.getElementByTagName( 'url' ).textContent,
          currentIndex = indexForNavItemWithId( navigationItems, navId );
      

      // Set the current index so that on a volatile reload we can reset the index.
      atv.sessionStorage.setItem( pageId.toUpperCase() + "CURRENTINDEX", currentIndex );


      if(navigationItem.getElementByTagName( 'title' ).textContent === 'Watchlist') {
        var progresskey = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
        var queuekey = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);
        
        if ( (!progresskey || progresskey.length === 0 || atv.localStorage.getItem('noProgressList') === 'true') &&
             (!queuekey || queuekey.length === 0 )
           ) {
          docUrl = docUrl + '/empty';
        } else if ( !progresskey || progresskey.length === 0 || atv.localStorage.getItem('noProgressList') === 'true') {

          docUrl = docUrl + '/progressempty';
        } else if (!queuekey || queuekey.length === 0) {
          docUrl = docUrl + '/queueempty';
        }
      }

      if (atv.localStorage.getItem('mvpd')) {
        docUrl += '/' + atv.localStorage.getItem('mvpd');
      }
      
      // Request the XML document via URL and send any headers you need to here.
      var ajax = new ATVUtils.Ajax({
        url: docUrl,
        success: function( xhr ){

          // After successfully retrieving the document you can manipulate the document
          // before sending it to the navigation bar.
          var doc = xhr.responseXML;

          // Once the document is ready to load pass it to the event.success function
          event.success( doc );
        },
        failure: function(status, xhr) {
          // If the document fails to load pass an error message to the event.failure button
          event.failure( 'Navigation failed to load.' );
        }
      });
    },

    handleVolatileReload: function (event, doc, pageId) {

      if (atv.sessionStorage.getItem('navbarStateChanged') !== 'true') {
        event.cancel();
      } else {
        var navigation = doc.rootElement.getElementByTagName( 'navigation' ),
            currentIndex = atv.sessionStorage.getItem( pageId.toUpperCase() + 'CURRENTINDEX' ) || 0;

        navigation.setAttribute( 'currentIndex', currentIndex );

        atv.sessionStorage.setItem( 'navbarStateChanged', false );

        //if state changed, find "watchlist" nav item, remove it. If doesn't exist, find 'search' nav item, insert watchlist before it
        var watchlistItem = doc.getElementById('watchlist');
        if (watchlistItem) {
          navigation.setAttribute( 'currentIndex', 0);
          watchlistItem.removeFromParent();
        } else {
          var searchItem = doc.getElementById('search');
          watchlistItem = document.makeElementNamed('navigationItem');
          watchlistItem.setAttribute('id', 'watchlist');

          var title = document.makeElementNamed('title');
          title.textContent = 'Watchlist';

          var url = document.makeElementNamed('url');
          url.textContent = aetn.globalContext.urlPath + '/watchlist';
          watchlistItem.appendChild(title);
          watchlistItem.appendChild(url);
          navigation.insertChildBefore(watchlistItem, searchItem);
        }
        atv.loadAndSwapXML( doc );
      }
    }
  });
});