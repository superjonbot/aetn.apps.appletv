'use strict';

define(['klass' , 'aetn/appletv/analytics/omniture', 'aetn/appletv/analytics/krux'], function (klass, Omniture, Krux) {

  return klass({

    prepareChapterEvents: function() {

      var video = atv.sessionStorage.getItem('currentVideo');

      var chapters = video.chapters;
      var trackingEvents = {};

      if (chapters.length === 0) {

        trackingEvents['1'] = {trackings: ['Video View', 'Video Start'], chapter: 'None', videoData: video};
        trackingEvents[Math.floor(video.totalVideoDuration/1000 - 2)] = {trackings: ['Video Complete'], chapter: 'None', videoData: video};

      } else {

        for (var i = 0; i < chapters.length; i++) {

          var timeInSecond = chapters[i];
          var trackings = ['Video View', 'Video Start'];

          if ( i > 0 ) {
            //end of last chapter event
            trackingEvents[(timeInSecond - 3).toString()] = {trackings: ['Video Complete'], chapter: 'Chapter ' + i, videoData: video};
          } else {
            trackings.push('Episode Start');
          }

          trackingEvents[(timeInSecond + 2).toString()] = {trackings: trackings, chapter: 'Chapter ' + (i + 1), videoData: video};
        }

        //tracking data for episode complete
        var timepoint = Math.floor(video.totalVideoDuration/1000 * 0.98);
        trackingEvents[timepoint.toString()] = {
          trackings : ['Episode Complete', 'Video Complete'],
          chapter : 'Chapter ' + (i + 1),
          videoData : video
        };

      }

      atv.sessionStorage.setItem('currentTracking', trackingEvents);
    },

    timeUpdate: function (playerTime) {

      var chapterTracker = atv.sessionStorage.getItem('currentTracking');
  
      if (chapterTracker && chapterTracker[playerTime]){
        if (chapterTracker[playerTime].trackings){
          new Omniture({
            pageName: 'VideoPlayer',
            events : chapterTracker[playerTime].trackings,
            chapter : chapterTracker[playerTime].chapter,
            videoDetail : chapterTracker[playerTime].videoData
          });
          new Krux({
            type: 'event',
            events: chapterTracker[playerTime].trackings,
            chapter : chapterTracker[playerTime].chapter,
            videoDetail : chapterTracker[playerTime].videoData
          })
        }
      }

      /** KRUX HEARTBEAT **/
      aetnUtils.kruxHeartbeats++;

      if ( aetnUtils.kruxHeartbeats === 10 ) {
        if ( chapterTracker ) {
          var prevChapter = chapterTracker['1'];
          for ( chapter in chapterTracker ) {
            if ( parseInt( chapter ) > playerTime ) {
              new Krux({
                type: 'heartbeat',
                events: [],
                chapter : chapterTracker[prevChapter].chapter,
                videoDetail : chapterTracker[prevChapter].videoData
              });
              break;
            }
            prevChapter = chapter;
          }
        }
        aetnUtils.kruxHeartbeats = 0;
      }

    },

    seekUpdate: function (playerTime, lastPlayTime) {

      //find out playertime is between which time frame in the tracking event array

      var video = atv.sessionStorage.getItem('currentVideo');
      var chapters = video.chapters;

      if (chapters.length > 0) {

        var lastChapter = 0,
            currentChapter = 0;

        for (var i = 0; i < chapters.length; i++) {

          var currentChapterPoint = (i < chapters.length - 1)? parseInt(chapters[i + 1], 10) : Math.floor(video.totalVideoDuration);
          var lastChapterPoint = parseInt(chapters[i], 10);

          if (lastPlayTime > lastChapterPoint && lastPlayTime < currentChapterPoint) {
            lastChapter = i + 1;
          }

          if (playerTime > lastChapterPoint && playerTime < currentChapterPoint) {
            currentChapter = i + 1;
          }

        }

        if (currentChapter > lastChapter) {
          new Omniture({
            pageName : 'VideoPlayer',
            events : ['Video View'],
            chapter : 'Chapter ' + currentChapter,
            videoDetail : video
          });
         
        }
      }

    }
    
  });

});



