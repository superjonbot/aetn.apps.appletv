"use strict";
define(['klass','underscore'], function(klass){

  return klass({

    kruxURL: aetn.globalContext.krux.server,

    initialize: function( callOptions ) {
      if ( aetn.globalContext.krux.disable )
        return;

      switch ( callOptions.type ) {
        case 'screen' :
          this.doScreenCall( callOptions );
          break;
        case 'event' :
          this.doEventCall( callOptions );
          break;
        case 'heartbeat' :
          this.doEventCall( callOptions, 'heartbeat.gif?' );
          break;
      }

    },

    doScreenCall: function( callOptions ) {
      var url = this.kruxURL + 'pixel.gif?';
      var isLaunched = atv.sessionStorage.getItem( aetn.globalContext.omniture.brand + '_launched' );
      var header = document.rootElement.getElementByTagName('header');

      if ( !callOptions )
        return;
      if( callOptions.section.toLowerCase() === 'featured' && isLaunched )
        return;

      switch ( callOptions.section.toLowerCase() ) {
        case 'shows' :
          if ( header && header.getAttribute('id') ) {
            if ( header.getAttribute('id').split('_')[0] === 'H2') 
              callOptions.section = 'H2Shows';
          }
          callOptions.pageLevel2 = encodeURIComponent ( header.getElementByTagName('title').textContent );
          break;
        case 'topics' :
          if ( document.rootElement.getElementByTagName('itemDetail') )
            callOptions.pageLevel2 = encodeURIComponent ( document.rootElement.getElementByTagName('itemDetail').getElementByTagName('title').textContent );
          break;
      }
     

      var ctx = this.buildCommonParams();
      ctx._kcp_sc = callOptions.section;
      ctx._kpa_page_level_1 = callOptions.section;

      if ( callOptions.showName ) {
        ctx._kpa_show_name = callOptions.showName;
      }

      if ( callOptions.pageLevel2 ) {
        ctx._kpa_page_level_2 = callOptions.pageLevel2;
      }

      this.sendRequest ( url, ctx, function( success ) {
      if ( success )
        atv.sessionStorage.setItem( aetn.globalContext.omniture.brand + '_launched', true );
      } );

    },

    doEventCall: function( callOptions, _src ) {
      var src = _src ? _src : 'event.gif?';
      var url = this.kruxURL + src;
      var event = '';

      if ( !callOptions)
        return;

      if ( callOptions.events.indexOf ( 'Video Start' ) > -1 ) {
        event = 'start';
      }
      else if ( callOptions.events.indexOf ( 'Video Complete' ) > -1 ) {
        event = 'end';
      }

      var ctx = this.buildCommonParams();

      ctx.event_id = ( event === '') ? '' : ( event === 'start' ? 'JLn4isGR' : 'JLn4ukRD' );
      ctx._kpa_video_analytics_series_name = aetn.globalContext.omniture.brandPrefix + ":" + callOptions.videoDetail.omnitureTag;
      ctx._kpa_video_analytics_clip_title = ctx._kpa_video_analytics_series_name + ':' + callOptions.videoDetail.title.replace(/ /g, '');
      
      ctx._kpa_video_analytics_lf_vs_sf = (callOptions.videoDetail.longForm === 'true') ? 'Longform' : 'Shortform';
      ctx._kpa_video_season =  (callOptions.videoDetail.season) ? callOptions.videoDetail.season : 'None';
      ctx._kpa_video_series_type = callOptions.videoDetail.tvNtvMix;
      ctx._kpa_video_chapter = callOptions.chapter;
      ctx._kpa_video_episode = (callOptions.videoDetail.episode) ? callOptions.videoDetail.episode : 'None';
      ctx._kpa_video_PPLID = (callOptions.videoDetail.programID) ? callOptions.videoDetail.programID: 'None';

      if ( callOptions.videoDetail.totalVideoDuration )
        ctx._kpa_video_duration = Math.round ( callOptions.videoDetail.totalVideoDuration / ( 1000 * 60 ) );

      if ( callOptions.videoDetail.originalAirDate ) {
        var today = new Date();
        var airdate = new Date( callOptions.videoDetail.originalAirDate )
        ctx._kpa_video_analytics_days_since_premiere = Math.floor ( new Date( today - airdate ) / ( 1000 * 60 * 60 * 24 ) );
      }
     
      this.sendRequest ( url, ctx );
     
    },

    buildCommonParams: function() {
      var ctx = {};
      var mvpd = atv.localStorage.getItem('mvpd');

      ctx._kcp_d = aetn.globalContext.omniture.brand + '_AppleTV';
      ctx._kcp_s = ctx._kcp_d;
      ctx._kuid = aetnUtils.getVisitorID();
      ctx._kpid = "7156d277-5d35-4c9c-8fb7-f454c47dbfe1";

      if ( mvpd ) {
        ctx._kpa_is_tve_authenticated = 1;
        ctx._kpa_tve_provider_name = mvpd;
      }
      else {
        ctx._kpa_is_tve_authenticated = 0;
      }

      return ctx;
    },

    sendRequest: function( url, ctx, cb ) {
      var getUrl = url;

      for (var key in ctx) {
        getUrl += encodeURIComponent(key) + '=' + encodeURIComponent(ctx[key]) + '&';
      }

      var ajax = new ATVUtils.Ajax({
        url: getUrl,
        headers: {},
        success: function(xhr) {
          console.log('** Successfully sent Krux request **');
          if ( cb ) cb ( true );
        },
        failure: function(status, xhr) {
          console.log('***Error in Krux request: ', status, xhr);
        }
      });
    }

    

  });
});