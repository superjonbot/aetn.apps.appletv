"use strict";
define(['klass','underscore'], function(klass){

  return klass({

    /*
    trackingOptions = {
      pageName : 'aaa',
      events : [a,b,c],
      pageDetails :  ''
      };
    */
    initialize: function(trackingOptions) {
      //omniture tracking will be off if in retail mode
      if (atv.localStorage.getItem('mvpd') === 'isInRetailDemoMode')
        return;

      var s = OmnitureMeasurement(aetn.globalContext.omniture.suiteIds);

      s.trackingServer = aetn.globalContext.omniture.server;
      s.trackingServerSecure = aetn.globalContext.omniture.secureServer;
      s.ssl = false;
      s.debugTracking = false;

      var context = {};

      context.platform = "AppleTV";
      context.brand = aetn.globalContext.omniture.brand;

      if (trackingOptions.watch) {
        _.extend(context, trackingOptions.watch);
      }

      if (trackingOptions.events) {
        s = this.setupEvents(trackingOptions.events, s);
      }

      switch (trackingOptions.pageName) {

      case 'SignIn':
        s = this.pageView(s, trackingOptions.pageName)
        context.authenticationstate = '0';
        context.cableprovidername = 'None';
        break;

      case 'Authentication':
        s = this.pageView(s, '');
        var mvpd = atv.localStorage.getItem('mvpd');
        if (mvpd) {
          context.authenticationstate = '1';
          context.cableprovidername = mvpd;
        } else {
          context.authenticationstate = '0';
          context.cableprovidername = 'None';
        }
        break;

      case 'Feature' :
        context.aetnanalyticstappedindex = trackingOptions.position;
        s = this.pageView(s, '');
        break;

      case 'VideoPlayer' :
          
        s = this.pageView(s, '');
        context.videocalltype = 'ContentView';
        context.videochapter = trackingOptions.chapter;

        if (trackingOptions.videoDetail) {
          context.videocategory = aetn.globalContext.omniture.brandPrefix + ":" + trackingOptions.videoDetail.omnitureTag;
          context.videocliptitle = context.videocategory + ':' + trackingOptions.videoDetail.title.replace(/ /g, '');
          context.videolf = (trackingOptions.videoDetail.longForm === 'true')? 'Longform' : 'Shortform';
          context.videotv = trackingOptions.videoDetail.tvNtvMix; //todo: make sure topic video is also 'TV'                        
          context.videoseason = (trackingOptions.videoDetail.season)? trackingOptions.videoDetail.season : 'None';
          context.videoepisode = (trackingOptions.videoDetail.episode)? trackingOptions.videoDetail.episode : 'None';
          context.videosubcategory = 'None';
          context.videofastfollow = (context.videolf === 'Longform')? ((trackingOptions.videoDetail.isFastFollow === 'true')?'FastFollow':'Archive') : 'Shortform';
          context.videoautoplay = 'Autoplay';
          context.videoduration = trackingOptions.videoDetail.duration;
          context.videofullscreen = 'Fullscreen';
          context.videoadvertiser = 'None'; //what value?
          context.videoadduration = 'None'; //what value?
          context.videoadtitle = 'None';  //what value?
          context.videorequiresauthentication = (trackingOptions.videoDetail.isBehindWall === 'true')? '1' : '0';
          context.PPLID = (trackingOptions.videoDetail.programID)? trackingOptions.videoDetail.programID: 'None';
        }
        break;

      default:
        s = this.pageView(s, trackingOptions.pageName, trackingOptions.pageDetail);
        break;
      }
      sendRequest(s, context);
    },

    pageView: function(s, pageName, pageDetail) {
      if (!pageDetail) {
        s.pagename = pageName;
      }else if (pageDetail) {
        s.pagename = pageName + ":" + pageDetail;
      }
      return s;
    },

    setupEvents: function(eventNames, s) {

      var eventNumberStr = '';
      for (var i = 0; i < eventNames.length; i++) {
        var eventNumber = this.getEventNumber(eventNames[i]);
        if (eventNumber) {
          eventNumberStr += (eventNumberStr.length > 0)? ',' : '';
          eventNumberStr += "event" + eventNumber;
        } else {
          console.log("Event Number doesn't exist for : " + eventNames[i]);
        }
      }
      s.events = eventNumberStr;
      return s;
    },

    getEventNumber: function(eventName) {

      var eventNumberMap = {
        "Video View": 21,
        "Video Start": 22,
        "Video Complete": 26,
        "Episode Start": 27,
        "Episode Complete": 28,
        "Internal Search": 7,
        "TV Everywhere Authentication Start": 72,
        "TV Everywhere Authentication Complete": 73,
        "TV Everywhere Authentication Error": 76
      };

      if (eventNumberMap[eventName]) {
        return eventNumberMap[eventName];
      } else {
        return null;
      }
    }

  });
});