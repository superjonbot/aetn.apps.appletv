define(['klass'], function(klass){

  return klass({
    
    initialize: function() {

    },

    deactivateDevice: function(cb) {

      var url  = aetn.globalContext.adobePass.endPoint + '/api/v1/logout?deviceId=' + aetnUtils.getVisitorID() + "&resource=" + aetn.globalContext.adobePass.requestor_id;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'DELETE', aetn.globalContext.adobePass.requestor_id, '/api/v1/logout');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "method": "DELETE",
        "headers": headers,
        "success": function(xhr) {
          console.log('deactivateDevice Success Status: ', xhr);
          cb();
        },
        "failure": function(status, xhr) {
          console.log('deactivateDevice Failure Status: ', status, xhr);
        }
      });

    },

    getAuthenticationToken: function(cbSuccess, cbFailure) {

      var url  = aetn.globalContext.adobePass.endPoint + '/api/v1/tokens/authn?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId='+ aetnUtils.getVisitorID();
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/tokens/authn');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function(xhr) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess( response );
        },
        "failure": function(status, xhr) {
          console.log('getAuthenticationToken Failure Status: ', status, xhr);
          cbFailure(status, xhr);
        }
      });

    },

    getAuthenticationTokenByCode: function(code, tryCount, cbSuccess, cbFailure) {
      var self = this;
      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/authenticate/' + code + '.json?requestor=' + aetn.globalContext.adobePass.requestor_id;

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers" :  {},
        "body" : {},
        "success": function( xhr ) {
          console.log('getAuthenticationTokenByCode success using', code, xhr.responseText);
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response);
        },
        "failure": function( status, xhr ) {
          console.log('getAuthenticationTokenByCode Failure Status:', status);
          if (tryCount < aetn.globalContext.adobePass.auth_retry_max) {
            console.log('retry count: ', tryCount);
            tryCount = tryCount + 1;
            atv.setTimeout(self.getAuthenticationTokenByCode.bind(self), 15000, code, tryCount, cbSuccess, cbFailure);
          }else {
            console.log('getAuthenticationTokenByCode Failure Status: Timeout');
            cbFailure("The Activation Process Timeout.");
          }
        }
      });
    },

    getAuthorization: function( video, cbSuccess, cbFailure ) {

      var resource = '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/"><channel><title>' + video.network.replace('A&E', 'AETV').toUpperCase() + '</title><item><title>' + video.title + '</title><guid>' + video.programID + '</guid></item></channel></rss>';
      resource = encodeURI( resource );

      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/authorize?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId=' + aetnUtils.getVisitorID() + '&resource=' + resource;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/authorize');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response.resource);
        },
        "failure": function( status, xhr ) {
          console.log("failure getting authorization", status, xhr.responseText);
          if (cbFailure) {
            try {
              var response = JSON.parse(xhr.responseText);
              if(response.details && response.details !== '' && response.details !== 'network') {
                cbFailure(response.details);
              } else {
                cbFailure('You are not authorized to see the videos');
              }
            } catch(e) {
              cbFailure('You are not authorized to see the videos');
            }
          }
        }
      });

    },

    getShortMediaToken: function( video, cbSuccess, cbFailure ) {

      var resource = '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/"><channel><title>' + video.network.replace('A&E', 'AETV').toUpperCase() + '</title><item><title>' + video.title + '</title><guid>' + video.programID + '</guid></item></channel></rss>';
      resource = encodeURI( resource );
      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/mediatoken?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId=' + aetnUtils.getVisitorID() + '&resource=' + resource;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/mediatoken');

      var headers = {
        'Authorization' : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response.serializedToken);
        },
        "failure": function( status, xhr ) {
          try {
            var response = JSON.parse(xhr.responseText);
            if(response.details && response.details !== '' && response.details !== 'network') {
              cbFailure(response.details);
            } else {
              cbFailure('We were unable to contact A+E Networks, please try again.');
            }
          } catch(e) {
            cbFailure('We were unable to contact A+E Networks, please try again.');
          }
        }
      });

    },

    getNewRegistrationCode: function(cb) {

      var url     = aetn.globalContext.adobePass.endPoint + '/reggie/v1/' + aetn.globalContext.adobePass.requestor_id + '/regcode';
      var payload = 'format=json&ttl=36000&deviceId=' + aetnUtils.getVisitorID();
      var auth    = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'POST', aetn.globalContext.adobePass.requestor_id, '/reggie/v1/' + aetn.globalContext.adobePass.requestor_id + '/regcode');
      var headers = {
        'Authorization' : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "method": "POST",
        "data": payload,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          console.log('getNewRegistrationCode: success:', aetnUtils.getVisitorID());
          cb(response.code);
        },
        "failure": function( status, xhr ) {
          console.log('getNewRegistrationCode Request Status:', status, xhr);
        }
      });

    },

    getAuthorizationHeader: function( public_key, private_key, verb, requestor_id, request_uri ) {
      var request_time  = new Date().getTime().toString();
      var nonce         = this.generateUUID();
      var msg           = verb + ' requestor_id=' + requestor_id + ', nonce=' + nonce + ', signature_method=HMAC-SHA1, request_time=' + request_time + ', request_uri=' + request_uri;
      var hash          = CryptoJS.HmacSHA1(msg, private_key);
      return msg + ', public_key=' + public_key + ', signature=' + hash.toString(CryptoJS.enc.Base64);
    },

    generateUUID: function() {

      var uuid = '';
      var o = null;
      for(var i = 1; i <= 32; i++) {
        o = getRandomInt(1,16);
        if (o <= 10) {
          o = o + 47;
        } else {
          o = o + 96 - 10;
        }
        uuid = uuid + String.fromCharCode(o);
      }

      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }

      return uuid;
    }

  });

});