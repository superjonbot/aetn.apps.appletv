"use strict";

define(['klass',
        'aetn/appletv/tve/adobepass',
        'aetn/appletv/analytics/omniture',
        'underscore'],
        function(
          klass,
          Adobepass,
          Omniture) {

  return klass({

    adobepass: null,

    initialize: function(pageId) {
      this.adobepass = new Adobepass();
      if (pageId === 'com.aetn.activate') {
        this.adobepass.getNewRegistrationCode(_.bind(this.drawAccessCodeView, this));
      } else if (pageId === 'com.aetn.deactivate') {
        this.adobepass.deactivateDevice(_.bind(this.deactivateDevice, this));
      }
    },

    drawAccessCodeView: function(code) {

      var screenFrame = atv.device.screenFrame;
      var width = screenFrame.width;
      var height = screenFrame.height;
      var activationTitle = 'Activate ' + aetn.globalContext.brand;
      var instructionLine1 = 'To get access to all ' + aetn.globalContext.brand + ' content, please verify your subscription with your TV provider.';
      var instructionLine2 = 'Go to ' + aetn.globalContext.adobePass.auth_page + ' and enter the code seen below.';
      var instructionLine3 = 'If your provider isn\'t supported, there are still plenty of full episodes and clips to enjoy.';

      //Title

      var textViewTitle = new atv.TextView();

      textViewTitle.attributedString = {
        string: activationTitle,
        attributes: {
          pointSize: 56.0,
          color: { red: 1, blue: 1, green: 1 },
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      textViewTitle.frame = {
        x: 0,
        y: 0 - ( height * 0.10 ),
        width: width,
        height: height
      };

      //Instructions

      var textViewInstructionsLine1 = new atv.TextView();

      textViewInstructionsLine1.attributedString = {
        string: instructionLine1,
        attributes: {
          pointSize: 26.0,
          color: { red: 1, blue: 1, green: 1 },
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      textViewInstructionsLine1.frame = {
        x: ( width / 2 ) - ( width / 3 ),
        y: 0 - ( height * 0.25 ),
        width: width / 1.5,
        height: height
      };

      var textViewInstructionsLine2 = new atv.TextView();

      textViewInstructionsLine2.attributedString = {
        string: instructionLine2,
        attributes: {
          pointSize: 26.0,
          color: { red: 1, blue: 1, green: 1 },
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      var authPageAccessibility = aetn.globalContext.adobePass.auth_page.replace(/qa2|qawww|aetv/gi, function(matched) {
        return matched.split('').join(' ');
      });

      var instructionLine2AccessibilityLabel = 'Go to ' + authPageAccessibility + ' and enter the code seen below.';
      textViewInstructionsLine2.accessibilityLabel = instructionLine2AccessibilityLabel;

      textViewInstructionsLine2.frame = {
        x: ( width / 2 ) - ( width / 3 ),
        y: 0 - ( height * 0.35 ),
        width: width / 1.5,
        height: height
      };

      var textViewInstructionsLine3 = new atv.TextView();

      textViewInstructionsLine3.attributedString = {
        string: instructionLine3,
        attributes: {
          pointSize: 26.0,
          color: { red: 1, blue: 1, green: 1 },
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      textViewInstructionsLine3.frame = {
        x: ( width / 2 ) - ( width / 3 ),
        y: 0 - ( height * 0.45 ),
        width: width / 1.5,
        height: height
      };


      //Label

      var textViewCodeLabel = new atv.TextView();
      textViewCodeLabel.attributedString = {
        string: 'Activation Code',
        attributes: {
          pointSize: 16.0,
          color: { red: 1, blue: 1, green: 1 },
          weight: "heavy",
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      textViewCodeLabel.frame = {
        x: ( width / 2 ) - ( width / 3.4 ),
        y: 0 - ( height * 0.60 ),
        width: width / 1.7,
        height: height
      };

      //Code

      var textViewCode = new atv.TextView();
      textViewCode.attributedString = {
        string: code,
        attributes: {
          pointSize: 40.0,
          color: { red: 1, blue: 1, green: 1 },
          weight: "heavy",
          alignment: "center",
          breakMode: "word-wrap"
        }
      };

      textViewCode.accessibilityLabel = code.split('').join(' ');
      
      textViewCode.frame = {
        x: ( width / 2 ) - ( width / 3.4 ),
        y: 0 - ( height * 0.65 ),
        width: width / 1.7,
        height: height
      };

      var rootView = new atv.View();
      rootView.subviews = [textViewTitle, textViewInstructionsLine1,textViewInstructionsLine2, textViewInstructionsLine3, textViewCodeLabel, textViewCode];
      controller.view = rootView;

      //this.adobepass.getAuthenticationTokenByCode(code, _.bind(this.drawSuccessView, this));

      this.adobepass.getAuthenticationTokenByCode(code, 0, _.bind(this.loadSuccessView, this), _.bind(this.drawFailureView, this));

      new Omniture({
        pageName : 'SignIn',
        events : ['TV Everywhere Authentication Start']
      });
    },

    loadSuccessView: function(data) {
      if (!data.mvpd) {
        return false;
      }
      atv.sessionStorage.setItem('authStateChanged', 'true');
      atv.localStorage.setItem('mvpd', data.mvpd);
      atv.localStorage.setItem('userId', data.userId);
      console.log("set Mvpd Hash code for ", data.mvpd);
      aetnUtils.getMvpdHash(data.mvpd, function(hash){
        if(hash) {
          atv.localStorage.setItem('mvpdHash', hash);
        }

        new Omniture({
          pageName : 'Authentication',
          events : ['TV Everywhere Authentication Complete']
        });

        var signInReferrer = atv.sessionStorage.getItem('SignInReferrer');
        if (signInReferrer) {
          atv.sessionStorage.removeItem('SignInReferrer');
          //update SignInReferrerUrl with mvpd name if it is video url
          if (atv.sessionStorage.getItem('SignInReferrerUrl').indexOf('/video/') > -1) {
            atv.sessionStorage.setItem('SignInReferrerUrl',
                                        atv.sessionStorage.getItem('SignInReferrerUrl') + '/' + atv.localStorage.getItem('mvpd')
                                        );
          }
          aetnUtils.loadVideo(signInReferrer, 'true', true);
        } else {
          atv.loadAndSwapURL(aetn.globalContext.urlPath + '/tve/success');
        }
      });      
      
    },

    drawFailureView: function(data) {

      //var rootView = new atv.View();
      //rootView.subviews = [];
      //controller.view = rootView;

      new Omniture({
        pageName: 'Authentication',
        events: ['TV Everywhere Authentication Error']
      });

      atv.loadAndSwapURL(aetn.globalContext.urlPath + '/tve/fail');
    },

    deactivateDevice: function(){

      atv.localStorage.removeItem(aetn.globalContext.adobePass.localStorageKey);
      atv.localStorage.removeItem('mvpd');
      atv.localStorage.removeItem('userId');
      atv.sessionStorage.setItem('authStateChanged', 'true');
      atv.localStorage.removeItem('mvpdHash');
      
      console.log(atv.sessionStorage.getItem('authStateChanged'));

      new Omniture({
        pageName : 'Authentication'
      });
      atv.unloadPage();
    }

  });

});