'use strict';

atv.config = {
  doesJavaScriptLoadRoot: true
};

atv.onAppEntry = function(){

  atv.sessionStorage.setItem('stackCtr', 0);
  atv.sessionStorage.setItem('authStateChanged', 'false');

  aetnUtils.initialAuthCheck();
  aetnUtils.initialWatchlistCheck();

  //initial mdialog player
  mDialog.ATVPlayer.init(aetn.globalContext.mdialogOptions);
};

atv.onAppExit = function() {
  atv.localStorage.removeItem('mvpd');
};

