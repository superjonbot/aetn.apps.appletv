"use strict";

module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-requirejs');

  var sites = ['history'];
  var envs = ['local'];

  var sites = ['history', 'ae', 'mlt', 'fyi'];
  var envs = ['local', 'dev', 'qa', 'prod'];
  
  var config = null;

  sites.forEach(function(site) {
    envs.forEach(function(env) {

      //build application level js file
      console.log('build/compiled/aetn.appletv.app.' + site + '.' + env + '.js');

      config = {
        options: {
          baseUrl: 'lib',
          //optimize: env === 'prod' ? 'uglify' : 'none',
          optimize: 'none',
          paths: {
            'klass': 'klass',
            'underscore': 'underscore',
            'aetn' : '../aetn'
          },
          shim: {
            underscore: {
              exports: '_'
            }
          },
          out: 'build/compiled/aetn.appletv.app.' + site + '.' + env + '.js',
          include : [
            'CryptoJS/rollups/hmac-sha1',
            'CryptoJS/components/enc-base64-min',
            'atvutils',
            'almond',
            'mDialog-ATVPlayer-2.3-patch-2.1',
            'aetn/env/' + site,
            'aetn/env/' + env,
            'aetn/appletv/aetnUtils',
            'aetn/appletv/analytics/videotracking',
            'aetn/appletv/app',
            'aetn/appletv/app.player',
            'aetn/appletv/AppMeasurement',
            'aetn/appletv/analytics/omniture',
          ]
        }
      };
      //console.log(grunt.config('requirejs'));
      grunt.config(['requirejs', site + '.' + env + '.app'], config);

      //build main js file
      console.log('build/compiled/aetn.appletv.main.' + site + '.' + env + '.js');

      config = {
        options: {
          baseUrl: 'lib',
          //optimize: env === 'prod' ? 'uglify' : 'none',
          optimize: 'none',
          paths: {
            'klass': 'klass',
            'underscore': 'underscore',
            'aetn' : '../aetn'
          },
          shim: {
            underscore: {
              exports: '_'
            }
          },
          out: 'build/compiled/aetn.appletv.main.' + site + '.' + env + '.js',
          include : [
            'CryptoJS/rollups/hmac-sha1',
            'CryptoJS/components/enc-base64-min',
            'atvutils',
            'almond',
            'mDialog-ATVPlayer-2.3-patch-2.1',
            'aetn/env/' + site,
            'aetn/env/' + env,
            'aetn/appletv/aetnUtils',
            'aetn/appletv/ui/navbar',
            'aetn/appletv/video/videoplayer',
            'aetn/appletv/ui/watchlist',
            'aetn/appletv/ui/progressbar',
            'aetn/appletv/AppMeasurement',
            'aetn/appletv/analytics/omniture',
            'aetn/appletv/analytics/krux',
            'aetn/appletv/analytics/videotracking',
            'aetn/appletv/page'
          ]
        }
      };
      //console.log(grunt.config('requirejs'));
      grunt.config(['requirejs', site + '.' + env + '.main'], config);

      console.log('build/compiled/aetn.appletv.auth.' + site + '.' + env + '.js');

      config = {
        options: {
          baseUrl: 'lib',
          //optimize: env === 'prod' ? 'uglify' : 'none',
          optimize: 'none',
          paths: {
            'klass': 'klass',
            'underscore': 'underscore',
            'aetn' : '../aetn'
          },
          shim: {
            underscore: {
              exports: '_'
            }
          },
          out: 'build/compiled/aetn.appletv.auth.' + site + '.' + env + '.js',
          include : [
            'CryptoJS/rollups/hmac-sha1',
            'CryptoJS/components/enc-base64-min',
            'atvutils',
            'almond',
            'aetn/env/' + site,
            'aetn/env/' + env,
            'aetn/appletv/aetnUtils',
            'aetn/appletv/video/videoplayer',
            'aetn/appletv/tve/auth',
            'aetn/appletv/AppMeasurement',
            'mDialog-ATVPlayer-2.3-patch-2.1',
            'aetn/appletv/analytics/videotracking',
            'aetn/appletv/video/videoplayer'
          ]
        }
      };
      //console.log(grunt.config('requirejs'));
      grunt.config(['requirejs', site + '.' + env + '.auth'], config);

    });
  });

  
};