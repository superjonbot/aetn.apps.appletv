mDialog = {
    ATVPlayer: function () {
        var version = 2.3;
        var activityMonitorKey = atv.sessionStorage.getItem("mDialog-activityMonitorKey");

        function toMS(seconds) {
            return seconds * 1e3
        }

        function parameterize(obj) {
            var str = "";
            for (var key in obj) {
                if (str != "") {
                    str += "&"
                }
                str += key + "=" + obj[key]
            }
            return str
        }

        function objConcat(o1, o2) {
            for (var key in o2) {
                o1[key] = o2[key]
            }
            return o1
        }

        function generateToken() {
            var num;
            var string = "";
            var tokens = "0123456789abcdefghiklmnopqrstuvwxyz";
            for (var i = 0; i < 25; i++) {
                num = Math.floor(Math.random() * tokens.length);
                string += tokens.substring(num, num + 1)
            }
            return string
        }

        function getSessionID() {
            var sessionID = atv.sessionStorage["mDialog-sessionid"];
            if (!sessionID) {
                sessionID = generateToken();
                atv.sessionStorage["mDialog-sessionid"] = sessionID
            }
            return sessionID
        }

        function getDeviceID() {
            var deviceID = atv.localStorage["mDialog-deviceid"];
            if (!deviceID) {
                deviceID = generateToken();
                atv.localStorage["mDialog-deviceid"] = deviceID
            }
            return deviceID
        }

        function streamsURLforAssetKey(assetKey) {
            var subdomain = atv.sessionStorage.getItem("mDialog-subdomain");
            return "https://" + subdomain + ".mdialog.com/video_assets/" + assetKey + "/streams"
        }

        function TrackingEvent(urlTemplates) {
            function urlForTemplate(template) {
                return template.replace("${APPLICATION_KEY}", atv.sessionStorage.getItem("mDialog-appKey")).replace("${CACHE_BUST}", Math.random().toFixed(8).substr(2)).replace("${SDK}", "javascript").replace("${SDK_VERSION}", version).replace("${DEVICE}", "AppleTV").replace("${DEVICE_UNIQUE_ID}", getDeviceID()).replace("${NETWORK}", "wifi").replace("${OS_NAME}", "AppleTV").replace("${OS_VERSION}", "Unknown").replace("${SESSION_ID}", getSessionID())
            }

            function addTrackingData(url) {
                var trackingData = JSON.parse(atv.sessionStorage.getItem("mDialog-trackingData"));
                if (trackingData) {
                    for (var k in trackingData) {
                        url = url.replace("#{" + k + "}", encodeURIComponent(trackingData[k]))
                    }
                }
                return url
            }
            return {
                urlTemplates: urlTemplates,
                fire: function () {
                    for (index in this.urlTemplates) {
                        if (this.urlTemplates[index]["href"] !== undefined) {
                            var analyticsUrl = urlForTemplate(addTrackingData(this.urlTemplates[index]["href"]));
                            pingURL(analyticsUrl)
                        }
                    }
                }
            }
        }

        function pingURL(url) {
            if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                console.log("-- Ping URL: " + url)
            }
            var req = new XMLHttpRequest;
            req.open("GET", url);
            req.send()
        }

        function pingAnalyticsURLs(urls) {
            for (var key in urls) {
                var analyticsUrl = urlForTemplate(unescape(urls[key]["href"]));
                pingURL(analyticsUrl)
            }
        }

        function decisioningTemplate(obj) {
            var decisioningData = {
                api_key: atv.sessionStorage.getItem("mDialog-apiKey"),
                application_key: atv.sessionStorage.getItem("mDialog-appKey"),
                stream_activity_key: atv.sessionStorage.getItem("mDialog-activityMonitorKey"),
                application_version: version,
                os_version: "Unknown",
                os_name: "AppleTV",
                model: "Unknown",
                application_session_unique_identifier: getSessionID(),
                sdk_version: version,
                device_unique_identifier: getDeviceID(),
                wifi: true
            };
            var output = objConcat(decisioningData, obj);
            return parameterize(output)
        }

        function AdBreak(breakInfo) {
            return {
                consumed: breakInfo["consumed"],
                startTime: breakInfo["startTime"],
                endTime: breakInfo["endTime"],
                duration: breakInfo["duration"],
                containsTime: function (t) {
                    return this.startTime <= t && this.endTime > t
                },
                timeRemaining: function (t) {
                    if (this.containsTime(t)) return this.endTime - t;
                    else return 0
                },
                save: function () {
                    var startTime = this["startTime"];
                    var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                    for (var i in adBreaks) {
                        if (adBreaks[i]["startTime"] == startTime) {
                            adBreaks[i] = this
                        }
                    }
                    atv.sessionStorage.setItem("mDialog-breakTimes", JSON.stringify(adBreaks))
                }
            }
        }

        function getOffsetTime(type, playerTime) {
            var startTime = atv.sessionStorage.getItem("mDialog-startTime");
            if (!startTime && type == "live") {
                startTime = playerTime;
                atv.sessionStorage.setItem("mDialog-startTime", startTime)
            } else if (type == "vod") {
                startTime = 0
            }
            return startTime
        }

        function Stream(hypermedia, streamInfo, successCallback) {
            var type = streamInfo["type"];
            var manifestURL = hypermedia["hd_manifest"]["href"];
            var streamKey = streamInfo["stream_key"];
            var duration = streamInfo["duration"];
            var assetKey = streamInfo["assetKey"];
            var watchFrequency = 10;
            var eventsURL = hypermedia["stream_time_events"]["href"];
            var interstitialsURL;
            if (type == "live") {
                var prerollManifestURL = hypermedia["pre_roll_hd_manifest"] ? hypermedia["pre_roll_hd_manifest"]["href"] : null;
                var heartBeatURLs = streamInfo["events"]["heartbeat"] ? streamInfo["events"]["heartbeat"]["tracking"] : null;
                atv.sessionStorage["mDialog-startURLs"] = JSON.stringify(streamInfo["events"]["start"]["tracking"]);
                atv.sessionStorage["mDialog-completeURLs"] = JSON.stringify(streamInfo["events"]["complete"]["tracking"]);
                if (streamInfo["pre_roll"]) {
                    var prerollEvents = streamInfo["pre_roll"]["stream_time_events"];
                    atv.sessionStorage.setItem("mDialog-prerollEventsData", JSON.stringify(prerollEvents));
                    parseBreaks(prerollEvents)
                }
            } else {
                interstitialsURL = hypermedia["interstitials"] ? hypermedia["interstitials"]["href"] : null
            }

            function watchHeartbeatURL(url, interval) {
                pingURL(url);
                var heartbeat = atv.setInterval(function (instance) {
                    pingURL(url)
                }, interval)
            }

            function fetchEvents() {
                var req = new XMLHttpRequest;
                req.onreadystatechange = function () {
                    if (req.readyState == 4 && req.status == 200) {
                        eventsData = JSON.parse(req.responseText);
                        atv.sessionStorage.setItem("mDialog-eventsData", JSON.stringify(eventsData));
                        if (eventsData) {
                            parseBreaks(eventsData)
                        }
                        successCallback()
                    }
                };
                req.open("GET", eventsURL);
                req.send()
            }

            function watchStreamTimeEvents() {
                fetchEvents();
                var streamTimeEventsWatcher = atv.setInterval(function (instance) {
                    fetchEvents()
                }, toMS(watchFrequency))
            }

            function parseBreaks(eventsData) {
                var eventsData = eventsData;
                for (t in eventsData) {
                    var breakEvent = eventsData[t]["ad_break"];
                    var eventTime = parseInt(t, 10);
                    if (breakEvent) {
                        var adBreak = {
                            consumed: false,
                            startTime: eventTime,
                            endTime: eventTime + breakEvent["duration"],
                            duration: breakEvent["duration"]
                        };
                        if (!inCurrenBreaks(adBreak)) {
                            addToBreakData(adBreak)
                        }
                    }
                }
            }

            function inCurrenBreaks(adBreak) {
                var currentBreakTimes = atv.sessionStorage.getItem("mDialog-breakTimes");
                if (currentBreakTimes) {
                    currentBreakTimes = JSON.parse(currentBreakTimes);
                    for (i in currentBreakTimes) {
                        if (currentBreakTimes[i]["startTime"] == adBreak["startTime"]) {
                            return true
                        }
                    }
                }
                return false
            }

            function addToBreakData(adBreak) {
                var currentBreakTimes = atv.sessionStorage.getItem("mDialog-breakTimes");
                currentBreakTimes = JSON.parse(currentBreakTimes) || [];
                currentBreakTimes.push(adBreak);
                atv.sessionStorage.setItem("mDialog-breakTimes", JSON.stringify(currentBreakTimes))
            }

            function initStream() {
                if (type == "live") {
                    watchStreamTimeEvents()
                } else {
                    fetchEvents()
                } if (heartBeatURLs) {
                    for (i in heartBeatURLs) {
                        var url = heartBeatURLs[i]["href"];
                        var interval = heartBeatURLs[i]["interval"];
                        watchHeartbeatURL(unescape(url), toMS(interval))
                    }
                }
            }
            initStream();
            return {
                type: type,
                assetKey: assetKey,
                duration: duration,
                streamKey: streamKey,
                interstitialsURL: interstitialsURL,
                manifestURL: manifestURL,
                prerollManifestURL: prerollManifestURL,
                prerollPlayed: false,
                shouldTrackFeature: false
            }
        }
        return {
            version: version,
            debug: atv.sessionStorage.getItem("mDialog-debug"),
            init: function (options) {
                atv.sessionStorage.setItem("mDialog-subdomain", options.subdomain);
                atv.sessionStorage.setItem("mDialog-apiKey", options.apiKey);
                atv.sessionStorage.setItem("mDialog-appKey", options.appKey);
                atv.sessionStorage.setItem("mDialog-debug", options.debug);
                atv.sessionStorage.setItem("mDialog-activityMonitorKey", options.activityMonitorKey || "");
                if (atv.sessionStorage["mDialog-debug"]) {
                    console.log("--");
                    console.log("-- mDialog Player Initialized v" + this.version);
                    console.log("-- subdomain: " + atv.sessionStorage["mDialog-subdomain"]);
                    console.log("-- apiKey: " + atv.sessionStorage["mDialog-apiKey"]);
                    console.log("-- appKey: " + atv.sessionStorage["mDialog-appKey"]);
                    console.log("-- deviceId: " + getDeviceID());
                    console.log("-- sessionId: " + getSessionID());
                    console.log("--")
                }
            },
            loadedStream: function () {
                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));

                function start() {
                    var startURLs = JSON.parse(atv.sessionStorage.getItem("mDialog-startURLs"));
                    var startEvents = new TrackingEvent(startURLs);
                    startEvents.fire()
                }
                return {
                    type: loadedStream["type"],
                    assetKey: loadedStream["assetKey"],
                    duration: loadedStream["duration"],
                    streamKey: loadedStream["streamKey"],
                    interstitialsURL: loadedStream["interstitialsURL"],
                    manifestURL: loadedStream["manifestURL"],
                    prerollManifestURL: loadedStream["prerollManifestURL"],
                    prerollPlayed: loadedStream["prerollPlayed"],
                    shouldTrackFeature: loadedStream["shouldTrackFeature"],
                    markPrerollPlayed: function () {
                        loadedStream.prerollPlayed = true;
                        atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream)
                    },
                    startFeature: function () {
                        console.log("-- Start Feature Content Tracking");
                        start();
                        loadedStream.shouldTrackFeature = true;
                        atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream)
                    }
                }
            },
            loadStreamForKey: function (assetKey, streamContext, successCallback, failureCallback) {
                var req = new XMLHttpRequest;
                var location;
                var hypermedia;
                var streamInfo;
                var decisioningData = decisioningTemplate();
                var apiKey = atv.sessionStorage.getItem("mDialog-apiKey");
                atv.sessionStorage.removeItem("mDialog-startTime");
                atv.sessionStorage.removeItem("mDialog-breakTimes");
                if (streamContext) {
                    if (streamContext["decisioningData"]) {
                        decisioningData = decisioningTemplate(streamContext["decisioningData"])
                    }
                    if (streamContext["trackingData"]) {
                        atv.sessionStorage.setItem("mDialog-trackingData", JSON.stringify(streamContext["trackingData"]))
                    }
                }
                req.open("POST", streamsURLforAssetKey(assetKey));
                req.setRequestHeader("Authorization", "mDialogAPI " + apiKey);
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        if (req.status == 201) {
                            location = req.getResponseHeader("Location");
                            hypermedia = JSON.parse(req.responseText);
                            if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                                console.log("-- Created stream with location: " + location);
                                if (hypermedia["stream_activity"]) {
                                    console.log("-- Stream Activity can be viewed at: " + hypermedia["stream_activity"]["href"])
                                }
                            }
                            req.open("GET", location);
                            req.send()
                        } else if (req.status == 200) {
                            streamInfo = JSON.parse(req.responseText);
                            streamInfo["assetKey"] = assetKey;
                            var loadedStream = new Stream(hypermedia, streamInfo, function (stream) {
                                atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream);
                                successCallback(loadedStream)
                            })
                        } else {
                            failureCallback()
                        }
                    }
                };
                req.send(decisioningData)
            },
            timeUpdate: function (playerTime, breakCallback) {

                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));
                var eventsData = loadedStream.shouldTrackFeature ? JSON.parse(atv.sessionStorage.getItem("mDialog-eventsData")) : JSON.parse(atv.sessionStorage.getItem("mDialog-prerollEventsData"));
                // console.log("eventsData = ", JSON.stringify(eventsData));
                var breakTimes = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                var timeOffset = getOffsetTime(loadedStream.type, playerTime);
                if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                    console.log(playerTime)
                }
                for (i in breakTimes) {
                    var ab = new AdBreak(breakTimes[i]);
                    if (ab.containsTime(playerTime)) {
                        var timeRemaining = ab.timeRemaining(playerTime);
                        breakCallback(ab, timeRemaining);
                        if (timeRemaining == 1 && ab["consumed"] == false) {
                            ab["consumed"] = true;
                            ab.save()
                        }
                    }
                }
                if (eventsData[playerTime]) {
                    var currentEvent = new TrackingEvent(eventsData[playerTime]["tracking"]);
                    currentEvent.fire()
                }
            },
            didStopPlaying: function () {
                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));
                if (loadedStream.shouldTrackFeature) var completeURLs = JSON.parse(atv.sessionStorage.getItem("mDialog-completeURLs"));
                var completeEvents = new TrackingEvent(completeURLs);
                completeEvents.fire()
            },
            nearestPreviousAdBreak: function (timeIntervalSec) {
                var currentTime = Math.floor(timeIntervalSec);
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                adBreaks.sort(function (a, b) {
                    return parseFloat(a["startTime"]) - parseFloat(b["startTime"])
                });
                var closestBreak;
                for (var i in adBreaks) {
                    if (currentTime >= adBreaks[i]["startTime"]) {
                        if (closestBreak && adBreaks[i]["startTime"] > closestBreak["startTime"]) {
                            closestBreak = adBreaks[i]
                        } else {
                            closestBreak = adBreaks[i]
                        }
                    }
                }
                return closestBreak
            },
            streamTimeWithoutAds: function (currentTime) {
                var streamTimeWithoutAds;
                var totalAdDuration = 0;
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                for (var i in adBreaks) {
                    if (adBreaks[i]["startTime"] < currentTime) {
                        totalAdDuration = totalAdDuration + adBreaks[i]["duration"]
                    }
                }
                streamTimeWithoutAds = currentTime - totalAdDuration;
                return streamTimeWithoutAds >= 0 ? streamTimeWithoutAds : 0
            },
            streamTimeWithAds: function (streamTimeWithoutAds) {
                var streamTimeWithAds;
                var totalAdDuration = 0;
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                for (var i in adBreaks) {
                    if (adBreaks[i]["startTime"] < streamTimeWithoutAds) {
                        totalAdDuration = totalAdDuration + adBreaks[i]["duration"]
                    }
                }
                streamTimeWithAds = streamTimeWithoutAds + totalAdDuration;
                return streamTimeWithAds >= 0 ? streamTimeWithAds : 0
            }
        }
    }()
};