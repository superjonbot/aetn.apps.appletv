/**
 * almond 0.2.7 Copyright (c) 2011-2012, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/almond for details
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*jslint sloppy: true */
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap,
            foundI, foundStarMap, starI, i, j, part,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name && name.charAt(0) === ".") {
            //If have a base name, try to normalize against it,
            //otherwise, assume it is a top-level require that will
            //be relative to baseUrl in the end.
            if (baseName) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that "directory" and not name of the baseName's
                //module. For instance, baseName of "one/two/three", maps to
                //"one/two/three.js", but we want the directory, "one/two" for
                //this normalization.
                baseParts = baseParts.slice(0, baseParts.length - 1);

                name = baseParts.concat(name.split("/"));

                //start trimDots
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                            //End of the line. Keep at least one non-dot
                            //path segment at the front so it can be mapped
                            //correctly to disk. Otherwise, there is likely
                            //no path mapping for a path starting with '..'.
                            //This can still fail, but catches the most reasonable
                            //uses of ..
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                //end trimDots

                name = name.join("/");
            } else if (name.indexOf('./') === 0) {
                // No baseName, so this is ID is resolved relative
                // to baseUrl, pull off the leading dot.
                name = name.substring(2);
            }
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            return req.apply(undef, aps.call(arguments, 0).concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            atv.setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        config = cfg;
        if (config.deps) {
            req(config.deps, config.callback);
        }
        return req;
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

define("almond", function(){});

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(g,l){var e={},d=e.lib={},m=function(){},k=d.Base={extend:function(a){m.prototype=this;var c=new m;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
p=d.WordArray=k.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=l?c:4*a.length},toString:function(a){return(a||n).stringify(this)},concat:function(a){var c=this.words,q=a.words,f=this.sigBytes;a=a.sigBytes;this.clamp();if(f%4)for(var b=0;b<a;b++)c[f+b>>>2]|=(q[b>>>2]>>>24-8*(b%4)&255)<<24-8*((f+b)%4);else if(65535<q.length)for(b=0;b<a;b+=4)c[f+b>>>2]=q[b>>>2];else c.push.apply(c,q);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=g.ceil(c/4)},clone:function(){var a=k.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],b=0;b<a;b+=4)c.push(4294967296*g.random()|0);return new p.init(c,a)}}),b=e.enc={},n=b.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],f=0;f<a;f++){var d=c[f>>>2]>>>24-8*(f%4)&255;b.push((d>>>4).toString(16));b.push((d&15).toString(16))}return b.join("")},parse:function(a){for(var c=a.length,b=[],f=0;f<c;f+=2)b[f>>>3]|=parseInt(a.substr(f,
2),16)<<24-4*(f%8);return new p.init(b,c/2)}},j=b.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],f=0;f<a;f++)b.push(String.fromCharCode(c[f>>>2]>>>24-8*(f%4)&255));return b.join("")},parse:function(a){for(var c=a.length,b=[],f=0;f<c;f++)b[f>>>2]|=(a.charCodeAt(f)&255)<<24-8*(f%4);return new p.init(b,c)}},h=b.Utf8={stringify:function(a){try{return decodeURIComponent(escape(j.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(a){return j.parse(unescape(encodeURIComponent(a)))}},
r=d.BufferedBlockAlgorithm=k.extend({reset:function(){this._data=new p.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=h.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,b=c.words,f=c.sigBytes,d=this.blockSize,e=f/(4*d),e=a?g.ceil(e):g.max((e|0)-this._minBufferSize,0);a=e*d;f=g.min(4*a,f);if(a){for(var k=0;k<a;k+=d)this._doProcessBlock(b,k);k=b.splice(0,a);c.sigBytes-=f}return new p.init(k,f)},clone:function(){var a=k.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});d.Hasher=r.extend({cfg:k.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){r.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,d){return(new a.init(d)).finalize(b)}},_createHmacHelper:function(a){return function(b,d){return(new s.HMAC.init(a,
d)).finalize(b)}}});var s=e.algo={};return e}(Math);
(function(){var g=CryptoJS,l=g.lib,e=l.WordArray,d=l.Hasher,m=[],l=g.algo.SHA1=d.extend({_doReset:function(){this._hash=new e.init([1732584193,4023233417,2562383102,271733878,3285377520])},_doProcessBlock:function(d,e){for(var b=this._hash.words,n=b[0],j=b[1],h=b[2],g=b[3],l=b[4],a=0;80>a;a++){if(16>a)m[a]=d[e+a]|0;else{var c=m[a-3]^m[a-8]^m[a-14]^m[a-16];m[a]=c<<1|c>>>31}c=(n<<5|n>>>27)+l+m[a];c=20>a?c+((j&h|~j&g)+1518500249):40>a?c+((j^h^g)+1859775393):60>a?c+((j&h|j&g|h&g)-1894007588):c+((j^h^
g)-899497514);l=g;g=h;h=j<<30|j>>>2;j=n;n=c}b[0]=b[0]+n|0;b[1]=b[1]+j|0;b[2]=b[2]+h|0;b[3]=b[3]+g|0;b[4]=b[4]+l|0},_doFinalize:function(){var d=this._data,e=d.words,b=8*this._nDataBytes,g=8*d.sigBytes;e[g>>>5]|=128<<24-g%32;e[(g+64>>>9<<4)+14]=Math.floor(b/4294967296);e[(g+64>>>9<<4)+15]=b;d.sigBytes=4*e.length;this._process();return this._hash},clone:function(){var e=d.clone.call(this);e._hash=this._hash.clone();return e}});g.SHA1=d._createHelper(l);g.HmacSHA1=d._createHmacHelper(l)})();
(function(){var g=CryptoJS,l=g.enc.Utf8;g.algo.HMAC=g.lib.Base.extend({init:function(e,d){e=this._hasher=new e.init;"string"==typeof d&&(d=l.parse(d));var g=e.blockSize,k=4*g;d.sigBytes>k&&(d=e.finalize(d));d.clamp();for(var p=this._oKey=d.clone(),b=this._iKey=d.clone(),n=p.words,j=b.words,h=0;h<g;h++)n[h]^=1549556828,j[h]^=909522486;p.sigBytes=b.sigBytes=k;this.reset()},reset:function(){var e=this._hasher;e.reset();e.update(this._iKey)},update:function(e){this._hasher.update(e);return this},finalize:function(e){var d=
this._hasher;e=d.finalize(e);d.reset();return d.finalize(this._oKey.clone().concat(e))}})})();

define("CryptoJS/rollups/hmac-sha1", function(){});

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
(function(){var h=CryptoJS,j=h.lib.WordArray;h.enc.Base64={stringify:function(b){var e=b.words,f=b.sigBytes,c=this._map;b.clamp();b=[];for(var a=0;a<f;a+=3)for(var d=(e[a>>>2]>>>24-8*(a%4)&255)<<16|(e[a+1>>>2]>>>24-8*((a+1)%4)&255)<<8|e[a+2>>>2]>>>24-8*((a+2)%4)&255,g=0;4>g&&a+0.75*g<f;g++)b.push(c.charAt(d>>>6*(3-g)&63));if(e=c.charAt(64))for(;b.length%4;)b.push(e);return b.join("")},parse:function(b){var e=b.length,f=this._map,c=f.charAt(64);c&&(c=b.indexOf(c),-1!=c&&(e=c));for(var c=[],a=0,d=0;d<
e;d++)if(d%4){var g=f.indexOf(b.charAt(d-1))<<2*(d%4),h=f.indexOf(b.charAt(d))>>>6-2*(d%4);c[a>>>2]|=(g|h)<<24-8*(a%4);a++}return j.create(c,a)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}})();

define("CryptoJS/components/enc-base64-min", function(){});

// ***************************************************
// ATVUtils - a JavaScript helper library for Apple TV
var atvutils = ATVUtils = {

  makeRequest: function(url, method, headers, body, callback) {
    if ( !url ) {
      throw "loadURL requires a url argument";
    }

    var method = method || "GET",
    headers = headers || {},
    body = body || "";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      try {
        if (xhr.readyState == 4 ) {
          if ( xhr.status == 200) {
            callback(xhr.responseXML);
          } else {
            console.log("makeRequest received HTTP status " + xhr.status + " for " + url);
            callback(null);
          }
        }
      } catch (e) {
        console.error('makeRequest caught exception while processing request for ' + url + '. Aborting. Exception: ' + e);
        xhr.abort();
        callback(null);
      }
    }
    xhr.open(method, url, true);

    for(var key in headers) {
      xhr.setRequestHeader(key, headers[key]);
    }

    xhr.send();
    return xhr;
  },

  makeErrorDocument: function(message, description) {
    if ( !message ) {
      message = "";
    }
    if ( !description ) {
      description = "";
    }

    var errorXML = '<?xml version="1.0" encoding="UTF-8"?> \
    <atv> \
    <body> \
    <dialog id="com.sample.error-dialog"> \
    <title><![CDATA[' + message + ']]></title> \
    <description><![CDATA[' + description + ']]></description> \
    </dialog> \
    </body> \
    </atv>';

    return atv.parseXML(errorXML);
  },

  siteUnavailableError: function() {
      // TODO: localize
      return this.makeErrorDocument("This page has error or not available.", "Please check.");
  },

  loadError: function(message, description) {
    atv.loadXML(this.makeErrorDocument(message, description));
  },

  loadAndSwapError: function(message, description) {
    atv.loadAndSwapXML(this.makeErrorDocument(message, description));
  },

  loadURLInternal: function(url, method, headers, body, loader) {
    var me = this,
    xhr,
    proxy = new atv.ProxyDocument;

    proxy.show();

    proxy.onCancel = function() {
      if ( xhr ) {
        xhr.abort();
      }
    };

    xhr = me.makeRequest(url, method, headers, body, function(xml) {
      try {
        loader(proxy, xml);
      } catch(e) {
        console.error("Caught exception in for " + url + ". " + e);
        loader(me.siteUnavailableError());
      }
    });
  },

  loadURL: function( options ) { //url, method, headers, body, processXML) {
    var me = this;
    if( typeof( options ) === "string" ) {
      var url = options;
    } else {
      var url = options.url,
      method = options.method || null,
      headers = options.headers || null,
      body = options.body || null,
      processXML = options.processXML || null;
    }

    this.loadURLInternal(url, method, headers, body, function(proxy, xml) {
      if(typeof(processXML) == "function") processXML.call(this, xml);
      try {
        proxy.loadXML(xml, function(success) {
          if ( !success ) {
            console.log("loadURL failed to load " + url);
            proxy.loadXML(me.siteUnavailableError());
          }
        });
      } catch (e) {
        console.log("loadURL caught exception while loading " + url + ". " + e);
        proxy.loadXML(me.siteUnavailableError());
      }
    });
  },

  // loadAndSwapURL can only be called from page-level JavaScript of the page that wants to be swapped out.
  loadAndSwapURL: function( options ) { //url, method, headers, body, processXML) {
    var me = this;
    if( typeof( options ) === "string" ) {
      var url = options;
    } else {
      var url = options.url,
      method = options.method || null,
      headers = options.headers || null,
      body = options.body || null,
      processXML = options.processXML || null;
    }

    this.loadURLInternal(url, method, headers, body, function(proxy, xml) {
      if(typeof(processXML) == "function") processXML.call(this, xml);
      try {
        proxy.loadXML(xml, function(success) {
          if ( success ) {
            atv.unloadPage();
          } else {
            console.log("loadAndSwapURL failed to load " + url);
            proxy.loadXML(me.siteUnavailableError(), function(success) {
              if ( success ) {
                atv.unloadPage();
              }
            });
          }
        });
      } catch (e) {
        console.error("loadAndSwapURL caught exception while loading " + url + ". " + e);
        proxy.loadXML(me.siteUnavailableError(), function(success) {
          if ( success ) {
            atv.unloadPage();
          }
        });
      }
    });
  },

  /**
   * Used to manage setting and retrieving data from local storage
   */
   data: function(key, value) {
    if(key && value) {
      try {
        atv.localStorage.setItem(key, value);
        return value;
      } catch(error) {
        console.error('Failed to store data element: '+ error);
      }

    } else if(key) {
      try {
        return atv.localStorage.getItem(key);
      } catch(error) {
        console.error('Failed to retrieve data element: '+ error);
      }
    }
    return null;
   },

   deleteData: function(key) {
    try {
      atv.localStorage.removeItem(key);
    } catch(error) {
      console.error('Failed to remove data element: '+ error);
    }
   },


  /**
   * @params options.name - string node name
   * @params options.text - string textContent
   * @params options.attrs - array of attribute to set {"name": string, "value": string, bool}
   * @params options.children = array of childNodes same values as options
   * @params doc - document to attach the node to
   * returns node
   */
   createNode: function(options, doc) {
    var doc = doc || document;
    options = options || {};

    if(options.name && options.name != '') {
      var newElement = doc.makeElementNamed(options.name);

      if(options.text) newElement.textContent = options.text;

      if(options.attrs) {
        options.attrs.forEach(function(e, i, a) {
          newElement.setAttribute(e.name, e.value);
        }, this);
      }

      if(options.children) {
        options.children.forEach(function(e,i,a) {
          newElement.appendChild( this.createNode( e, doc ) );
        }, this)
      }

      return newElement;
    }
   },

   validEmailAddress: function( email ) {
    var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
    isValid = email.search( emailRegex );
    return ( isValid > -1 );
   },

   softwareVersionIsAtLeast: function( version ) {
    var deviceVersion = atv.device.softwareVersion.split('.'),
    requestedVersion = version.split('.');

    // We need to pad the device version length with "0" to account for 5.0 vs 5.0.1
    if( deviceVersion.length < requestedVersion.length ) {
      var difference = requestedVersion.length - deviceVersion.length,
      dvl = deviceVersion.length;

      for( var i = 0; i < difference; i++ ) {
        deviceVersion[dvl + i] =  "0";
      };
    };

    // compare the same index from each array.
    for( var c = 0; c < deviceVersion.length; c++ ) {
      var dv = deviceVersion[c],
      rv = requestedVersion[c] || "0";

      if( parseInt( dv ) > parseInt( rv ) ) {
        return true;
      } else if( parseInt( dv ) < parseInt( rv )  ) {
        return false;
      };
    };

    // If we make it this far the two arrays are identical, so we're true
    return true;
  },

  shuffleArray: function( arr ) {
    var tmp, current, top = arr.length;

    if(top) {
      while(--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = arr[current];
        arr[current] = arr[top];
        arr[top] = tmp;
      };
    };

    return arr;
  },

  loadTextEntry: function( textEntryOptions ) {
    var textView = new atv.TextEntry;

    textView.type              = textEntryOptions.type             || "emailAddress";
    textView.title             = textEntryOptions.title            || "";
    textView.image             = textEntryOptions.image            || null;
    textView.instructions      = textEntryOptions.instructions     || "";
    textView.label             = textEntryOptions.label            || "";
    textView.footnote          = textEntryOptions.footnote         || "";
    textView.defaultValue      = textEntryOptions.defaultValue     || null;
    textView.defaultToAppleID  = textEntryOptions.defaultToAppleID || false;
    textView.onSubmit          = textEntryOptions.onSubmit,
    textView.onCancel          = textEntryOptions.onCancel,

    textView.show();
  },

  log: function ( message , level ) {
    var debugLevel = atv.sessionStorage.getItem( "DEBUG_LEVEL" ),
    level = level || 0;

    if( level <= debugLevel ) {
      console.log( message );
    }
  },

  accessibilitySafeString: function ( string ) {
    var string = unescape( string );

    string = string
        .replace( /&amp;/g, 'and' )
        .replace( /&lt;/g, 'less than' )
        .replace( /&gt;/g, 'greater than' )
        .replace( /&/g, 'and' )
        .replace( /\</g, 'less than' )
        .replace( /\>/g, 'greater than' );

    return string;
  }
};

// Extend Object
Object.create = function (o) {
    var F = function() {}
    F.prototype = o;
    return new F();
};

Object.extend = function(obj, extension) {
    for( var p in extension ) {
        if( extension.hasOwnProperty( p ) ) {
            obj.prototype[ p ] = extension[ p ];
        }
    }
}

// Extend atv.ProxyDocument to load errors from a message and description.
if( atv.ProxyDocument ) {
  atv.ProxyDocument.prototype.loadError = function(message, description) {
    var doc = atvutils.makeErrorDocument(message, description);
    this.loadXML(doc);
  }
}


// atv.Document extensions
if( atv.Document ) {
  atv.Document.prototype.getElementById = function(id) {
    var elements = this.evaluateXPath("//*[@id='" + id + "']", this);
    if ( elements && elements.length > 0 ) {
      return elements[0];
    }
    return undefined;
  }
}


// atv.Element extensions
if( atv.Element ) {
  atv.Element.prototype.getElementsByTagName = function(tagName) {
    return this.ownerDocument.evaluateXPath("descendant::" + tagName, this);
  }

  atv.Element.prototype.getElementByTagName = function(tagName) {
    var elements = this.getElementsByTagName(tagName);
    if ( elements && elements.length > 0 ) {
      return elements[0];
    }
    return undefined;
  }
}

// Simple Array Sorting methods
Array.prototype.sortAsc = function() {
  this.sort(function( a, b ){
    return a - b;
  });
};

Array.prototype.sortDesc = function() {
  this.sort(function( a, b ){
    return b - a;
  });
};


// Date methods and properties
Date.lproj = {
  "DAYS": {
    "en": {
      "full": ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      "abbrv": ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    },
    "en_GB": {
      "full": ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      "abbrv": ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    }
  },
  "MONTHS": {
    "en": {
      "full": ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      "abbrv": ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    "en_GB": {
      "full": ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      "abbrv": ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
  }
}

Date.prototype.getLocaleMonthName = function( type ) {
  var language = atv.device.language,
  type = ( type === true ) ? "abbrv" : "full",
  MONTHS = Date.lproj.MONTHS[ language ] || Date.lproj.MONTHS[ "en" ];

  return MONTHS[ type ][ this.getMonth() ];
};

Date.prototype.getLocaleDayName = function( type ) {
  var language = atv.device.language,
  type = ( type === true ) ? "abbrv" : "full",
  DAYS = Date.lproj.DAYS[ language ] || Date.lproj.DAYS[ "en" ];

  return DAYS[ type ][ this.getDay() ];
};

Date.prototype.nextDay = function( days ) {
  var oneDay = 86400000,
  days = days || 1;
  this.setTime( new Date( this.valueOf() + ( oneDay * days ) ) );
};

Date.prototype.prevDay = function( days ) {
  var oneDay = 86400000,
  days = days || 1;
  this.setTime( new Date( this.valueOf() - ( oneDay * days ) ) );
};


// String Trim methods
String.prototype.trim = function ( ch )
{
  var ch = ch || '\\s',
  s = new RegExp( '^['+ch+']+|['+ch+']+$','g');
  return this.replace(s,'');
};

String.prototype.trimLeft = function ( ch )
{
  var ch = ch || '\\s',
  s = new RegExp( '^['+ch+']+','g');
  return this.replace(s,'');
};

String.prototype.trimRight = function ( ch )
{
  var ch = ch || '\\s',
  s = new RegExp( '['+ch+']+$','g');
  return this.replace(s,'');
};

String.prototype.xmlEncode = function()
{
    var string = unescape( this );

    string = string
            .replace( /&(?![a-z#]+;)/ig, '&amp;' )
            .replace( /\</g, '&lt;' )
            .replace( /\>/g, '&gt;' );

    return string;
};

String.prototype.capitalize = function()
{
  console.log( typeof this );
  var firstLetter = this.match(/^[a-z]/i);
  if ( firstLetter )
  {
    firstLetter = firstLetter[0];
    return firstLetter.toUpperCase() + this.slice( 1 );
  }
};

// End ATVUtils
// ***************************************************
/**
 * This is an XHR handler. It handles most of tediousness of the XHR request
 * and keeps track of onRefresh XHR calls so that we don't end up with multiple
 * page refresh calls.
 *
 * You can see how I call it on the handleRefresh function below.
 *
 *
 * @params object (hash) $options
 * @params string $options.url - url to be loaded
 * @params string $options.method - "GET", "POST", "PUT", "DELTE"
 * @params bool $options.type - false = "Sync" or true = "Async" (You should always use true)
 * @params func $options.success - Gets called on readyState 4 & status 200
 * @params func $options.failure - Gets called on readyState 4 & status != 200
 * @params func $options.callback - Gets called after the success and failure on readyState 4
 * @params string $options.data - data to be sent to the server
 * @params bool $options.refresh - Is this a call from the onRefresh event.
 */
 ATVUtils.Ajax = function($options) {
  var me = this;
  $options = $options || {}

  /* Setup properties */
  this.url = $options.url || false;
  this.method = $options.method || "GET";
  this.type = ($options.type === false) ? false : true;
  this.success = $options.success || null;
  this.failure = $options.failure || null;
  this.data = $options.data || null;
  this.complete = $options.complete || null;
  this.refresh = $options.refresh || false;
  this.headers = $options.headers || null;

  if(!this.url) {
    console.error('\nAjax Object requires a url to be passed in: e.g. { "url": "some string" }\n')
    return undefined;
  };
  console.log('Ajax', this.url);
  this.id = Date.now();

  this.createRequest();

  this.req.onreadystatechange = this.stateChange;
  this.req.object = this;

  this.open();

  this.setupHeaders();

  this.send();

};

ATVUtils.Ajax.currentlyRefreshing = false;
ATVUtils.Ajax.activeRequests = {};

ATVUtils.Ajax.onPageUnload = function() {
  ATVUtils.Ajax._cancelAllRequests();
};

ATVUtils.Ajax._cancelAllRequests = function() {
  for ( var p in ATVUtils.Ajax.activeRequests ) {
    if( ATVUtils.Ajax.activeRequests.hasOwnProperty( p ) ) {
      var obj = ATVUtils.Ajax.activeRequests[ p ];
      if( obj.hasOwnProperty( "abort" ) && typeof obj.abort == "function"  ) {
        obj.req.abort();
      };
      delete ATVUtils.Ajax.activeRequests[ p ];
    };
  };
  ATVUtils.Ajax.activeRequests = {};
};

ATVUtils.Ajax.prototype = {
  stateChange: function() {
    var me = this.object;
    switch(this.readyState) {
      case 1:
        if(typeof(me.connection) === "function") me.connection(this, me);
        break;
      case 2:
        if(typeof(me.received) === "function") me.received(this, me);
        break;
      case 3:
        if(typeof(me.processing) === "function") me.processing(this, me);
        break;
      case 4:
        if(this.status == "200" || this.status == "201" || this.status == "204") {
          if(typeof(me.success) === "function") me.success(this, me);
        } else {
          if(typeof(me.failure) === "function") me.failure(this.status, this, me);
        }
        if(typeof(me.complete) === "function") me.complete(this, me);
        if(me.refresh) Ajax.currentlyRefreshing = false;
        break;
      default:
        console.log("I don't think I should be here.");
        break;
    }
  },
  cancelRequest: function() {
    this.req.abort();
    delete ATVUtils.Ajax.activeRequests[ this.id ];
  },
  cancelAllActiveRequests: function() {
    ATVUtils.Ajax._cancelAllRequests();
  },
  createRequest: function() {
    try {
      this.req = new XMLHttpRequest();
      ATVUtils.Ajax.activeRequests[ this.id ] = this;
      if(this.refresh) ATVUtils.Ajax.currentlyRefreshing = true;
    } catch (error) {
      alert("The request could not be created: </br>" + error);
      console.error("failed to create request: " +error);
    }
  },
  open: function() {
    try {
      this.req.open(this.method, this.url, this.type);
    } catch(error) {
      console.log("failed to open request: " + error);
    }
  },
  setupHeaders: function(){
    for(var key in this.headers) {
      this.req.setRequestHeader(key, this.headers[key]);
    }
  },
  send: function() {
    var data = this.data || null;
    try {
      this.req.send(data);
    } catch(error) {
      console.log("failed to send request: " + error);
    }
  }
};

// http://www.webtoolkit.info/javascript-base64.html

ATVUtils.Base64 = (function() {
    var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    // private method for UTF-8 encoding
    function _utf8_encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            };

        };

        return utftext;
    };

    // private method for UTF-8 decoding
    function _utf8_decode(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            };

        };

        return string;
    };

    // public method for encoding
    function encode(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = _utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            };

            output = output +
            _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
            _keyStr.charAt(enc3) + _keyStr.charAt(enc4);

        };

        return output;
    };

    // public method for decoding
    function decode(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            };
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            };

        };

        output = _utf8_decode(output);

        return output;

    };
    
    function prepareForHttGetRequest( input ) {
      var output = input.replace(/\+/g, "@").replace(/=/g, "_");
      return output;
    };

    return {
        "decode": decode,
        "encode": encode
    };
})();


define("atvutils", function(){});

mDialog = {
    ATVPlayer: function () {
        var version = 2.3;
        var activityMonitorKey = atv.sessionStorage.getItem("mDialog-activityMonitorKey");

        function toMS(seconds) {
            return seconds * 1e3
        }

        function parameterize(obj) {
            var str = "";
            for (var key in obj) {
                if (str != "") {
                    str += "&"
                }
                str += key + "=" + obj[key]
            }
            return str
        }

        function objConcat(o1, o2) {
            for (var key in o2) {
                o1[key] = o2[key]
            }
            return o1
        }

        function generateToken() {
            var num;
            var string = "";
            var tokens = "0123456789abcdefghiklmnopqrstuvwxyz";
            for (var i = 0; i < 25; i++) {
                num = Math.floor(Math.random() * tokens.length);
                string += tokens.substring(num, num + 1)
            }
            return string
        }

        function getSessionID() {
            var sessionID = atv.sessionStorage["mDialog-sessionid"];
            if (!sessionID) {
                sessionID = generateToken();
                atv.sessionStorage["mDialog-sessionid"] = sessionID
            }
            return sessionID
        }

        function getDeviceID() {
            var deviceID = atv.localStorage["mDialog-deviceid"];
            if (!deviceID) {
                deviceID = generateToken();
                atv.localStorage["mDialog-deviceid"] = deviceID
            }
            return deviceID
        }

        function streamsURLforAssetKey(assetKey) {
            var subdomain = atv.sessionStorage.getItem("mDialog-subdomain");
            return "https://" + subdomain + ".mdialog.com/video_assets/" + assetKey + "/streams"
        }

        function TrackingEvent(urlTemplates) {
            function urlForTemplate(template) {
                return template.replace("${APPLICATION_KEY}", atv.sessionStorage.getItem("mDialog-appKey")).replace("${CACHE_BUST}", Math.random().toFixed(8).substr(2)).replace("${SDK}", "javascript").replace("${SDK_VERSION}", version).replace("${DEVICE}", "AppleTV").replace("${DEVICE_UNIQUE_ID}", getDeviceID()).replace("${NETWORK}", "wifi").replace("${OS_NAME}", "AppleTV").replace("${OS_VERSION}", "Unknown").replace("${SESSION_ID}", getSessionID())
            }

            function addTrackingData(url) {
                var trackingData = JSON.parse(atv.sessionStorage.getItem("mDialog-trackingData"));
                if (trackingData) {
                    for (var k in trackingData) {
                        url = url.replace("#{" + k + "}", encodeURIComponent(trackingData[k]))
                    }
                }
                return url
            }
            return {
                urlTemplates: urlTemplates,
                fire: function () {
                    for (index in this.urlTemplates) {
                        if (this.urlTemplates[index]["href"] !== undefined) {
                            var analyticsUrl = urlForTemplate(addTrackingData(this.urlTemplates[index]["href"]));
                            pingURL(analyticsUrl)
                        }
                    }
                }
            }
        }

        function pingURL(url) {
            if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                console.log("-- Ping URL: " + url)
            }
            var req = new XMLHttpRequest;
            req.open("GET", url);
            req.send()
        }

        function pingAnalyticsURLs(urls) {
            for (var key in urls) {
                var analyticsUrl = urlForTemplate(unescape(urls[key]["href"]));
                pingURL(analyticsUrl)
            }
        }

        function decisioningTemplate(obj) {
            var decisioningData = {
                api_key: atv.sessionStorage.getItem("mDialog-apiKey"),
                application_key: atv.sessionStorage.getItem("mDialog-appKey"),
                stream_activity_key: atv.sessionStorage.getItem("mDialog-activityMonitorKey"),
                application_version: version,
                os_version: "Unknown",
                os_name: "AppleTV",
                model: "Unknown",
                application_session_unique_identifier: getSessionID(),
                sdk_version: version,
                device_unique_identifier: getDeviceID(),
                wifi: true
            };
            var output = objConcat(decisioningData, obj);
            return parameterize(output)
        }

        function AdBreak(breakInfo) {
            return {
                consumed: breakInfo["consumed"],
                startTime: breakInfo["startTime"],
                endTime: breakInfo["endTime"],
                duration: breakInfo["duration"],
                containsTime: function (t) {
                    return this.startTime <= t && this.endTime > t
                },
                timeRemaining: function (t) {
                    if (this.containsTime(t)) return this.endTime - t;
                    else return 0
                },
                save: function () {
                    var startTime = this["startTime"];
                    var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                    for (var i in adBreaks) {
                        if (adBreaks[i]["startTime"] == startTime) {
                            adBreaks[i] = this
                        }
                    }
                    atv.sessionStorage.setItem("mDialog-breakTimes", JSON.stringify(adBreaks))
                }
            }
        }

        function getOffsetTime(type, playerTime) {
            var startTime = atv.sessionStorage.getItem("mDialog-startTime");
            if (!startTime && type == "live") {
                startTime = playerTime;
                atv.sessionStorage.setItem("mDialog-startTime", startTime)
            } else if (type == "vod") {
                startTime = 0
            }
            return startTime
        }

        function Stream(hypermedia, streamInfo, successCallback) {
            var type = streamInfo["type"];
            var manifestURL = hypermedia["hd_manifest"]["href"];
            var streamKey = streamInfo["stream_key"];
            var duration = streamInfo["duration"];
            var assetKey = streamInfo["assetKey"];
            var watchFrequency = 10;
            var eventsURL = hypermedia["stream_time_events"]["href"];
            var interstitialsURL;
            if (type == "live") {
                var prerollManifestURL = hypermedia["pre_roll_hd_manifest"] ? hypermedia["pre_roll_hd_manifest"]["href"] : null;
                var heartBeatURLs = streamInfo["events"]["heartbeat"] ? streamInfo["events"]["heartbeat"]["tracking"] : null;
                atv.sessionStorage["mDialog-startURLs"] = JSON.stringify(streamInfo["events"]["start"]["tracking"]);
                atv.sessionStorage["mDialog-completeURLs"] = JSON.stringify(streamInfo["events"]["complete"]["tracking"]);
                if (streamInfo["pre_roll"]) {
                    var prerollEvents = streamInfo["pre_roll"]["stream_time_events"];
                    atv.sessionStorage.setItem("mDialog-prerollEventsData", JSON.stringify(prerollEvents));
                    parseBreaks(prerollEvents)
                }
            } else {
                interstitialsURL = hypermedia["interstitials"] ? hypermedia["interstitials"]["href"] : null
            }

            function watchHeartbeatURL(url, interval) {
                pingURL(url);
                var heartbeat = atv.setInterval(function (instance) {
                    pingURL(url)
                }, interval)
            }

            function fetchEvents() {
                var req = new XMLHttpRequest;
                req.onreadystatechange = function () {
                    if (req.readyState == 4 && req.status == 200) {
                        eventsData = JSON.parse(req.responseText);
                        atv.sessionStorage.setItem("mDialog-eventsData", JSON.stringify(eventsData));
                        if (eventsData) {
                            parseBreaks(eventsData)
                        }
                        successCallback()
                    }
                };
                req.open("GET", eventsURL);
                req.send()
            }

            function watchStreamTimeEvents() {
                fetchEvents();
                var streamTimeEventsWatcher = atv.setInterval(function (instance) {
                    fetchEvents()
                }, toMS(watchFrequency))
            }

            function parseBreaks(eventsData) {
                var eventsData = eventsData;
                for (t in eventsData) {
                    var breakEvent = eventsData[t]["ad_break"];
                    var eventTime = parseInt(t, 10);
                    if (breakEvent) {
                        var adBreak = {
                            consumed: false,
                            startTime: eventTime,
                            endTime: eventTime + breakEvent["duration"],
                            duration: breakEvent["duration"]
                        };
                        if (!inCurrenBreaks(adBreak)) {
                            addToBreakData(adBreak)
                        }
                    }
                }
            }

            function inCurrenBreaks(adBreak) {
                var currentBreakTimes = atv.sessionStorage.getItem("mDialog-breakTimes");
                if (currentBreakTimes) {
                    currentBreakTimes = JSON.parse(currentBreakTimes);
                    for (i in currentBreakTimes) {
                        if (currentBreakTimes[i]["startTime"] == adBreak["startTime"]) {
                            return true
                        }
                    }
                }
                return false
            }

            function addToBreakData(adBreak) {
                var currentBreakTimes = atv.sessionStorage.getItem("mDialog-breakTimes");
                currentBreakTimes = JSON.parse(currentBreakTimes) || [];
                currentBreakTimes.push(adBreak);
                atv.sessionStorage.setItem("mDialog-breakTimes", JSON.stringify(currentBreakTimes))
            }

            function initStream() {
                if (type == "live") {
                    watchStreamTimeEvents()
                } else {
                    fetchEvents()
                } if (heartBeatURLs) {
                    for (i in heartBeatURLs) {
                        var url = heartBeatURLs[i]["href"];
                        var interval = heartBeatURLs[i]["interval"];
                        watchHeartbeatURL(unescape(url), toMS(interval))
                    }
                }
            }
            initStream();
            return {
                type: type,
                assetKey: assetKey,
                duration: duration,
                streamKey: streamKey,
                interstitialsURL: interstitialsURL,
                manifestURL: manifestURL,
                prerollManifestURL: prerollManifestURL,
                prerollPlayed: false,
                shouldTrackFeature: false
            }
        }
        return {
            version: version,
            debug: atv.sessionStorage.getItem("mDialog-debug"),
            init: function (options) {
                atv.sessionStorage.setItem("mDialog-subdomain", options.subdomain);
                atv.sessionStorage.setItem("mDialog-apiKey", options.apiKey);
                atv.sessionStorage.setItem("mDialog-appKey", options.appKey);
                atv.sessionStorage.setItem("mDialog-debug", options.debug);
                atv.sessionStorage.setItem("mDialog-activityMonitorKey", options.activityMonitorKey || "");
                if (atv.sessionStorage["mDialog-debug"]) {
                    console.log("--");
                    console.log("-- mDialog Player Initialized v" + this.version);
                    console.log("-- subdomain: " + atv.sessionStorage["mDialog-subdomain"]);
                    console.log("-- apiKey: " + atv.sessionStorage["mDialog-apiKey"]);
                    console.log("-- appKey: " + atv.sessionStorage["mDialog-appKey"]);
                    console.log("-- deviceId: " + getDeviceID());
                    console.log("-- sessionId: " + getSessionID());
                    console.log("--")
                }
            },
            loadedStream: function () {
                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));

                function start() {
                    var startURLs = JSON.parse(atv.sessionStorage.getItem("mDialog-startURLs"));
                    var startEvents = new TrackingEvent(startURLs);
                    startEvents.fire()
                }
                return {
                    type: loadedStream["type"],
                    assetKey: loadedStream["assetKey"],
                    duration: loadedStream["duration"],
                    streamKey: loadedStream["streamKey"],
                    interstitialsURL: loadedStream["interstitialsURL"],
                    manifestURL: loadedStream["manifestURL"],
                    prerollManifestURL: loadedStream["prerollManifestURL"],
                    prerollPlayed: loadedStream["prerollPlayed"],
                    shouldTrackFeature: loadedStream["shouldTrackFeature"],
                    markPrerollPlayed: function () {
                        loadedStream.prerollPlayed = true;
                        atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream)
                    },
                    startFeature: function () {
                        console.log("-- Start Feature Content Tracking");
                        start();
                        loadedStream.shouldTrackFeature = true;
                        atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream)
                    }
                }
            },
            loadStreamForKey: function (assetKey, streamContext, successCallback, failureCallback) {
                var req = new XMLHttpRequest;
                var location;
                var hypermedia;
                var streamInfo;
                var decisioningData = decisioningTemplate();
                var apiKey = atv.sessionStorage.getItem("mDialog-apiKey");
                atv.sessionStorage.removeItem("mDialog-startTime");
                atv.sessionStorage.removeItem("mDialog-breakTimes");
                if (streamContext) {
                    if (streamContext["decisioningData"]) {
                        decisioningData = decisioningTemplate(streamContext["decisioningData"])
                    }
                    if (streamContext["trackingData"]) {
                        atv.sessionStorage.setItem("mDialog-trackingData", JSON.stringify(streamContext["trackingData"]))
                    }
                }
                req.open("POST", streamsURLforAssetKey(assetKey));
                req.setRequestHeader("Authorization", "mDialogAPI " + apiKey);
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        if (req.status == 201) {
                            location = req.getResponseHeader("Location");
                            hypermedia = JSON.parse(req.responseText);
                            if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                                console.log("-- Created stream with location: " + location);
                                if (hypermedia["stream_activity"]) {
                                    console.log("-- Stream Activity can be viewed at: " + hypermedia["stream_activity"]["href"])
                                }
                            }
                            req.open("GET", location);
                            req.send()
                        } else if (req.status == 200) {
                            streamInfo = JSON.parse(req.responseText);
                            streamInfo["assetKey"] = assetKey;
                            var loadedStream = new Stream(hypermedia, streamInfo, function (stream) {
                                atv.sessionStorage["mDialog-loadedStream"] = JSON.stringify(loadedStream);
                                successCallback(loadedStream)
                            })
                        } else {
                            failureCallback()
                        }
                    }
                };
                req.send(decisioningData)
            },
            timeUpdate: function (playerTime, breakCallback) {

                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));
                var eventsData = loadedStream.shouldTrackFeature ? JSON.parse(atv.sessionStorage.getItem("mDialog-eventsData")) : JSON.parse(atv.sessionStorage.getItem("mDialog-prerollEventsData"));
                // console.log("eventsData = ", JSON.stringify(eventsData));
                var breakTimes = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                var timeOffset = getOffsetTime(loadedStream.type, playerTime);
                if (JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))) {
                    console.log(playerTime)
                }
                for (i in breakTimes) {
                    var ab = new AdBreak(breakTimes[i]);
                    if (ab.containsTime(playerTime)) {
                        var timeRemaining = ab.timeRemaining(playerTime);
                        breakCallback(ab, timeRemaining);
                        if (timeRemaining == 1 && ab["consumed"] == false) {
                            ab["consumed"] = true;
                            ab.save()
                        }
                    }
                }
                if (eventsData[playerTime]) {
                    var currentEvent = new TrackingEvent(eventsData[playerTime]["tracking"]);
                    currentEvent.fire()
                }
            },
            didStopPlaying: function () {
                var loadedStream = JSON.parse(atv.sessionStorage.getItem("mDialog-loadedStream"));
                if (loadedStream.shouldTrackFeature) var completeURLs = JSON.parse(atv.sessionStorage.getItem("mDialog-completeURLs"));
                var completeEvents = new TrackingEvent(completeURLs);
                completeEvents.fire()
            },
            nearestPreviousAdBreak: function (timeIntervalSec) {
                var currentTime = Math.floor(timeIntervalSec);
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                adBreaks.sort(function (a, b) {
                    return parseFloat(a["startTime"]) - parseFloat(b["startTime"])
                });
                var closestBreak;
                for (var i in adBreaks) {
                    if (currentTime >= adBreaks[i]["startTime"]) {
                        if (closestBreak && adBreaks[i]["startTime"] > closestBreak["startTime"]) {
                            closestBreak = adBreaks[i]
                        } else {
                            closestBreak = adBreaks[i]
                        }
                    }
                }
                return closestBreak
            },
            streamTimeWithoutAds: function (currentTime) {
                var streamTimeWithoutAds;
                var totalAdDuration = 0;
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                for (var i in adBreaks) {
                    if (adBreaks[i]["startTime"] < currentTime) {
                        totalAdDuration = totalAdDuration + adBreaks[i]["duration"]
                    }
                }
                streamTimeWithoutAds = currentTime - totalAdDuration;
                return streamTimeWithoutAds >= 0 ? streamTimeWithoutAds : 0
            },
            streamTimeWithAds: function (streamTimeWithoutAds) {
                var streamTimeWithAds;
                var totalAdDuration = 0;
                var adBreaks = JSON.parse(atv.sessionStorage.getItem("mDialog-breakTimes"));
                for (var i in adBreaks) {
                    if (adBreaks[i]["startTime"] < streamTimeWithoutAds) {
                        totalAdDuration = totalAdDuration + adBreaks[i]["duration"]
                    }
                }
                streamTimeWithAds = streamTimeWithoutAds + totalAdDuration;
                return streamTimeWithAds >= 0 ? streamTimeWithAds : 0
            }
        }
    }()
};
define("mDialog-ATVPlayer-2.3-patch-2.1", function(){});

if( ! aetn ) { var aetn = {}; }
if( ! aetn.group ) { aetn.group = {}; }

aetn.group = 'fyi';
define("aetn/env/fyi", function(){});

var network = null;
//var domain = 'player.aetndigital.com/pservice/appletv';
var domain = 'appletv.aetndigital.com';
if( aetn.group === 'history' ) {
  network = 'HISTORY';
  aetn.globalContext = {
    brand : 'HISTORY and H2',
    watchlist : {
      queuekey: 'history_watchlist',
      progresskey: 'history_progresslist',
      siteName: 'history'
    },
    adobePass :  {
      localStorageKey : 'history_uuid',
      endPoint: 'https://api.auth.adobe.com',
      requestor_id: 'HISTORY',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'history.com/activate',
      auth_retry_max: 10
    },
    mdialogOptions : { //todo: undate the values later
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '64dd4e8fa3bf2515d1e43acfaf49e3f0',
      debug: false
    },
    omniture : {
      brand: 'History',
      suiteIds : 'aetnappletvhistory,aetnwatchglobal',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'HIS'
    },
    mvpdHashConfigUrl : 'https://watchapprokuhistorymvpduser:BAveRTC9@mobile-a.akamaihd.net/configs/watchapp/Roku/History/mvpd_config.json',
    mpxFeedPath : 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles/episode/history',
    urlPath: 'https://' + domain +  '/history'
  };

} else if( aetn.group === 'ae' ) {

  network = 'AETV';
  aetn.globalContext = {
    brand : 'A&E',
    watchlist : {
      queuekey: 'aetv_watchlist',
      progresskey: 'aetv_progresslist',
      siteName: 'ae'
    },
    adobePass :  {
      localStorageKey : 'aetv_uuid',
      endPoint: 'https://api.auth.adobe.com',
      requestor_id: 'AETV',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'aetv.com/activate',
      auth_retry_max: 10
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '42a1ef88dfa8c2cab31be18ec3a2691a',
      debug: false
    },
    omniture : {
      brand : 'A&E',
      suiteIds : 'aetnappletvae,aetnwatchglobal',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'AETV'
    },
    mvpdHashConfigUrl : 'https://watchapprokuaemvpduser:Whtu8NSB@mobile-a.akamaihd.net/configs/watchapp/Roku/AE/mvpd_config.json',
    mpxFeedPath : 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles2/episode/ae',
    urlPath: 'https://' + domain +  '/ae'
  };

} else if( aetn.group === 'mlt' ) {

  network = 'MLT';
  aetn.globalContext = {
    brand : 'Lifetime',
    watchlist : {
      queuekey: 'mlt_watchlist',
      progresskey: 'mlt_progresslist',
      siteName: 'mlt'
    },
    inProcessVideosList : {
      key: 'mlt_inprocessvideo'
    },
    adobePass : {
      localStorageKey: 'mlt_uuid',
      endPoint: 'https://api.auth.adobe.com',
      requestor_id: 'LIFETIME',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'mylifetime.com/activate',
      auth_retry_max: 10
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: '42a1ef88dfa8c2cab31be18ec3a2691a',
      debug: false
    },
    omniture : {
      brand : 'Lifetime',
      suiteIds : 'aetnappletvlifetime,aetnwatchglobal',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'MYL'
    },
    mvpdHashConfigUrl : 'https://watchapprokumltmvpduser:DYB4mvtL@mobile-a.akamaihd.net/configs/watchapp/Roku/MLT/mvpd_config.json',
    mpxFeedPath: 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles/episode/mlt',
    urlPath: 'https://' + domain +  '/mlt'
  };

} else if( aetn.group === 'fyi' ) {

  network = 'FYI';
  aetn.globalContext = {
    brand : 'FYI',
    watchlist : {
      queuekey: 'aetv_watchlist',
      progresskey: 'aetv_progresslist',
      siteName: 'fyi'
    },
    adobePass :  {
      localStorageKey : 'fyi_uuid',
      endPoint: 'https://api.auth.adobe.com',
      requestor_id: 'FYI',
      public_key: 'mowmBjtUFgjoVUlklRtQKFNBJYXnVo2A',
      private_key: '5c34ezGcB4VF4wnY',
      auth_page: 'fyi.tv/activate',
      auth_retry_max: 10
    },
    mdialogOptions : {
      subdomain: 'aetn-vod',
      apiKey: '599b88193e3f5a6c96f7a362a75b34ad',
      appKey: 'c660aa95f92995258cd2e11daf5234f4',
      debug: true,
      activityMonitorKey: '64524e36b96b6f547b3e9dce101bf8bd'
    },
    omniture : {
      brand : 'FYI',
      suiteIds : 'aetnappletvfyi,aetnwatchglobal',
      server : 'metrics.aetn.com',
      secureServer : 'smetrics.aetn.com',
      brandPrefix: 'FYI'
    },
    mvpdHashConfigUrl : 'https://watchappiosfyimvpduser:5kcdpq5s@mobile-a.akamaihd.net/configs/watchapp/iOS/FYI/mvpd_config.json',
    mpxFeedPath : 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles/episode/fyi',
    urlPath: 'https://' + domain +  '/fyi'
  };

}
aetn.globalContext.tpTokenAuthUrl = 'https://servicesaetn-a.akamaihd.net/jservice/video/components/get-signed-signature';
aetn.globalContext.visitorIDKey = 'visitorID';
aetn.globalContext.krux = {
                            disable: false,
                            server: 'https://beacon.krxd.net/'
                          };
define("aetn/env/prod", function(){});

/*!
  * klass: a classical JS OOP façade
  * https://github.com/ded/klass
  * License MIT (c) Dustin Diaz & Jacob Thornton 2012
  */

!function (name, context, definition) {
  if (typeof define == 'function') define('klass',definition)
  else if (typeof module != 'undefined') module.exports = definition()
  else context[name] = definition()
}('klass', this, function () {
  var context = this
    , old = context.klass
    , f = 'function'
    , fnTest = /xyz/.test(function () {xyz}) ? /\bsupr\b/ : /.*/
    , proto = 'prototype'

  function klass(o) {
    return extend.call(isFn(o) ? o : function () {}, o, 1)
  }

  function isFn(o) {
    return typeof o === f
  }

  function wrap(k, fn, supr) {
    return function () {
      var tmp = this.supr
      this.supr = supr[proto][k]
      var undef = {}.fabricatedUndefined
      var ret = undef
      try {
        ret = fn.apply(this, arguments)
      } finally {
        this.supr = tmp
      }
      return ret
    }
  }

  function process(what, o, supr) {
    for (var k in o) {
      if (o.hasOwnProperty(k)) {
        what[k] = isFn(o[k])
          && isFn(supr[proto][k])
          && fnTest.test(o[k])
          ? wrap(k, o[k], supr) : o[k]
      }
    }
  }

  function extend(o, fromSub) {
    // must redefine noop each time so it doesn't inherit from previous arbitrary classes
    function noop() {}
    noop[proto] = this[proto]
    var supr = this
      , prototype = new noop()
      , isFunction = isFn(o)
      , _constructor = isFunction ? o : this
      , _methods = isFunction ? {} : o
    function fn() {
      if (this.initialize) this.initialize.apply(this, arguments)
      else {
        fromSub || isFunction && supr.apply(this, arguments)
        _constructor.apply(this, arguments)
      }
    }

    fn.methods = function (o) {
      process(prototype, o, supr)
      fn[proto] = prototype
      return this
    }

    fn.methods.call(fn, _methods).prototype.constructor = fn

    fn.extend = arguments.callee
    fn[proto].implement = fn.statics = function (o, optFn) {
      o = typeof o == 'string' ? (function () {
        var obj = {}
        obj[o] = optFn
        return obj
      }()) : o
      process(this, o, supr)
      return this
    }

    return fn
  }

  klass.noConflict = function () {
    context.klass = old
    return this
  }

  return klass
});
define('aetn/appletv/tve/adobepass',['klass'], function(klass){

  return klass({
    
    initialize: function() {

    },

    deactivateDevice: function(cb) {

      var url  = aetn.globalContext.adobePass.endPoint + '/api/v1/logout?deviceId=' + aetnUtils.getVisitorID() + "&resource=" + aetn.globalContext.adobePass.requestor_id;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'DELETE', aetn.globalContext.adobePass.requestor_id, '/api/v1/logout');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "method": "DELETE",
        "headers": headers,
        "success": function(xhr) {
          console.log('deactivateDevice Success Status: ', xhr);
          cb();
        },
        "failure": function(status, xhr) {
          console.log('deactivateDevice Failure Status: ', status, xhr);
        }
      });

    },

    getAuthenticationToken: function(cbSuccess, cbFailure) {

      var url  = aetn.globalContext.adobePass.endPoint + '/api/v1/tokens/authn?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId='+ aetnUtils.getVisitorID();
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/tokens/authn');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function(xhr) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess( response );
        },
        "failure": function(status, xhr) {
          console.log('getAuthenticationToken Failure Status: ', status, xhr);
          cbFailure(status, xhr);
        }
      });

    },

    getAuthenticationTokenByCode: function(code, tryCount, cbSuccess, cbFailure) {
      var self = this;
      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/authenticate/' + code + '.json?requestor=' + aetn.globalContext.adobePass.requestor_id;

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers" :  {},
        "body" : {},
        "success": function( xhr ) {
          console.log('getAuthenticationTokenByCode success using', code, xhr.responseText);
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response);
        },
        "failure": function( status, xhr ) {
          console.log('getAuthenticationTokenByCode Failure Status:', status);
          if (tryCount < aetn.globalContext.adobePass.auth_retry_max) {
            console.log('retry count: ', tryCount);
            tryCount = tryCount + 1;
            atv.setTimeout(self.getAuthenticationTokenByCode.bind(self), 15000, code, tryCount, cbSuccess, cbFailure);
          }else {
            console.log('getAuthenticationTokenByCode Failure Status: Timeout');
            cbFailure("The Activation Process Timeout.");
          }
        }
      });
    },

    getAuthorization: function( video, cbSuccess, cbFailure ) {

      var resource = '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/"><channel><title>' + video.network.replace('A&E', 'AETV').toUpperCase() + '</title><item><title>' + video.title + '</title><guid>' + video.programID + '</guid></item></channel></rss>';
      resource = encodeURI( resource );

      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/authorize?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId=' + aetnUtils.getVisitorID() + '&resource=' + resource;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/authorize');

      var headers = {
        Authorization : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response.resource);
        },
        "failure": function( status, xhr ) {
          console.log("failure getting authorization", status, xhr.responseText);
          if (cbFailure) {
            try {
              var response = JSON.parse(xhr.responseText);
              if(response.details && response.details !== '' && response.details !== 'network') {
                cbFailure(response.details);
              } else {
                cbFailure('You are not authorized to see the videos');
              }
            } catch(e) {
              cbFailure('You are not authorized to see the videos');
            }
          }
        }
      });

    },

    getShortMediaToken: function( video, cbSuccess, cbFailure ) {

      var resource = '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/"><channel><title>' + video.network.replace('A&E', 'AETV').toUpperCase() + '</title><item><title>' + video.title + '</title><guid>' + video.programID + '</guid></item></channel></rss>';
      resource = encodeURI( resource );
      var url = aetn.globalContext.adobePass.endPoint + '/api/v1/mediatoken?format=json&requestor=' + aetn.globalContext.adobePass.requestor_id + '&deviceId=' + aetnUtils.getVisitorID() + '&resource=' + resource;
      var auth = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'GET', aetn.globalContext.adobePass.requestor_id, '/api/v1/mediatoken');

      var headers = {
        'Authorization' : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          cbSuccess(response.serializedToken);
        },
        "failure": function( status, xhr ) {
          try {
            var response = JSON.parse(xhr.responseText);
            if(response.details && response.details !== '' && response.details !== 'network') {
              cbFailure(response.details);
            } else {
              cbFailure('We were unable to contact A+E Networks, please try again.');
            }
          } catch(e) {
            cbFailure('We were unable to contact A+E Networks, please try again.');
          }
        }
      });

    },

    getNewRegistrationCode: function(cb) {

      var url     = aetn.globalContext.adobePass.endPoint + '/reggie/v1/' + aetn.globalContext.adobePass.requestor_id + '/regcode';
      var payload = 'format=json&ttl=36000&deviceId=' + aetnUtils.getVisitorID();
      var auth    = this.getAuthorizationHeader(aetn.globalContext.adobePass.public_key, aetn.globalContext.adobePass.private_key, 'POST', aetn.globalContext.adobePass.requestor_id, '/reggie/v1/' + aetn.globalContext.adobePass.requestor_id + '/regcode');
      var headers = {
        'Authorization' : auth
      };

      var ajax = new ATVUtils.Ajax({
        "url": url,
        "method": "POST",
        "data": payload,
        "headers": headers,
        "success": function( xhr ) {
          var response = JSON.parse(xhr.responseText);
          console.log('getNewRegistrationCode: success:', aetnUtils.getVisitorID());
          cb(response.code);
        },
        "failure": function( status, xhr ) {
          console.log('getNewRegistrationCode Request Status:', status, xhr);
        }
      });

    },

    getAuthorizationHeader: function( public_key, private_key, verb, requestor_id, request_uri ) {
      var request_time  = new Date().getTime().toString();
      var nonce         = this.generateUUID();
      var msg           = verb + ' requestor_id=' + requestor_id + ', nonce=' + nonce + ', signature_method=HMAC-SHA1, request_time=' + request_time + ', request_uri=' + request_uri;
      var hash          = CryptoJS.HmacSHA1(msg, private_key);
      return msg + ', public_key=' + public_key + ', signature=' + hash.toString(CryptoJS.enc.Base64);
    },

    generateUUID: function() {

      var uuid = '';
      var o = null;
      for(var i = 1; i <= 32; i++) {
        o = getRandomInt(1,16);
        if (o <= 10) {
          o = o + 47;
        } else {
          o = o + 96 - 10;
        }
        uuid = uuid + String.fromCharCode(o);
      }

      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }

      return uuid;
    }

  });

});
//     Underscore.js 1.5.2
//     http://underscorejs.org
//     (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` in the browser, or `exports` on the server.
  var root = this;

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Establish the object that gets returned to break out of a loop iteration.
  var breaker = {};

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  // Create quick reference variables for speed access to core prototypes.
  var
    push             = ArrayProto.push,
    slice            = ArrayProto.slice,
    concat           = ArrayProto.concat,
    toString         = ObjProto.toString,
    hasOwnProperty   = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var
    nativeForEach      = ArrayProto.forEach,
    nativeMap          = ArrayProto.map,
    nativeReduce       = ArrayProto.reduce,
    nativeReduceRight  = ArrayProto.reduceRight,
    nativeFilter       = ArrayProto.filter,
    nativeEvery        = ArrayProto.every,
    nativeSome         = ArrayProto.some,
    nativeIndexOf      = ArrayProto.indexOf,
    nativeLastIndexOf  = ArrayProto.lastIndexOf,
    nativeIsArray      = Array.isArray,
    nativeKeys         = Object.keys,
    nativeBind         = FuncProto.bind;

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for the old `require()` API. If we're in
  // the browser, add `_` as a global object via a string identifier,
  // for Closure Compiler "advanced" mode.
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.5.2';

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles objects with the built-in `forEach`, arrays, and raw objects.
  // Delegates to **ECMAScript 5**'s native `forEach` if available.
  var each = _.each = _.forEach = function(obj, iterator, context) {
    if (obj == null) return;
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, length = obj.length; i < length; i++) {
        if (iterator.call(context, obj[i], i, obj) === breaker) return;
      }
    } else {
      var keys = _.keys(obj);
      for (var i = 0, length = keys.length; i < length; i++) {
        if (iterator.call(context, obj[keys[i]], keys[i], obj) === breaker) return;
      }
    }
  };

  // Return the results of applying the iterator to each element.
  // Delegates to **ECMAScript 5**'s native `map` if available.
  _.map = _.collect = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
    each(obj, function(value, index, list) {
      results.push(iterator.call(context, value, index, list));
    });
    return results;
  };

  var reduceError = 'Reduce of empty array with no initial value';

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`. Delegates to **ECMAScript 5**'s native `reduce` if available.
  _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduce && obj.reduce === nativeReduce) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
    }
    each(obj, function(value, index, list) {
      if (!initial) {
        memo = value;
        initial = true;
      } else {
        memo = iterator.call(context, memo, value, index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // The right-associative version of reduce, also known as `foldr`.
  // Delegates to **ECMAScript 5**'s native `reduceRight` if available.
  _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
    }
    var length = obj.length;
    if (length !== +length) {
      var keys = _.keys(obj);
      length = keys.length;
    }
    each(obj, function(value, index, list) {
      index = keys ? keys[--length] : --length;
      if (!initial) {
        memo = obj[index];
        initial = true;
      } else {
        memo = iterator.call(context, memo, obj[index], index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, iterator, context) {
    var result;
    any(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) {
        result = value;
        return true;
      }
    });
    return result;
  };

  // Return all the elements that pass a truth test.
  // Delegates to **ECMAScript 5**'s native `filter` if available.
  // Aliased as `select`.
  _.filter = _.select = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
    each(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, iterator, context) {
    return _.filter(obj, function(value, index, list) {
      return !iterator.call(context, value, index, list);
    }, context);
  };

  // Determine whether all of the elements match a truth test.
  // Delegates to **ECMAScript 5**'s native `every` if available.
  // Aliased as `all`.
  _.every = _.all = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = true;
    if (obj == null) return result;
    if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
    each(obj, function(value, index, list) {
      if (!(result = result && iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if at least one element in the object matches a truth test.
  // Delegates to **ECMAScript 5**'s native `some` if available.
  // Aliased as `any`.
  var any = _.some = _.any = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = false;
    if (obj == null) return result;
    if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
    each(obj, function(value, index, list) {
      if (result || (result = iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if the array or object contains a given value (using `===`).
  // Aliased as `include`.
  _.contains = _.include = function(obj, target) {
    if (obj == null) return false;
    if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
    return any(obj, function(value) {
      return value === target;
    });
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = function(obj, method) {
    var args = slice.call(arguments, 2);
    var isFunc = _.isFunction(method);
    return _.map(obj, function(value) {
      return (isFunc ? method : value[method]).apply(value, args);
    });
  };

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, function(value){ return value[key]; });
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs, first) {
    if (_.isEmpty(attrs)) return first ? void 0 : [];
    return _[first ? 'find' : 'filter'](obj, function(value) {
      for (var key in attrs) {
        if (attrs[key] !== value[key]) return false;
      }
      return true;
    });
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.where(obj, attrs, true);
  };

  // Return the maximum element or (element-based computation).
  // Can't optimize arrays of integers longer than 65,535 elements.
  // See [WebKit Bug 80797](https://bugs.webkit.org/show_bug.cgi?id=80797)
  _.max = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.max.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return -Infinity;
    var result = {computed : -Infinity, value: -Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed > result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.min.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return Infinity;
    var result = {computed : Infinity, value: Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed < result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Shuffle an array, using the modern version of the 
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisherâ€“Yates_shuffle).
  _.shuffle = function(obj) {
    var rand;
    var index = 0;
    var shuffled = [];
    each(obj, function(value) {
      rand = _.random(index++);
      shuffled[index - 1] = shuffled[rand];
      shuffled[rand] = value;
    });
    return shuffled;
  };

  // Sample **n** random values from an array.
  // If **n** is not specified, returns a single random element from the array.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (arguments.length < 2 || guard) {
      return obj[_.random(obj.length - 1)];
    }
    return _.shuffle(obj).slice(0, Math.max(0, n));
  };

  // An internal function to generate lookup iterators.
  var lookupIterator = function(value) {
    return _.isFunction(value) ? value : function(obj){ return obj[value]; };
  };

  // Sort the object's values by a criterion produced by an iterator.
  _.sortBy = function(obj, value, context) {
    var iterator = lookupIterator(value);
    return _.pluck(_.map(obj, function(value, index, list) {
      return {
        value: value,
        index: index,
        criteria: iterator.call(context, value, index, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior) {
    return function(obj, value, context) {
      var result = {};
      var iterator = value == null ? _.identity : lookupIterator(value);
      each(obj, function(value, index) {
        var key = iterator.call(context, value, index, obj);
        behavior(result, key, value);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, key, value) {
    (_.has(result, key) ? result[key] : (result[key] = [])).push(value);
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, key, value) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, key) {
    _.has(result, key) ? result[key]++ : result[key] = 1;
  });

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iterator, context) {
    iterator = iterator == null ? _.identity : lookupIterator(iterator);
    var value = iterator.call(context, obj);
    var low = 0, high = array.length;
    while (low < high) {
      var mid = (low + high) >>> 1;
      iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
    }
    return low;
  };

  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (obj.length === +obj.length) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return (obj.length === +obj.length) ? obj.length : _.keys(obj).length;
  };

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null) return void 0;
    return (n == null) || guard ? array[0] : slice.call(array, 0, n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N. The **guard** check allows it to work with
  // `_.map`.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array. The **guard** check allows it to work with `_.map`.
  _.last = function(array, n, guard) {
    if (array == null) return void 0;
    if ((n == null) || guard) {
      return array[array.length - 1];
    } else {
      return slice.call(array, Math.max(array.length - n, 0));
    }
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array. The **guard**
  // check allows it to work with `_.map`.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, (n == null) || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, _.identity);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, output) {
    if (shallow && _.every(input, _.isArray)) {
      return concat.apply(output, input);
    }
    each(input, function(value) {
      if (_.isArray(value) || _.isArguments(value)) {
        shallow ? push.apply(output, value) : flatten(value, shallow, output);
      } else {
        output.push(value);
      }
    });
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, []);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = function(array) {
    return _.difference(array, slice.call(arguments, 1));
  };

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iterator, context) {
    if (_.isFunction(isSorted)) {
      context = iterator;
      iterator = isSorted;
      isSorted = false;
    }
    var initial = iterator ? _.map(array, iterator, context) : array;
    var results = [];
    var seen = [];
    each(initial, function(value, index) {
      if (isSorted ? (!index || seen[seen.length - 1] !== value) : !_.contains(seen, value)) {
        seen.push(value);
        results.push(array[index]);
      }
    });
    return results;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = function() {
    return _.uniq(_.flatten(arguments, true));
  };

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var rest = slice.call(arguments, 1);
    return _.filter(_.uniq(array), function(item) {
      return _.every(rest, function(other) {
        return _.indexOf(other, item) >= 0;
      });
    });
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
    var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
    return _.filter(array, function(value){ return !_.contains(rest, value); });
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = function() {
    var length = _.max(_.pluck(arguments, "length").concat(0));
    var results = new Array(length);
    for (var i = 0; i < length; i++) {
      results[i] = _.pluck(arguments, '' + i);
    }
    return results;
  };

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values.
  _.object = function(list, values) {
    if (list == null) return {};
    var result = {};
    for (var i = 0, length = list.length; i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // If the browser doesn't supply us with indexOf (I'm looking at you, **MSIE**),
  // we need this function. Return the position of the first occurrence of an
  // item in an array, or -1 if the item is not included in the array.
  // Delegates to **ECMAScript 5**'s native `indexOf` if available.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = function(array, item, isSorted) {
    if (array == null) return -1;
    var i = 0, length = array.length;
    if (isSorted) {
      if (typeof isSorted == 'number') {
        i = (isSorted < 0 ? Math.max(0, length + isSorted) : isSorted);
      } else {
        i = _.sortedIndex(array, item);
        return array[i] === item ? i : -1;
      }
    }
    if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
    for (; i < length; i++) if (array[i] === item) return i;
    return -1;
  };

  // Delegates to **ECMAScript 5**'s native `lastIndexOf` if available.
  _.lastIndexOf = function(array, item, from) {
    if (array == null) return -1;
    var hasIndex = from != null;
    if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) {
      return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
    }
    var i = (hasIndex ? from : array.length);
    while (i--) if (array[i] === item) return i;
    return -1;
  };

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (arguments.length <= 1) {
      stop = start || 0;
      start = 0;
    }
    step = arguments[2] || 1;

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var idx = 0;
    var range = new Array(length);

    while(idx < length) {
      range[idx++] = start;
      start += step;
    }

    return range;
  };

  // Function (ahem) Functions
  // ------------------

  // Reusable constructor function for prototype setting.
  var ctor = function(){};

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = function(func, context) {
    var args, bound;
    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
    if (!_.isFunction(func)) throw new TypeError;
    args = slice.call(arguments, 2);
    return bound = function() {
      if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
      ctor.prototype = func.prototype;
      var self = new ctor;
      ctor.prototype = null;
      var result = func.apply(self, args.concat(slice.call(arguments)));
      if (Object(result) === result) return result;
      return self;
    };
  };

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context.
  _.partial = function(func) {
    var args = slice.call(arguments, 1);
    return function() {
      return func.apply(this, args.concat(slice.call(arguments)));
    };
  };

  // Bind all of an object's methods to that object. Useful for ensuring that
  // all callbacks defined on an object belong to it.
  _.bindAll = function(obj) {
    var funcs = slice.call(arguments, 1);
    if (funcs.length === 0) throw new Error("bindAll must be passed function names");
    each(funcs, function(f) { obj[f] = _.bind(obj[f], obj); });
    return obj;
  };

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memo = {};
    hasher || (hasher = _.identity);
    return function() {
      var key = hasher.apply(this, arguments);
      return _.has(memo, key) ? memo[key] : (memo[key] = func.apply(this, arguments));
    };
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = function(func, wait) {
    var args = slice.call(arguments, 2);
    return setTimeout(function(){ return func.apply(null, args); }, wait);
  };

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = function(func) {
    return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
  };

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    options || (options = {});
    var later = function() {
      previous = options.leading === false ? 0 : new Date;
      timeout = null;
      result = func.apply(context, args);
    };
    return function() {
      var now = new Date;
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0) {
        clearTimeout(timeout);
        timeout = null;
        previous = now;
        result = func.apply(context, args);
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, args, context, timestamp, result;
    return function() {
      context = this;
      args = arguments;
      timestamp = new Date();
      var later = function() {
        var last = (new Date()) - timestamp;
        if (last < wait) {
          timeout = setTimeout(later, wait - last);
        } else {
          timeout = null;
          if (!immediate) result = func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
      if (callNow) result = func.apply(context, args);
      return result;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = function(func) {
    var ran = false, memo;
    return function() {
      if (ran) return memo;
      ran = true;
      memo = func.apply(this, arguments);
      func = null;
      return memo;
    };
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return function() {
      var args = [func];
      push.apply(args, arguments);
      return wrapper.apply(this, args);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var funcs = arguments;
    return function() {
      var args = arguments;
      for (var i = funcs.length - 1; i >= 0; i--) {
        args = [funcs[i].apply(this, args)];
      }
      return args[0];
    };
  };

  // Returns a function that will only be executed after being called N times.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Object Functions
  // ----------------

  // Retrieve the names of an object's properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`
  _.keys = nativeKeys || function(obj) {
    if (obj !== Object(obj)) throw new TypeError('Invalid object');
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = new Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Convert an object into a list of `[key, value]` pairs.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = new Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    each(keys, function(key) {
      if (key in obj) copy[key] = obj[key];
    });
    return copy;
  };

   // Return a copy of the object without the blacklisted properties.
  _.omit = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    for (var key in obj) {
      if (!_.contains(keys, key)) copy[key] = obj[key];
    }
    return copy;
  };

  // Fill in a given object with default properties.
  _.defaults = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          if (obj[prop] === void 0) obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Internal recursive comparison function for `isEqual`.
  var eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a == 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null) return a === b;
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className != toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, dates, and booleans are compared by value.
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return a == String(b);
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive. An `egal` comparison is performed for
        // other numeric values.
        return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b);
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a == +b;
      // RegExps are compared by their source patterns and flags.
      case '[object RegExp]':
        return a.source == b.source &&
               a.global == b.global &&
               a.multiline == b.multiline &&
               a.ignoreCase == b.ignoreCase;
    }
    if (typeof a != 'object' || typeof b != 'object') return false;
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] == a) return bStack[length] == b;
    }
    // Objects with different constructors are not equivalent, but `Object`s
    // from different frames are.
    var aCtor = a.constructor, bCtor = b.constructor;
    if (aCtor !== bCtor && !(_.isFunction(aCtor) && (aCtor instanceof aCtor) &&
                             _.isFunction(bCtor) && (bCtor instanceof bCtor))) {
      return false;
    }
    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);
    var size = 0, result = true;
    // Recursively compare objects and arrays.
    if (className == '[object Array]') {
      // Compare array lengths to determine if a deep comparison is necessary.
      size = a.length;
      result = size == b.length;
      if (result) {
        // Deep compare the contents, ignoring non-numeric properties.
        while (size--) {
          if (!(result = eq(a[size], b[size], aStack, bStack))) break;
        }
      }
    } else {
      // Deep compare objects.
      for (var key in a) {
        if (_.has(a, key)) {
          // Count the expected number of properties.
          size++;
          // Deep compare each member.
          if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
        }
      }
      // Ensure that both objects contain the same number of properties.
      if (result) {
        for (key in b) {
          if (_.has(b, key) && !(size--)) break;
        }
        result = !size;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return result;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b, [], []);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
    for (var key in obj) if (_.has(obj, key)) return false;
    return true;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) == '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    return obj === Object(obj);
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp.
  each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) == '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return !!(obj && _.has(obj, 'callee'));
    };
  }

  // Optimize `isFunction` if appropriate.
  if (typeof (/./) !== 'function') {
    _.isFunction = function(obj) {
      return typeof obj === 'function';
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
  _.isNaN = function(obj) {
    return _.isNumber(obj) && obj != +obj;
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) == '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, key) {
    return hasOwnProperty.call(obj, key);
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iterators.
  _.identity = function(value) {
    return value;
  };

  // Run a function **n** times.
  _.times = function(n, iterator, context) {
    var accum = Array(Math.max(0, n));
    for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // List of HTML entities for escaping.
  var entityMap = {
    escape: {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;'
    }
  };
  entityMap.unescape = _.invert(entityMap.escape);

  // Regexes containing the keys and values listed immediately above.
  var entityRegexes = {
    escape:   new RegExp('[' + _.keys(entityMap.escape).join('') + ']', 'g'),
    unescape: new RegExp('(' + _.keys(entityMap.unescape).join('|') + ')', 'g')
  };

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  _.each(['escape', 'unescape'], function(method) {
    _[method] = function(string) {
      if (string == null) return '';
      return ('' + string).replace(entityRegexes[method], function(match) {
        return entityMap[method][match];
      });
    };
  });

  // If the value of the named `property` is a function then invoke it with the
  // `object` as context; otherwise, return it.
  _.result = function(object, property) {
    if (object == null) return void 0;
    var value = object[property];
    return _.isFunction(value) ? value.call(object) : value;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return result.call(this, func.apply(_, args));
      };
    });
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\t':     't',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  _.template = function(text, data, settings) {
    var render;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = new RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset)
        .replace(escaper, function(match) { return '\\' + escapes[match]; });

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      }
      if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      }
      if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }
      index = offset + match.length;
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + "return __p;\n";

    try {
      render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    if (data) return render(data, _);
    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled function source as a convenience for precompilation.
    template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function, which will delegate to the wrapper.
  _.chain = function(obj) {
    return _(obj).chain();
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var result = function(obj) {
    return this._chain ? _(obj).chain() : obj;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name == 'shift' || name == 'splice') && obj.length === 0) delete obj[0];
      return result.call(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return result.call(this, method.apply(this._wrapped, arguments));
    };
  });

  _.extend(_.prototype, {

    // Start chaining a wrapped Underscore object.
    chain: function() {
      this._chain = true;
      return this;
    },

    // Extracts the result from a wrapped and chained object.
    value: function() {
      return this._wrapped;
    }

  });

}).call(this);
define("underscore", (function (global) {
    return function () {
        var ret, fn;
        return ret || global._;
    };
}(this)));

define('aetn/appletv/ui/watchlist',['klass','underscore'], function(klass){

  return klass({
    
    watchlistKey : null,
    listElement : null,

    initialize: function(pageId, listType) {
      this.listType = listType;
      this.watchlistKey = (listType === 'progress')? aetn.globalContext.watchlist.progresskey : aetn.globalContext.watchlist.queuekey;
      this.listElement = (listType === 'progress')? 'inprogress' : 'queue';
      if (pageId === 'com.aetn.watchlist') {
        this.renderUI((listType === 'progress')? true : false);
      }
    },

    renderUI: function(isProgressList){
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      var queue = document.getElementById(this.listElement);

      if (!this.checkIfWatchlistEmpty(watchlist, isProgressList)) {
        var oldQueue = queue.getElementsByTagName('sixteenByNinePoster');
        _.each(oldQueue, function(element){
          element.removeFromParent();
        });

        watchlist.reverse();

        for(var i = 0; i < watchlist.length; i++) {
          if (this.watchlistKey === aetn.globalContext.watchlist.progresskey && watchlist[i].finished === 'true')
            continue;

          var newChild = document.makeElementNamed('sixteenByNinePoster');
          newChild.setAttribute('id',  i );
          newChild.setAttribute('alwaysShowTitles', 'true');
          newChild.setAttribute('onSelect', 'aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + "/video/" + ' + watchlist[i].id + ')');
          newChild.setAttribute('onPlay', 'aetnUtils.loadVideo("' + watchlist[i].id + '", "' + watchlist[i].isBehindWall + '")');

          var titleElement = document.makeElementNamed('title');
          titleElement.textContent = (watchlist[i].seriesName)? watchlist[i].seriesName : '';
          newChild.appendChild(titleElement);
          
          var subtitleElement = document.makeElementNamed('subtitle');
          subtitleElement.textContent = watchlist[i].title;
          newChild.appendChild(subtitleElement);

          var image = document.makeElementNamed('image');
          image.setAttribute('src720', watchlist[i].image);
          image.setAttribute('src1080', watchlist[i].image);
          newChild.appendChild(image);
          queue.appendChild(newChild);
        }
      }
    },

    handleWatchlistButtonClicked: function(videoTitle, thePlatformId, image, seriesName, isBehindWall) {

      try {
        var button = document.getElementById('add');
        var title = button.getElementByTagName('title');
      
        if (title.textContent === 'Add') {
          this.addVideoToWatchlist(thePlatformId, {
            title: videoTitle
          , id: thePlatformId
          , image: image
          , seriesName: seriesName
          , isBehindWall: isBehindWall
          , finished : 'false'
          });
          title.textContent = 'Remove';
          button.setAttribute('accessibilityLabel', 'Remove from watch list');
        } else {
          this.removeVideoFromWatchlist(thePlatformId);
          title.textContent = 'Add';
          button.setAttribute('accessibilityLabel', 'Add to watch list');
        }
      
      } catch(error){
        console.log('Caught exception trying to toggle DOM element: ' + error);
      }
    },

    getVideoFromWatchlist: function(thePlatformId) {
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      if (watchlist) {
        return _.findWhere(watchlist, {
          id : thePlatformId
        });
      }
      return null;
    },

    updateVideoInWatchlist: function(video, stopTime) {
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      if (watchlist === null || watchlist === undefined) {
        watchlist = [];
      }

      var matchedVideo = _.findWhere(watchlist, {
        id : video.thePlatformId
      });

      if (matchedVideo) {
        var matchedVideoIndex = _.indexOf(watchlist, matchedVideo);
        if (matchedVideoIndex > -1) {
          watchlist.splice(matchedVideoIndex, 1);
        }
      }

      var durationInMin = Math.round(video.totalVideoDuration / 60000);
      var saveBookmark = true;

      if (durationInMin < 20 && (video.totalVideoDuration/1000 - stopTime) < 15) {
        saveBookmark = false;
      } else if ((durationInMin <= 65 && durationInMin > 20) && video.totalVideoDuration/1000 - stopTime < 120) {
        saveBookmark = false;
      } else if (durationInMin > 65 && video.totalVideoDuration/1000 - stopTime < 360) {
        saveBookmark = false;
      }

      //this.checkAndSaveIfStateChanged();
      watchlist.push({
          id: video.thePlatformId
        , title: video.title
        , image: video.stillImageURL
        , seriesName: video.seriesName
        , isBehindWall: video.isBehindWall
        , stopTime: stopTime
        , finished: (saveBookmark) ? 'false' : 'true'
        , duration: video.totalVideoDuration / 1000
      });

      atv.localStorage.setItem(this.watchlistKey, watchlist);
      this.checkAndSaveIfStateChanged();
    },

    addVideoToWatchlist: function(thePlatformId, value) {
      this.checkAndSaveIfStateChanged();
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      
      if (watchlist === null || watchlist === undefined) {
        watchlist = [];
      }

      if (value !== null && value !== undefined) {
        watchlist.push(value);
      } else {
        console.log('Caught exception trying to toggle DOM element');
        return false;
      }

      atv.localStorage.setItem(this.watchlistKey, watchlist);
    },

    removeVideoFromWatchlist: function(thePlatformId){
      var watchlist = atv.localStorage.getItem(this.watchlistKey);
      
      var matchedVideo = _.findWhere(watchlist, {id : thePlatformId} );
      var matchedVideoIndex = _.indexOf(watchlist, matchedVideo);

      if (matchedVideoIndex > -1) {
        watchlist.splice(matchedVideoIndex, 1);
      }

      atv.localStorage.removeItem(this.watchlistKey);
      atv.localStorage.setItem(this.watchlistKey, watchlist);
      this.checkAndSaveIfStateChanged();
    },

    checkAndSaveIfStateChanged: function(actionType) {
      //after removing an item, if the lists are empty, state changed.
      var progressList = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
      var queueList = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);

      if ( this.checkIfWatchlistEmpty(progressList, true) && this.checkIfWatchlistEmpty(queueList, false)) {
        atv.sessionStorage.setItem( 'navbarStateChanged', 'true');
      } 
    },

    checkIfWatchlistEmpty: function(list, isContinueWatch) {
      if (list === null || list.length === 0)
        return true;
      
      if (isContinueWatch) {
        for (var i = 0; i < list.length; i++) {
          if (list[i].finished === 'false') {
            atv.localStorage.setItem('noProgressList', 'false');
            return false;
          }
        }
        atv.localStorage.setItem('noProgressList', 'true');
        return true;
      }
      
      return false;
    },

    validateAndUpdateWatchlist: function() {
      var self = this;
      var list = atv.localStorage.getItem(self.watchlistKey);
      var finishedList = [];
    
      if (list && list.length > 0) {
        var ids = '';
        counter = 0;
        for (var i = 0; i < list.length; i++) {
          if (this.listType === 'progress' || this.listType !== 'progress') {
            if (!list[i].finished || list[i].finished === 'false') {
              if (counter > 0)  ids += ',';
              ids += list[i].id;
              counter = counter + 1;
            } else {
              finishedList.push(list[i]);
            }
            
          }
        }

        console.log("validateAndUpdateWatchlist", ids);

        if (!ids)
          return false;

        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.mpxFeedPath + '?title_id=' + ids,
          headers: {},
          success: function(xhr) {
            var validVideoJson = JSON.parse(xhr.responseText);
            var validVideos = validVideoJson.Items;

            //if there are less validVideos than what's on the watchlist, remove
            if (validVideos.length < list.length) {
              var validVideoIds = [];
              for (var j = 0; j < validVideos.length; j++) {
                validVideoIds.push(validVideos[j].thePlatformId);
              }

              var updateList = [];
              for (var k = 0; k < list.length; k++) {
                if (validVideoIds.indexOf(list[k].id) > -1) {
                  updateList.push(list[k]);
                }
              }

              updateList = updateList.concat(finishedList);
              //console.log("updated ", self.watchlistKey, "=", updateList.length);
              atv.localStorage.setItem(self.watchlistKey, updateList);
            }
          },
          failure: function(status, xhr){
            console.log("Cannot get response for", aetn.globalContext.mpxFeedPath + ids, status, xhr);
          }
        });
        return true;
      } else {
        return false;
      }

    },

    //if a video is a show episode, save it's season and episode number, and if the video is complete information
    saveShowVideoPlayedInfo: function(video, endTime) {
      if (video.tvNtvMix === 'Show' && video.chapters && video.chapters.length > 0) {
        var finished = false;
        var duration = video.totalVideoDuration / 1000;
        if ((duration - endTime)/(duration) < 0.02) {
          finished = true;
        }
        var season = (video.season)? video.season : '0';
        var lastVideoData = {
          season: season
        , thePlatformId: video.thePlatformId
        , finished: finished
        };        

        //if (!atv.localStorage.getItem(video.seriesNameForAnalytics) || finished) {
          atv.sessionStorage.setItem('videoItemStateChanged', 'true');
        //} 

        atv.localStorage.setItem(video.seriesNameForAnalytics, lastVideoData);

      }
    }

  });

});

define('aetn/appletv/analytics/omniture',['klass','underscore'], function(klass){

  return klass({

    /*
    trackingOptions = {
      pageName : 'aaa',
      events : [a,b,c],
      pageDetails :  ''
      };
    */
    initialize: function(trackingOptions) {
      //omniture tracking will be off if in retail mode
      if (atv.localStorage.getItem('mvpd') === 'isInRetailDemoMode')
        return;

      var s = OmnitureMeasurement(aetn.globalContext.omniture.suiteIds);

      s.trackingServer = aetn.globalContext.omniture.server;
      s.trackingServerSecure = aetn.globalContext.omniture.secureServer;
      s.ssl = false;
      s.debugTracking = false;

      var context = {};

      context.platform = "AppleTV";
      context.brand = aetn.globalContext.omniture.brand;

      if (trackingOptions.watch) {
        _.extend(context, trackingOptions.watch);
      }

      if (trackingOptions.events) {
        s = this.setupEvents(trackingOptions.events, s);
      }

      switch (trackingOptions.pageName) {

      case 'SignIn':
        s = this.pageView(s, trackingOptions.pageName)
        context.authenticationstate = '0';
        context.cableprovidername = 'None';
        break;

      case 'Authentication':
        s = this.pageView(s, '');
        var mvpd = atv.localStorage.getItem('mvpd');
        if (mvpd) {
          context.authenticationstate = '1';
          context.cableprovidername = mvpd;
        } else {
          context.authenticationstate = '0';
          context.cableprovidername = 'None';
        }
        break;

      case 'Feature' :
        context.aetnanalyticstappedindex = trackingOptions.position;
        s = this.pageView(s, '');
        break;

      case 'VideoPlayer' :
          
        s = this.pageView(s, '');
        context.videocalltype = 'ContentView';
        context.videochapter = trackingOptions.chapter;

        if (trackingOptions.videoDetail) {
          context.videocategory = aetn.globalContext.omniture.brandPrefix + ":" + trackingOptions.videoDetail.omnitureTag;
          context.videocliptitle = context.videocategory + ':' + trackingOptions.videoDetail.title.replace(/ /g, '');
          context.videolf = (trackingOptions.videoDetail.longForm === 'true')? 'Longform' : 'Shortform';
          context.videotv = trackingOptions.videoDetail.tvNtvMix; //todo: make sure topic video is also 'TV'                        
          context.videoseason = (trackingOptions.videoDetail.season)? trackingOptions.videoDetail.season : 'None';
          context.videoepisode = (trackingOptions.videoDetail.episode)? trackingOptions.videoDetail.episode : 'None';
          context.videosubcategory = 'None';
          context.videofastfollow = (context.videolf === 'Longform')? ((trackingOptions.videoDetail.isFastFollow === 'true')?'FastFollow':'Archive') : 'Shortform';
          context.videoautoplay = 'Autoplay';
          context.videoduration = trackingOptions.videoDetail.duration;
          context.videofullscreen = 'Fullscreen';
          context.videoadvertiser = 'None'; //what value?
          context.videoadduration = 'None'; //what value?
          context.videoadtitle = 'None';  //what value?
          context.videorequiresauthentication = (trackingOptions.videoDetail.isBehindWall === 'true')? '1' : '0';
          context.PPLID = (trackingOptions.videoDetail.programID)? trackingOptions.videoDetail.programID: 'None';
        }
        break;

      default:
        s = this.pageView(s, trackingOptions.pageName, trackingOptions.pageDetail);
        break;
      }
      sendRequest(s, context);
    },

    pageView: function(s, pageName, pageDetail) {
      if (!pageDetail) {
        s.pagename = pageName;
      }else if (pageDetail) {
        s.pagename = pageName + ":" + pageDetail;
      }
      return s;
    },

    setupEvents: function(eventNames, s) {

      var eventNumberStr = '';
      for (var i = 0; i < eventNames.length; i++) {
        var eventNumber = this.getEventNumber(eventNames[i]);
        if (eventNumber) {
          eventNumberStr += (eventNumberStr.length > 0)? ',' : '';
          eventNumberStr += "event" + eventNumber;
        } else {
          console.log("Event Number doesn't exist for : " + eventNames[i]);
        }
      }
      s.events = eventNumberStr;
      return s;
    },

    getEventNumber: function(eventName) {

      var eventNumberMap = {
        "Video View": 21,
        "Video Start": 22,
        "Video Complete": 26,
        "Episode Start": 27,
        "Episode Complete": 28,
        "Internal Search": 7,
        "TV Everywhere Authentication Start": 72,
        "TV Everywhere Authentication Complete": 73,
        "TV Everywhere Authentication Error": 76
      };

      if (eventNumberMap[eventName]) {
        return eventNumberMap[eventName];
      } else {
        return null;
      }
    }

  });
});

define('aetn/appletv/analytics/krux',['klass','underscore'], function(klass){

  return klass({

    kruxURL: aetn.globalContext.krux.server,

    initialize: function( callOptions ) {
      if ( aetn.globalContext.krux.disable )
        return;

      switch ( callOptions.type ) {
        case 'screen' :
          this.doScreenCall( callOptions );
          break;
        case 'event' :
          this.doEventCall( callOptions );
          break;
        case 'heartbeat' :
          this.doEventCall( callOptions, 'heartbeat.gif?' );
          break;
      }

    },

    doScreenCall: function( callOptions ) {
      var url = this.kruxURL + 'pixel.gif?';
      var isLaunched = atv.sessionStorage.getItem( aetn.globalContext.omniture.brand + '_launched' );
      var header = document.rootElement.getElementByTagName('header');

      if ( !callOptions )
        return;
      if( callOptions.section.toLowerCase() === 'featured' && isLaunched )
        return;

      switch ( callOptions.section.toLowerCase() ) {
        case 'shows' :
          if ( header && header.getAttribute('id') ) {
            if ( header.getAttribute('id').split('_')[0] === 'H2') 
              callOptions.section = 'H2Shows';
          }
          callOptions.pageLevel2 = encodeURIComponent ( header.getElementByTagName('title').textContent );
          break;
        case 'topics' :
          if ( document.rootElement.getElementByTagName('itemDetail') )
            callOptions.pageLevel2 = encodeURIComponent ( document.rootElement.getElementByTagName('itemDetail').getElementByTagName('title').textContent );
          break;
      }
     

      var ctx = this.buildCommonParams();
      ctx._kcp_sc = callOptions.section;
      ctx._kpa_page_level_1 = callOptions.section;

      if ( callOptions.showName ) {
        ctx._kpa_show_name = callOptions.showName;
      }

      if ( callOptions.pageLevel2 ) {
        ctx._kpa_page_level_2 = callOptions.pageLevel2;
      }

      this.sendRequest ( url, ctx, function( success ) {
      if ( success )
        atv.sessionStorage.setItem( aetn.globalContext.omniture.brand + '_launched', true );
      } );

    },

    doEventCall: function( callOptions, _src ) {
      var src = _src ? _src : 'event.gif?';
      var url = this.kruxURL + src;
      var event = '';

      if ( !callOptions)
        return;

      if ( callOptions.events.indexOf ( 'Video Start' ) > -1 ) {
        event = 'start';
      }
      else if ( callOptions.events.indexOf ( 'Video Complete' ) > -1 ) {
        event = 'end';
      }

      var ctx = this.buildCommonParams();

      ctx.event_id = ( event === '') ? '' : ( event === 'start' ? 'JLn4isGR' : 'JLn4ukRD' );
      ctx._kpa_video_analytics_series_name = aetn.globalContext.omniture.brandPrefix + ":" + callOptions.videoDetail.omnitureTag;
      ctx._kpa_video_analytics_clip_title = ctx._kpa_video_analytics_series_name + ':' + callOptions.videoDetail.title.replace(/ /g, '');
      
      ctx._kpa_video_analytics_lf_vs_sf = (callOptions.videoDetail.longForm === 'true') ? 'Longform' : 'Shortform';
      ctx._kpa_video_season =  (callOptions.videoDetail.season) ? callOptions.videoDetail.season : 'None';
      ctx._kpa_video_series_type = callOptions.videoDetail.tvNtvMix;
      ctx._kpa_video_chapter = callOptions.chapter;
      ctx._kpa_video_episode = (callOptions.videoDetail.episode) ? callOptions.videoDetail.episode : 'None';
      ctx._kpa_video_PPLID = (callOptions.videoDetail.programID) ? callOptions.videoDetail.programID: 'None';

      if ( callOptions.videoDetail.totalVideoDuration )
        ctx._kpa_video_duration = Math.round ( callOptions.videoDetail.totalVideoDuration / ( 1000 * 60 ) );

      if ( callOptions.videoDetail.originalAirDate ) {
        var today = new Date();
        var airdate = new Date( callOptions.videoDetail.originalAirDate )
        ctx._kpa_video_analytics_days_since_premiere = Math.floor ( new Date( today - airdate ) / ( 1000 * 60 * 60 * 24 ) );
      }
     
      this.sendRequest ( url, ctx );
     
    },

    buildCommonParams: function() {
      var ctx = {};
      var mvpd = atv.localStorage.getItem('mvpd');

      ctx._kcp_d = aetn.globalContext.omniture.brand + '_AppleTV';
      ctx._kcp_s = ctx._kcp_d;
      ctx._kuid = aetnUtils.getVisitorID();
      ctx._kpid = "7156d277-5d35-4c9c-8fb7-f454c47dbfe1";

      if ( mvpd ) {
        ctx._kpa_is_tve_authenticated = 1;
        ctx._kpa_tve_provider_name = mvpd;
      }
      else {
        ctx._kpa_is_tve_authenticated = 0;
      }

      return ctx;
    },

    sendRequest: function( url, ctx, cb ) {
      var getUrl = url;

      for (var key in ctx) {
        getUrl += encodeURIComponent(key) + '=' + encodeURIComponent(ctx[key]) + '&';
      }

      var ajax = new ATVUtils.Ajax({
        url: getUrl,
        headers: {},
        success: function(xhr) {
          console.log('** Successfully sent Krux request **');
          if ( cb ) cb ( true );
        },
        failure: function(status, xhr) {
          console.log('***Error in Krux request: ', status, xhr);
        }
      });
    }

    

  });
});


define('aetn/appletv/analytics/videotracking',['klass' , 'aetn/appletv/analytics/omniture', 'aetn/appletv/analytics/krux'], function (klass, Omniture, Krux) {

  return klass({

    prepareChapterEvents: function() {

      var video = atv.sessionStorage.getItem('currentVideo');

      var chapters = video.chapters;
      var trackingEvents = {};

      if (chapters.length === 0) {

        trackingEvents['1'] = {trackings: ['Video View', 'Video Start'], chapter: 'None', videoData: video};
        trackingEvents[Math.floor(video.totalVideoDuration/1000 - 2)] = {trackings: ['Video Complete'], chapter: 'None', videoData: video};

      } else {

        for (var i = 0; i < chapters.length; i++) {

          var timeInSecond = chapters[i];
          var trackings = ['Video View', 'Video Start'];

          if ( i > 0 ) {
            //end of last chapter event
            trackingEvents[(timeInSecond - 3).toString()] = {trackings: ['Video Complete'], chapter: 'Chapter ' + i, videoData: video};
          } else {
            trackings.push('Episode Start');
          }

          trackingEvents[(timeInSecond + 2).toString()] = {trackings: trackings, chapter: 'Chapter ' + (i + 1), videoData: video};
        }

        //tracking data for episode complete
        var timepoint = Math.floor(video.totalVideoDuration/1000 * 0.98);
        trackingEvents[timepoint.toString()] = {
          trackings : ['Episode Complete', 'Video Complete'],
          chapter : 'Chapter ' + (i + 1),
          videoData : video
        };

      }

      atv.sessionStorage.setItem('currentTracking', trackingEvents);
    },

    timeUpdate: function (playerTime) {

      var chapterTracker = atv.sessionStorage.getItem('currentTracking');
  
      if (chapterTracker && chapterTracker[playerTime]){
        if (chapterTracker[playerTime].trackings){
          new Omniture({
            pageName: 'VideoPlayer',
            events : chapterTracker[playerTime].trackings,
            chapter : chapterTracker[playerTime].chapter,
            videoDetail : chapterTracker[playerTime].videoData
          });
          new Krux({
            type: 'event',
            events: chapterTracker[playerTime].trackings,
            chapter : chapterTracker[playerTime].chapter,
            videoDetail : chapterTracker[playerTime].videoData
          })
        }
      }

      /** KRUX HEARTBEAT **/
      aetnUtils.kruxHeartbeats++;

      if ( aetnUtils.kruxHeartbeats === 10 ) {
        if ( chapterTracker ) {
          var prevChapter = chapterTracker['1'];
          for ( chapter in chapterTracker ) {
            if ( parseInt( chapter ) > playerTime ) {
              new Krux({
                type: 'heartbeat',
                events: [],
                chapter : chapterTracker[prevChapter].chapter,
                videoDetail : chapterTracker[prevChapter].videoData
              });
              break;
            }
            prevChapter = chapter;
          }
        }
        aetnUtils.kruxHeartbeats = 0;
      }

    },

    seekUpdate: function (playerTime, lastPlayTime) {

      //find out playertime is between which time frame in the tracking event array

      var video = atv.sessionStorage.getItem('currentVideo');
      var chapters = video.chapters;

      if (chapters.length > 0) {

        var lastChapter = 0,
            currentChapter = 0;

        for (var i = 0; i < chapters.length; i++) {

          var currentChapterPoint = (i < chapters.length - 1)? parseInt(chapters[i + 1], 10) : Math.floor(video.totalVideoDuration);
          var lastChapterPoint = parseInt(chapters[i], 10);

          if (lastPlayTime > lastChapterPoint && lastPlayTime < currentChapterPoint) {
            lastChapter = i + 1;
          }

          if (playerTime > lastChapterPoint && playerTime < currentChapterPoint) {
            currentChapter = i + 1;
          }

        }

        if (currentChapter > lastChapter) {
          new Omniture({
            pageName : 'VideoPlayer',
            events : ['Video View'],
            chapter : 'Chapter ' + currentChapter,
            videoDetail : video
          });
         
        }
      }

    }
    
  });

});






define('aetn/appletv/video/videoplayer',['klass',
        'aetn/appletv/tve/adobepass',
        'aetn/appletv/ui/watchlist',
        'aetn/appletv/analytics/videotracking',
        'underscore'],

  function (klass,
            Adobepass,
            WatchList,
            VideoTracker) {

    return klass({
      initialize: function (videoId, swapCurrentScreen) {

        var self = this;
        this.proxy = new atv.ProxyDocument();
        this.proxy.onCancel = function() {
          if (ajax) {
            ajax.cancelRequest();
          }
        };

        if (swapCurrentScreen) {
          this.swapCurrentScreen = swapCurrentScreen;
        } else {
          this.swapCurrentScreen = swapCurrentScreen;
          this.proxy.show();
        }

        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.urlPath + '/video/json/' + videoId,
          header: {},
          success: function(xhr) {
            var video = JSON.parse(xhr.responseText);

              //&amp; in the player url will cause problem, unescape those
            video.mediaURL = video.mediaURL.replace(/&amp;/g, '&');
            atv.sessionStorage.setItem('currentVideo', video);
            self.loadVideoStream();
          },
          failure: function(status, xhr) {
            console.log('Fail to retrieve video data', status, xhr);
          }
        });
      },
      
      loadVideoStream: function () {
        
        var self = this;

        this.watchList = new WatchList('com.aetn.video-player', 'progress');
        this.video = atv.sessionStorage.getItem('currentVideo');
        
        
        var streamContext = aetnUtils.setupStreamContext();

        //console.log("streamContext=", JSON.stringify(streamContext.decisioningData));

        var vt = new VideoTracker();
        vt.prepareChapterEvents();

        if (atv.device.isInRetailDemoMode) {
          this.loadStreamfromMPX();
        } else {
          mDialog.ATVPlayer.loadStreamForKey(
            self.video.thePlatformId,
            streamContext,
            _.bind(this.loadStreamfromMDialog, self),
            _.bind(this.loadStreamfromMPX, self)
          );
        }

      },

      loadStreamfromMDialog: function (loadedStream) {

        var self = this;
        console.log('Loading from mDialog');

        var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
        this.video.interstitialsURL = loadedStream['interstitialsURL'];
        this.video.mediaURL = manifestURL;
        //load player with full url with signature and adobe token if is pay video (uncomment when the mdialog server is ready)       
        this.loadPlayerWithFullMediaURL(this.video, function(updatedVideo) {
          if (self.swapCurrentScreen) {
            atv.loadAndSwapXML(self.drawPlayer(self.video, updatedVideo.fullURL, updatedVideo.interstitialsURL));
          } else {
            self.proxy.loadXML(self.drawPlayer(self.video, updatedVideo.fullURL, updatedVideo.interstitialsURL));
          }
        });

      },

      loadStreamfromMPX: function(){

        var self = this;
        console.log('Loading from MPX');
        

        this.loadPlayerWithFullMediaURL(this.video, function(updatedVideo) {
          if (self.swapCurrentScreen) {
            atv.loadAndSwapXML(self.drawPlayer(self.video, updatedVideo.fullURL));
          } else {
            self.proxy.loadXML(self.drawPlayer(self.video, updatedVideo.fullURL));
          }
        });
      },

      loadPlayerWithFullMediaURL: function(video, cb) {

        var self = this;
        var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(video.mediaURL),
          headers: {},
          success: function(xhr) {
            video.fullURL = video.mediaURL + '&sig=' + xhr.responseText;
            if (video.isBehindWall === 'true') {
              aetnUtils.authorizeVideo(video, video.fullURL, cb);
            } else  {
              cb(video);
            }
          },
          failure: function(status, xhr) {
            console.log('getSignedSignature Failure Status: ', status, xhr);
          }
        });
      },

/*      authorizeVideo: function(video, fullURL, cb) {

        var self = this;
        var adobepass = new Adobepass();
        
        adobepass.getAuthorization(video,
          function(resource) {
            adobepass.getShortMediaToken(video,
              function(shortToken) {
                shortToken = encodeURIComponent(ATVUtils.Base64.decode(shortToken));
                fullURL += '&auth=' + shortToken;
                cb(fullURL);
              },
              function(message) {
                var xml = self.drawFailureMessage(message);
                atv.loadAndSwapXML(xml);
              }
            );
          },
          function(message) {
            var xml = self.drawFailureMessage(message);
            atv.loadAndSwapXML(xml);
          }
        );
      },

      drawFailureMessage: function(message) {

        var errorXML = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
                <body>\
                  <dialog id="com.aetn.error">\
                    <title>Error</title>\
                    <description><![CDATA[ ' + message + ' ]]></description>\
                  </dialog>\
                </body>\
              </atv>';
        return atv.parseXML(errorXML);
      },*/

      drawPlayer: function(video, mediaURL, interstitialsURL) {

        var savedVideo = this.watchList.getVideoFromWatchlist(video.thePlatformId);
        var thumbnail = video.modalImageURL ? video.modalImageURL : video.thumbnailImageURL;

        var player = '<?xml version="1.0" encoding="UTF-8"?>\
            <atv>\
              <body>\
                <videoPlayer id="com.aetn.video-player">\
                  <httpLiveStreamingVideoAsset id="' +  video.thePlatformId +'" indefiniteDuration="false">\
                    <mediaURL><![CDATA[' + mediaURL + ']]></mediaURL>\
                    <title><![CDATA[' + video.title + ']]></title>\
                    <description><![CDATA[' + video.description + ']]></description>\
                    <image>' + thumbnail + '</image>';

        if (interstitialsURL) {
          player += '<eventGroup>' + interstitialsURL + '</eventGroup>';
          if (savedVideo && savedVideo.stopTime > 10 && savedVideo.finished === 'false') {
            player += '<bookmarkNetTime>' + savedVideo.stopTime + '</bookmarkNetTime>';
          }
        } else {
          if (savedVideo && savedVideo.stopTime > 10 && savedVideo.finished === 'false') {
            player += '<bookmarkTime>' + savedVideo.stopTime + '</bookmarkTime>';
          }
        }

        /*if (video.longForm === 'true') {
            if (video.nextVideoId) {
              var upnextTime = video.totalVideoDuration/1000 - 30;
              player += '<upNextPresentationTime>' + upnextTime + '</upNextPresentationTime>';
              player += '<myMetadata>\
                          <relatedPlaybackID>' + video.nextVideoId + '</relatedPlaybackID>\
                        </myMetadata>';
            }
        } else {
            if (video.remainingVideos) {
              player += '<myMetadata><relatedPlaybackVideos>';
              for (var i = 0; i < video.remainingVideos.length; i++) {
                player += '<httpLiveStreamingVideoAsset id="PLAYLISTVIDE' + (i + 1) + '">';
                player += '<mediaURL><![CDATA[' + video.remainingVideos[i].playURL_HLS + ']]></mediaURL>';
                player += '<videoID>' + video.remainingVideos[i].id + '</videoID>';
                player += '<title>' + video.remainingVideos[i].title + '</title>';
                player += '<description>' + video.remainingVideos[i].description + '</description>';
                player += '<image>' + video.remainingVideos[i].image + '</image>';
                player += '</httpLiveStreamingVideoAsset>';
              }
              player += '</relatedPlaybackVideos></myMetadata>';
            }
        }*/
            
        player += '\
                  </httpLiveStreamingVideoAsset> \
                </videoPlayer>\
              </body>\
            </atv>';
        if (interstitialsURL) {
          atv.sessionStorage.setItem('hasInterstitials', true);
        } else {
          atv.sessionStorage.setItem('hasInterstitials', false);
        }

//console.log(player);

        return atv.parseXML(player);
      }
    });
  });


if( !aetnUtils ) { var aetnUtils = {}; }

aetnUtils.loadURLWithMVPD = function(link) {
  if (atv.localStorage.getItem('mvpd')) {
    link += '/' + atv.localStorage.getItem('mvpd');
  }
  atv.loadURL(link);
};

aetnUtils.loadAndSwapURLWithMVPD = function(link) {
  if (atv.localStorage.getItem('mvpd')) {
    link += '/' + atv.localStorage.getItem('mvpd');
  }
  atv.loadAndSwapURL(link);
};

aetnUtils.loadVideo = function(thePlatformId, isBehindWall, swapCurrentScreen) {
  console.log('loadVideo', thePlatformId, isBehindWall, swapCurrentScreen);
  if (atv.sessionStorage.getItem('SignInReferrer')){
    atv.sessionStorage.removeItem('SignInReferrer');
  }

  if(atv.sessionStorage.getItem('SignInReferrerUrl')) {
    atv.sessionStorage.removeItem('SignInReferrerUrl');
  }
  if (isBehindWall === 'true' && !atv.localStorage.getItem('mvpd')) {
    aetnUtils.initiateSignIn(thePlatformId);
  } else {
    require(['aetn/appletv/video/videoplayer'], function(VideoPlayer){
      new VideoPlayer(thePlatformId, swapCurrentScreen);
    });
  }
};

aetnUtils.loadFeaturedVideo = function(index, thePlatformId, isBehindWall) {
  require(['aetn/appletv/analytics/omniture'],
    function(Omniture){
      new Omniture({
        pageName: 'Feature'
      , position: index
      });
    });
  aetnUtils.loadVideo(thePlatformId, isBehindWall);
};

aetnUtils.initiateSignIn = function(thePlatformId, signInReferrerUrl) {
  atv.sessionStorage.setItem('SignInReferrer', thePlatformId);
  if (!signInReferrerUrl) {
    signInReferrerUrl = aetn.globalContext.urlPath + '/video/' + thePlatformId;
  }
  atv.sessionStorage.setItem('SignInReferrerUrl', signInReferrerUrl);
  atv.loadURL(aetn.globalContext.urlPath + '/tve/activate');
};

aetnUtils.loadFeaturedURL = function(index, url) {
  require(['aetn/appletv/analytics/omniture'],
    function(Omniture){
      new Omniture({
        pageName: 'Feature'
      , position: index
      });
    });
  aetnUtils.loadURLWithMVPD(url);
};

aetnUtils.initialAuthCheck = function() {

  if (atv.device.isInRetailDemoMode) {
    require(['aetn/appletv/analytics/omniture'], function(Omniture) {
      atv.localStorage.setItem('mvpd','isInRetailDemoMode');
      new Omniture({
        pageName: 'Authentication'
      });
    });
    return;
  }

  require(['aetn/appletv/tve/adobepass', 'aetn/appletv/analytics/omniture'],
    function(Adobepass, Omniture) {
      var adobepass = new Adobepass();
      adobepass.getAuthenticationToken(function(token){
        if (token) {
          atv.localStorage.setItem('mvpd',token.mvpd);
          aetnUtils.getMvpdHash(token.mvpd, function(data){
            if (data)
              atv.localStorage.setItem('mvpdHash', data);
          });

          new Omniture({pageName: 'Authentication'});
        }
      }, function(status, xhr) {
        if (atv.localStorage.getItem('mvpd')) {
          atv.localStorage.removeItem('mvpd');
          new Omniture({
            pageName: 'Authentication'
          });
        }
      });
    });
};

aetnUtils.initialWatchlistCheck = function() {

  require(['aetn/appletv/ui/watchlist'], function(Watchlist) {

    if (atv.device.isInRetailDemoMode) {
      atv.loadURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
      return;
    }

    var progressList = new Watchlist('', 'progress');
    var queueList = new Watchlist('', 'queue');

    var hasProgressList = progressList.validateAndUpdateWatchlist();
    var hasQueueList = queueList.validateAndUpdateWatchlist();

    if (hasProgressList || hasQueueList) {
      atv.loadURL(aetn.globalContext.urlPath + '/nav');
    } else {
      atv.loadURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
    }

  });
};

aetnUtils.loadAndSwapNav = function() {

  require(['aetn/appletv/ui/watchlist'], function(Watchlist) {
    var progresslist = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
    var queuelist = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);

    

    if ( (!progresslist || progresslist.length === 0) && (!queuelist || queuelist.length === 0 )) {
      atv.loadAndSwapURL(aetn.globalContext.urlPath + '/nav/nowatchlist');
    } else {
      atv.loadAndSwapURL(aetn.globalContext.urlPath + '/nav');
    }
    console.log('loadAndSwapNav', atv.sessionStorage.getItem('authStateChanged'));
  });
};

//Visitor ID As String
aetnUtils.getVisitorID = function() {
  var visitorID = atv.localStorage.getItem(aetn.globalContext.visitorIDKey);
  if (!visitorID) {
    //implement generating UUID, and save to localStorage.
    var s = [];
    var hexDigits = '0123456789abcdef';
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4';  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23];

    visitorID = s.join('');
    atv.localStorage.setItem(aetn.globalContext.visitorIDKey, visitorID);
  }
  return visitorID;
};

aetnUtils.handleVolatileReload = function (pageID, doc, reloadUrl, seriesName) {

   console.log('sectionVolatileReload', 
    'authStateChanged: ', 
    atv.sessionStorage.getItem('authStateChanged'),
    'videoItemStateChanged: ', 
    atv.sessionStorage.getItem('videoItemStateChanged'));


  switch(pageID) {


    case 'com.aetn.settings' :
        aetnUtils.settingsVolatileReload(doc);
      break;

    case 'com.aetn.topics' :
    case 'com.aetn.movies' :
    case 'com.aetn.shows' :
    case 'com.aetn.h2shows' :
      aetnUtils.sectionVolatileReload(doc);
      break;

    case 'com.aetn.watchlist' :
      aetnUtils.watchlistVolatileReload(reloadUrl);
      break;

    case 'com.aetn.season' :
      aetnUtils.seasonVolatileReload(reloadUrl, doc, seriesName);
      break;
    
    case 'com.aetn.videodetail' :
        //if (atv.sessionStorage.getItem('authStateChanged') === 'true') {          
          aetnUtils.loadAndSwapURLWithMVPD(reloadUrl);
        //}
      break;

    default :
        if (atv.sessionStorage.getItem('authStateChanged') === 'true') {          
          aetnUtils.loadAndSwapURLWithMVPD(reloadUrl);
        }
      break;

  }
  event.cancel();
};

aetnUtils.seasonVolatileReload = function(reloadUrl, doc, seriesName) {
  if (atv.sessionStorage.getItem('videoItemStateChanged') === 'true' || atv.sessionStorage.getItem('authStateChanged') === 'true') {
    var reloadUrlFull;
    var seriesnameKey = seriesName.replace(/[' ]/g, '');
    //console.log("==================seriesname", seriesnameKey);
    var lastVideoForShow = atv.localStorage.getItem(seriesnameKey);
    if (lastVideoForShow) {
      
      var info =  lastVideoForShow.season + '-' + 
                  lastVideoForShow.thePlatformId + '-' +
                  ((lastVideoForShow.finished) ? 'new' : 'current');
      reloadUrlFull = reloadUrl + encodeURIComponent(seriesName) + '/' + info;
      atv.sessionStorage.setItem('videoItemStateChanged', 'false');
      //console.log("before season reload");
      aetnUtils.loadAndSwapURLWithMVPD(reloadUrlFull);
    }
  } 
};

aetnUtils.watchlistVolatileReload = function(reloadUrl) {

  var progressList = atv.localStorage.getItem(aetn.globalContext.watchlist.progresskey);
  var queueList = atv.localStorage.getItem(aetn.globalContext.watchlist.queuekey);
  var reloadUrlFull = null;

  var hasProgressList = (progressList && progressList.length > 0 && atv.localStorage.getItem('noProgressList') === 'false')? true : false;
  var hasQueueList = (queueList && queueList.length > 0)? true : false;

  if (hasProgressList && hasQueueList) {
    console.log("has both list");
    reloadUrlFull = reloadUrl;
  } else if (hasProgressList) {
    console.log("has progress list");
    reloadUrlFull = reloadUrl + '/queueempty';
  } else if (hasQueueList) {
    console.log("has queue list");
    reloadUrlFull = reloadUrl + '/progressempty';
  } 

  if (reloadUrlFull) {
    if (atv.localStorage.getItem('mvpd')) {
      reloadUrlFull += '/' + atv.localStorage.getItem('mvpd');
    }

    var ajax = new ATVUtils.Ajax({url: reloadUrlFull,
      success: function(xhr){
         return atv.loadAndSwapXML(xhr.responseXML);
      },
      failure: function(){
        console.log("failure retrieve xml");
      }});
  }
}

aetnUtils.sectionVolatileReload = function(doc) {
  console.log('sectionVolatileReload', atv.sessionStorage.getItem('authStateChanged'));

  if (atv.sessionStorage.getItem('authStateChanged') !== 'true') {
      event.cancel();
      return;
  } 
  
  var img = null;
  if (atv.localStorage.getItem('mvpd')) {

    var mvpd = atv.localStorage.getItem('mvpd');
    
    img = doc.getElementById("brand_logo");

    var newLogo720 = img.getAttribute("src720").replace('logo@',  mvpd + '@');
    var newLogo1080 = img.getAttribute("src1080").replace('logo@',  mvpd + '@');

    img.setAttribute("src720", newLogo720);
    img.setAttribute("src1080", newLogo1080);

  } else {

    doc.getElementById("brand_logo").setAttribute("src720", aetn.globalContext.urlPath + 'logo@720.png');
    doc.getElementById("brand_logo").setAttribute("src1080", aetn.globalContext.urlPath + 'logo@1080.png');

  }

  atv.sessionStorage.setItem('authStateChanged', 'false');
};

aetnUtils.settingsVolatileReload = function(doc) {

  console.log('settingsVolatileReload', atv.sessionStorage.getItem('authStateChanged'));

  if (atv.sessionStorage.getItem('authStateChanged') !== 'true') {
      event.cancel();
      return;
  }

  var activation = null,
      label = null,
      text = null;

  if (atv.localStorage.getItem('mvpd')) {
    var mvpd = atv.localStorage.getItem('mvpd');

    activation = doc.getElementById("activation");
    if (activation) {
      label = activation.getElementByName("label");
      if (label && mvpd) {
        text = 'Sign Out';
        label.textContent = text;
      }
      activation.setAttribute("accessibilityLabel", "Sign out");
      activation.setAttribute("onSelect", "atv.loadURL(aetn.globalContext.urlPath + '/tve/deactivate');");
    }
  } else {

    activation = doc.getElementById("activation");
    if (activation) {
      label = activation.getElementByName("label");
      if (label) {
        text = 'Sign in to Provider';
        label.textContent = text;
      }
      activation.setAttribute("accessibilityLabel", "Sign in to Provider");
      activation.setAttribute("onSelect", "atv.loadURL(aetn.globalContext.urlPath + '/tve/activate');");
    }
  }

  atv.sessionStorage.setItem('authStateChanged', 'false');

};

aetnUtils.getMvpdHash = function(mvpdId, cb) {

  var ajax = new ATVUtils.Ajax({url: aetn.globalContext.mvpdHashConfigUrl,
      success: function(xhr){
        var hashCode = null;
        var configList = JSON.parse(xhr.responseText);
        for (var i = 0; i < configList.mvpdWhitelist.length; i++) {
            if (configList.mvpdWhitelist[i].mvpd === mvpdId) {
              foundProvider = true;
              hashCode = (configList.mvpdWhitelist[i].freewheelKeyValues)? configList.mvpdWhitelist[i].freewheelKeyValues.mvpdHash : null;
              break;
            }
        }
        cb(hashCode);
         
      },
      failure: function(){
        console.log("failure retrieve mvpd hash config file");
        cb(null);
      }});
};

aetnUtils.setupStreamContext = function() {
  var streamContext = {
    trackingData: {
      VISITOR_ID: aetnUtils.getVisitorID()
    }
  };

  var decisioningData = aetn.globalContext.decisioningData;
  var hash = atv.localStorage.getItem('mvpdHash');
  if (hash) {
    if (decisioningData == null)
      decisioningData = {};

      decisioningData._fw_ae = hash;
  }

  if (decisioningData) {
    streamContext.decisioningData = decisioningData;
  }

  return streamContext;

};

aetnUtils.authorizeVideo = function(video, mediaURL, cb) {
  require(['aetn/appletv/tve/adobepass'], function(Adobepass){
      var adobepass = new Adobepass();
      adobepass.getAuthorization(video,
          function(resource) {
            adobepass.getShortMediaToken(video,
              function(shortToken) {
                shortToken = encodeURIComponent(ATVUtils.Base64.decode(shortToken));
                //fullURL += '&auth=' + shortToken;
                video.fullURL = mediaURL + '&auth=' + shortToken;
                cb(video);
              },
              function(message) {
                var xml = aetnUtils.drawFailureMessage(message);
                atv.loadAndSwapXML(xml);
              }
            );
          },
          function(message) {
            var xml = aetnUtils.drawFailureMessage(message);
            atv.loadAndSwapXML(xml);
          }
      );
  });
};

       

aetnUtils.drawFailureMessage = function(message) {

        var errorXML = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
                <body>\
                  <dialog id="com.aetn.error">\
                    <title>Error</title>\
                    <description><![CDATA[ ' + message + ' ]]></description>\
                  </dialog>\
                </body>\
              </atv>';
        return atv.parseXML(errorXML);
};


aetnUtils.kruxHeartbeats = 0;


define("aetn/appletv/aetnUtils", function(){});



atv.config = {
  doesJavaScriptLoadRoot: true
};

atv.onAppEntry = function(){

  atv.sessionStorage.setItem('stackCtr', 0);
  atv.sessionStorage.setItem('authStateChanged', 'false');

  aetnUtils.initialAuthCheck();
  aetnUtils.initialWatchlistCheck();

  //initial mdialog player
  mDialog.ATVPlayer.init(aetn.globalContext.mdialogOptions);
};

atv.onAppExit = function() {
  atv.localStorage.removeItem('mvpd');
};


define("aetn/appletv/app", function(){});



var seeking = false;
var lastTime = -1;
var currentBreak = null;
var transportControlsVisible;
var playerState = null;
var breakPlayed = false;

if ( atv.player ) {

  atv.player.willStartPlaying = function() {
    if (atv.sessionStorage.getItem('hasInterstitials')) {
      var loadedStream = new mDialog.ATVPlayer.loadedStream();
      TextViewController.initiateView( 'counter' );
      TextViewController.hideView( 'counter', 0 );
        
      lastTime = -1;
      if (loadedStream) {
        loadedStream.startFeature();
      }
    }
   
  };

  atv.player.playerStateChanged = function(newState, timeIntervalSec) {
    require(['aetn/appletv/analytics/videotracking'],
      function(VideoTracker){
        playerState = newState;
      
        switch (newState) {
        
        case atv.player.states.FastForwarding:
          seeking = true;
          break;
        case atv.player.states.Rewinding:
          seeking = true;
          break;
        case atv.player.states.Loading:

          /*
          Check nearestPreviousAdBreak on atv.player.states.Loading, 
          if the break has not been consumed, snapback to the 
          startTime of the Adbreak
          */

          if (seeking === true) {
            breakPlayed = false;

            if (!atv.sessionStorage.getItem('hasInterstitials')) {
              var vt = new VideoTracker();
              vt.seekUpdate(mDialog.ATVPlayer.streamTimeWithoutAds(timeIntervalSec), atv.sessionStorage.getItem('lastTime'));
            }else {

              var nearestPreviousAdBreak = mDialog.ATVPlayer.nearestPreviousAdBreak(timeIntervalSec);
              //save fast forward to point
              if (!nearestPreviousAdBreak['consumed']) {
                atv.sessionStorage.setItem('FastForwardTo', timeIntervalSec);
                atv.player.playerSeekToTime( nearestPreviousAdBreak['startTime'] );
                atv.sessionStorage.setItem('BeforeSeekPosition', atv.sessionStorage.getItem('lastTime'));
              }
            }
          }
          break;
        
        case atv.player.states.Playing:
          seeking = false;
          break;
        case atv.player.states.Stopped:
          aetnUtils.kruxHeartbeats = 0;
          break;
        }
      });
  };


  atv.player.didStopPlaying = function(){
    atv.localStorage.setItem('noProgressList', 'false');
    require(['aetn/appletv/ui/watchlist'],
      function(WatchList){
        var watchList = new WatchList('com.aetn.videoplayer', 'progress');
        watchList.updateVideoInWatchlist(atv.sessionStorage.getItem('currentVideo'),  atv.sessionStorage.getItem('lastTime'));
        watchList.saveShowVideoPlayedInfo(atv.sessionStorage.getItem('currentVideo'), atv.sessionStorage.getItem('lastTime'));
      });
    if (atv.sessionStorage.getItem('hasInterstitials')) {
      mDialog.ATVPlayer.didStopPlaying();
    }
    if ( TextViewController.getView( 'counter' ) ) {
      TextViewController.removeView( 'counter' );
    }

    //Send back to the page where video play was originated
    var gotoUrl = atv.sessionStorage.getItem('SignInReferrerUrl');
    if (gotoUrl) {
      atv.sessionStorage.removeItem('SignInReferrerUrl');
      atv.loadURL(gotoUrl);
    }
  };

  atv.player.playerTimeDidChange = function(timeIntervalSec) {

    require(['aetn/appletv/analytics/videotracking'],
      function(VideoTracker){
        var vt = new VideoTracker();
        var currentTime = Math.floor(timeIntervalSec);

        if (!seeking && currentTime !== lastTime && playerState === 'Playing') {
          lastTime = currentTime;
          currentBreak = null;
          var currentTimeWithNoAd = atv.player.convertGrossToNetTime(currentTime);
          atv.sessionStorage.setItem('lastTime', currentTimeWithNoAd);

          if (atv.sessionStorage.getItem('hasInterstitials')) {
            mDialog.ATVPlayer.timeUpdate (
              currentTime, function(breakInfo, timeRemaining) {
                currentBreak = breakInfo;
                if (timeRemaining === 1) {
                  breakPlayed = true;
                }
                TextViewController.updateMessage( 'ADVERTISEMENT '+ timeRemaining +'s REMAINING' );
                if (currentBreak.startTime === currentTime && TextViewController.visibility() === 'hidden' ) {
                  TextViewController.showView( 'counter', 0 );
                  if (transportControlsVisible === false) {
                    atv.setTimeout( function() {
                      TextViewController.hideView( 'counter', 0 );
                    }, 4000);
                  }
                }
              });

            if ( !currentBreak ) {
              if ( TextViewController.visibility() === 'visible' ) {
                TextViewController.hideView( 'counter', 0 );
              }

              var fastForwardTo = atv.sessionStorage.getItem('FastForwardTo');
              if (fastForwardTo !== null  && breakPlayed === true) {
                atv.player.playerSeekToTime(fastForwardTo);
                vt.seekUpdate(fastForwardTo, atv.sessionStorage.getItem('BeforeSeekPosition'));

                atv.sessionStorage.removeItem('FastForwardTo');
                atv.sessionStorage.removeItem('BeforeSeekPosition');
              }else {
                vt.timeUpdate(currentTimeWithNoAd);
              }
            }
          } else {
             vt.timeUpdate(currentTimeWithNoAd);
             //quit video at 2 min if it is in retail mode
             if ( atv.localStorage.getItem('mvpd') === 'isInRetailDemoMode') {
                if (currentTime >= 120) {
                  atv.player.stop();
                }
             }
          }
        }
      });
  };

  atv.player.playerShouldHandleEvent = function(event, timeIntervalSec) {
    var allowEvent = true;
    switch (event) {

    case atv.player.events.FFwd:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.Rew:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.SkipFwd:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;
    case atv.player.events.SkipBack:
      if ( currentBreak && !currentBreak['consumed'] ) {
        allowEvent = false;
      }
      break;

    }
    return allowEvent;
  };

  var currentPlaylistPosition = 0;

  /**
 * This function is called whenever more assets are required by the player. The implementation
 * should call callback.success or callback.failure at least once as a result. This function
 * will not be called again until the invocation from the last call invokes callback.success
 * or callback.failure. Also, this function will not be called again for a playback if callback.success
 * is called with null argument, or callback.failure is called.
 * Calling any of the callback functions more than once during the function execution has no effect.
 */
  atv.player.loadMoreAssets = function(callback) {

    console.log(" == Load more assets has been called == " );
    //set current playlist position, and clear playlist streams from storage
    currentPlaylistPosition = 0;
    atv.sessionStorage.removeItem("playlistStreams");

    //save eventDatas before retriving other mdialog streams for the new assets since it will override the eventsData
    var currentVideoStream = atv.sessionStorage.getItem("mDialog-loadedStream");
    
    var metadataItem = retrieveMetadataItem( "relatedPlaybackVideos" );

    if (metadataItem instanceof atv.Element ) {
        playlist = metadataItem.getElementsByTagName('httpLiveStreamingVideoAsset');
        console.log("playlist size=", playlist.length);

        var counter = 0;
        var fullURLs = [];
        var interstitialsURLs = [];
        var mdialogStreams = [];

        function getFullMediaUrlForShortformVideo(mediaURL, index) {
          var ajax = new ATVUtils.Ajax({
            url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(mediaURL),
            headers: {},
            success: function(xhr) {
              fullURLs[index] = mediaURL + '&sig=' + xhr.responseText;
              console.log("full valid url = ", fullURLs[index]);
              counter++;                                          
            },
            failure: function(status, xhr) {
              counter++;
              console.log('getSignedSignature Failure Status: ', status, xhr);
              fullURLs[index] = null;
            }
          });
        }

        playlist.forEach(function(item, index){
          var video = {};
          video.thePlatformId = item.getElementByTagName("videoID").textContent;
          video.playURL_HLS = item.getElementByTagName("mediaURL").textContent;
          video.isBehindWall = 'false';

          //get mdialog video stream url
          mDialog.ATVPlayer.loadStreamForKey(
            video.thePlatformId,
            aetnUtils.setupStreamContext(),
                function(loadedStream) {
                  //console.log("loadedStream = ", JSON.stringify(loadedStream));
                  var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
                  //console.log("====successful geting mdialog stream ", manifestURL);
                  interstitialsURLs[index] = loadedStream['interstitialsURL'];
                  mdialogStreams[index] = loadedStream;
                  getFullMediaUrlForShortformVideo(manifestURL, index);

                },
                function() {
                  interstitialsURLs[index] = null;
                  getFullMediaUrlForShortformVideo(video.playURL_HLS, index);
                }
              );
        });

        var t = atv.setInterval(function() {
          
          if (counter == playlist.length) {
              atv.clearInterval(t);
              var docStr = '<?xml version="1.0" encoding="UTF-8"?><playlistAssets>';
              var mdialogStreamArray = [];
          
              //update elements with the new fullURLs
              for (var i = 0; i < playlist.length; i++) {
                if (fullURLs[i]) {
                  docStr += '<httpLiveStreamingVideoAsset>';
                  docStr += '<mediaURL><![CDATA[' + fullURLs[i] + ']]></mediaURL>';
                  docStr += '<title>' + playlist[i].getElementByTagName("title").textContent + '</title>';
                  docStr += '<description>' + playlist[i].getElementByTagName("description").textContent + '</description>';
                  docStr += '<image>' + playlist[i].getElementByTagName("image").textContent + '</image>';
                  if (interstitialsURLs[i]) 
                    docStr += '<eventGroup>' + interstitialsURLs[i] + '</eventGroup>';
                  docStr += '</httpLiveStreamingVideoAsset>';
                  if (mdialogStreams[i])
                    mdialogStreamArray.push(mdialogStreams[i]);
                }
              }
              docStr += '</playlistAssets>'

              var playlistDoc = atv.parseXML(docStr);
              newAssets = playlistDoc.rootElement.getElementsByTagName('httpLiveStreamingVideoAsset');
              console.log("add ", newAssets.length, " more assets to play list ====", newAssets[0].getElementByTagName("mediaURL") );
              //update storage with current video stream
              atv.sessionStorage.setItem("mDialog-loadedStream", currentVideoStream);

              //save the mdialog stream data
              //atv.sessionStorage.setItem("playlistStreams", mdialogStreamArray);
              callback.success(newAssets);

          }
        }, 100);

    } else {
      callback.success(null);
    }

  };

  


  /**
   * In order to access the relatedPlayback document we need it to be
   * somewhere globally accessible. For demonstration I am setting it
   * as a global variable.
   **/
  var currentRelatedPlaybackDocument;

  /**
   * Up Next / Postplay
   * atv.player.loadRelatedPlayback
   *
   * This function is called just before the Up Next (postplay) screen
   * will appear. It provides you the opportunity to set or change the
   * UpNext asset, and/or add additional options for the user to select.
   * These will appear on the postplay screen below the currently playing
   * asset.
   */
  atv.player.loadRelatedPlayback = function(upNextAsset, callback) {
    console.log(" == Load post playback options called with upNextAsset: "+ upNextAsset +" : "+ (upNextAsset instanceof atv.Element) +" == ");

    var relatedPlaybackID = retrieveMetadataItem('relatedPlaybackID');

    console.log( " == Do we have a relatedPlaybackID: "+ relatedPlaybackID +" == " );

    if (relatedPlaybackID instanceof atv.Element) {
      console.log( " == We will be loading the related content from this url: "+ relatedPlaybackID.textContent +" == " );

      var videoId = relatedPlaybackID.textContent;
      console.log("video id = ", videoId);

      var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.urlPath + '/video/json/' + videoId,
          header: {},
          success: function(xhr) {
            //console.log(xhr.responseText);
            var video = JSON.parse(xhr.responseText);

              //&amp; in the player url will cause problem, unescape those
            video.mediaURL = video.mediaURL.replace(/&amp;/g, '&');
            atv.sessionStorage.setItem('currentVideo', video);

            require(['aetn/appletv/analytics/videotracking'], function(VideoTracker){
              var vt = new VideoTracker();
              vt.prepareChapterEvents();
            });

            mDialog.ATVPlayer.loadStreamForKey(
              video.thePlatformId,
              aetnUtils.setupStreamContext(),
              function(loadedStream) {
                var manifestURL = (loadedStream.prerollManifestURL && !(loadedStream.prerollPlayed)) ? loadedStream.prerollManifestURL : loadedStream.manifestURL;
                video.interstitialsURL = loadedStream['interstitialsURL'];
                loadFullMediaUrl(video, manifestURL, callback);

                //callback.success(loadFullMediaUrl(video, manifestURL, makeUpDocumentForRelatedPlayback));
              },
              function() {
                loadFullMediaUrl(video, video.playURL_HLS, callback);
                //callback.success(loadFullMediaUrl(video, video.playURL_HLS, makeUpDocumentForRelatedPlayback));
              }
            );

            
          },
          failure: function(status, xhr) {
            console.log('Fail to retrieve video data', status, xhr);
          }
        });


    } else if( upNextAsset instanceof atv.Element ) {
      /**
       * For the UpNext example that uses the loadMoreAssets playlist
       * we do not want to change the current asset, so we pass an empty
       * <relatedPlayback> document to callback.success.
       */
      console.log( " == We have an upNextAsset: "+ upNextAsset.ownerDocument.serializeToString() +" == " );
      callback.success( atv.parseXML( '<atv><body><relatedPlayback></relatedPlayback></body></atv>'));
    } else {
      /**
       * When there are no more asset to be display we pass
       * null to callback.success.
       */
      console.log( " == No relatedPlaybackID, so we're sending null == " );
      callback.success(null);
    }
  };

  function loadFullMediaUrl(video, mediaURL, callback) {
    console.log("===get the full url for media url, then call callback", callback);
    var ajax = new ATVUtils.Ajax({
          url: aetn.globalContext.tpTokenAuthUrl + '?url=' + encodeURI(mediaURL),
          headers: {},
          success: function(xhr) {
            console.log("signature is back: ", xhr.responseText);
            video.fullURL = mediaURL + '&sig=' + xhr.responseText;
            if (video.isBehindWall === 'true') {
              aetnUtils.authorizeVideo(video, video.fullURL, function(updatedVideo){
                callback.success(makeUpDocumentForRelatedPlayback(updatedVideo));
              });
            } else  {
              callback.success(makeUpDocumentForRelatedPlayback(video));
            }
          },
          failure: function(status, xhr) {
            console.log('getSignedSignature Failure Status: ', status, xhr);
            callback.success(null);
          }
        });
  }

  function makeUpDocumentForRelatedPlayback(video) {
    
    //console.log("in makeup Document For related playback")
    var document = '<?xml version="1.0" encoding="UTF-8"?>\
              <atv>\
              <body>\
              <relatedPlayback>\
                <upNextItem id="com.aetn.videorelatedplayback">\
                  <httpLiveStreamingVideoAsset id="' + video.thePlatformId + '" indefiniteDuration="false">\
                    <mediaURL><![CDATA[' + video.fullURL + ']]></mediaURL>\
                    <title>' + video.title + '</title>\
                    <description>' + video.description + '</description>\
                    <image>' + video.thumbnailImageURL + '</image>';
    if (video.interstitialsURL) {
      document += '<eventGroup>' + video.interstitialsURL + '</eventGroup>';
    }
    if (video.nextVideoId) {
      document += '<upNextPresentationTime>' + (video.totalVideoDuration/1000 - 30) + '</upNextPresentationTime>\
                    <myMetadata>\
                      <relatedPlaybackID>' + video.nextVideoId + '</relatedPlaybackID>\
                    </myMetadata>';
    }
      document += '</httpLiveStreamingVideoAsset></upNextItem>' +                          
             ' </relatedPlayback>\
            </body>\
            </atv>';
    //console.log(document);
    return atv.parseXML(document);

  }

  function onRelatedPlaybackOptionSelected(id) {
  console.log(" == ITEM WITH ID "+ id +" has been selected.");
}

function loadAssetURL(url) {
  console.log( " == Loading alternate video: "+ url +" == " );
  var ajax = new ATVUtils.Ajax({
    "url": url,
    "success": function(xhr) {
      var xml = xhr.responseXML,
        videoType = atv.sessionStorage.getItem('videoType'),
        videoElement = xml.rootElement.getElementByTagName(videoType);

      console.log( " == We have successfully loaded the XML file: "+ xhr.responseText +" == " );
      console.log( " == Here is our video element: "+ videoElement +" == " );
      atv.player.changeToAsset(videoElement);
    }
  })
}

function retrieveMetadataItem( itemName ) {
  var metadata = atv.player.asset.getElementByTagName('myMetadata'),
    item;
  if( metadata instanceof atv.Element ) {
    item = metadata.getElementByTagName( itemName );
  }
  return item;
}

function customOnSelect(msg, videoType) {
  console.log(" == HANDLING THE UPNEXT ONSELECT FOR TRACKING: "+ msg +" == ");
  var videoElement = currentRelatedPlaybackDocument.rootElement.getElementByTagName(videoType);

  if( videoElement )
  {
    atv.player.changeToAsset(videoElement);
  }
}

// atv.player.currentAssetChanged
// Called when the current asset changes to the next item in a playlist.
atv.player.currentAssetChanged = function() {
  console.log(" == ASSET LENGTH: currentAssetChanged to position: ", currentPlaylistPosition, "== ");
  /*var streams = atv.sessionStorage.getItem("playlistStreams");
  if (streams) {
    console.log("++++++++++++++ Found streams data and ", streams[currentPlaylistPosition]);
    //save stream to storage to prepare for next asset play
    atv.sessionStorage.setItem("mDialog-loadedStream", JSON.stringify(streams[currentPlaylistPosition]));
    console.log("+++++++++++++++++finish saving data=", JSON.stringify(streams[currentPlaylistPosition]));
  }
  currentPlaylistPosition++;*/
}



   /*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be displayed
  */
  
  atv.player.onTransportControlsDisplayed = function( animationDuration ) {
    transportControlsVisible = true;
    if ( TextViewController.getView( 'counter' ) && currentBreak && playerState === 'Playing') {
      TextViewController.showView( 'counter', animationDuration);
    }
  };

  /*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be hidden
  */
  
  atv.player.onTransportControlsHidden = function( animationDuration ) {
    transportControlsVisible = false;
    if ( TextViewController.getView( 'counter' ) && currentBreak && playerState === 'Playing' ) {
      TextViewController.hideView( 'counter', animationDuration );
    }
  };

}

var TextViewController = ( function() {

  var __config = {},
  __views = {};
  
  function SetConfig(property, value) {
    if(property) {
      __config[ property ] = value;
    }
  }
  
  function GetConfig(property) {
    if(property) {
      return __config[ property ];
    } else {
      return false;
    }
  }
  
  function SaveView( name, value ) {
    if( name ) {
      __views[ name ] = value;
    }
  }
  
  function GetView( name ) {
    if(name) {
      return __views[ name ];
    } else {
      return false;
    }
  }
  
  function RemoveView( name ) {
    if( GetView( name ) ) {
      delete __views[ name ];
    }
  }
  
  function HideView( name, timeIntervalSec ) {
    
    var animation = {
      'type': 'BasicAnimation',
      'keyPath': 'opacity',
      'fromValue': 1,
      'toValue': 0,
      'duration': timeIntervalSec,
      'removedOnCompletion': false,
      'fillMode': 'forwards',
      'animationDidStop': function(finished) {
        console.log('Animation Finished:', finished);
      }
    },
    viewContainer = GetView( name );
    
    SetConfig('visibility', 'hidden' );
                      
    if( viewContainer ) {
      viewContainer.addAnimation( animation, name );
    }
  }
  
  function ShowView( name, timeIntervalSec ) {
    
    var animation = {
      'type': 'BasicAnimation',
      'keyPath': 'opacity',
      'fromValue': 0,
      'toValue': 1,
      'duration': timeIntervalSec,
      'removedOnCompletion': false,
      'fillMode': 'forwards',
      'animationDidStop': function(finished) {
        console.log('Animation Finished:', finished);
      }
    },
    viewContainer = GetView( name );
    
    SetConfig('visibility', 'visible' );
                      
    if( viewContainer ) {
      viewContainer.addAnimation( animation, name );
    }
  }
  
  function UpdateMessage( message ) {
    
    var messageView = GetConfig( 'messageView' ),
        seconds = GetConfig( 'numberOfSeconds' );
    
    if(messageView && message) {

      messageView.attributedString = {
        'string': message,
        'attributes': {
          'pointSize': 22.0,
          'color': {
            'red': 1,
            'blue': 1,
            'green': 1
          }
        }
      };

    }

  }
  
  function InitiateView( name ) {

    var viewContainer = new atv.View(),
        message = new atv.TextView(),
        screenFrame = atv.device.screenFrame,
        width = screenFrame.width,
        height = screenFrame.height * 0.07;
    
    // Setup the View container.
    viewContainer.frame = {
      'x': screenFrame.x,
      'y': screenFrame.y + screenFrame.height - height,
      'width': width,
      'height': height
    };
    
    viewContainer.backgroundColor = {
      'red': 0.188,
      'blue': 0.188,
      'green': 0.188,
      'alpha': 0.7
    };
    
    viewContainer.alpha = 1;
    
    var topPadding = viewContainer.frame.height * 0.35,
        horizontalPadding = viewContainer.frame.width * 0.05;
    
    // Setup the message frame
    message.frame = {
      'x': horizontalPadding,
      'y': 0,
      'width': viewContainer.frame.width - (2 * horizontalPadding),
      'height': viewContainer.frame.height - topPadding
    };
    
    // Save the initial number of seconds as 0
    SetConfig( 'numberOfSeconds', 0 );
    
    // Update the overlay message
    //var messageTimer = atv.setInterval( __updateMessage, 1000 );
    //SetConfig( 'messageTimer', messageTimer )
    
    // Save the message to config
    SetConfig('messageView', message );
    SetConfig('animation', 'complete' );
                      
    UpdateMessage();
    
    // Add the sub view
    viewContainer.subviews = [ message ];
    
    // Paint the view on Screen.
    atv.player.overlay = viewContainer;
    
    SaveView( name, viewContainer );
  }
  
  function Visibility() {
    return GetConfig( 'visibility' );
  }
  
  return {
    'initiateView': InitiateView,
    'hideView': HideView,
    'showView': ShowView,
    'saveView': SaveView,
    'getView': GetView,
    'removeView': RemoveView,
    'setConfig': SetConfig,
    'getConfig': GetConfig,
    'updateMessage': UpdateMessage,
    'visibility': Visibility
  };
})();
define("aetn/appletv/app.player", function(){});

function OmnitureMeasurement(rsid) {
  
  var s = {
    account: rsid //required
  , linkURL: ''
  , linkName: ''
  , linkType: ''
  , dc: ''
  , trackingServer: ''
  , trackingServerSecure: ''
  , userAgent: ''
  , dynamicVariablePrefix: ''
  , visitorID: aetnUtils.getVisitorID() //required. no longer defaults to Device Hardware ID
  , vmk: ''
  , visitorMigrationKey: ''
  , visitorMigrationServer: ''
  , visitorMigrationServerSecure: ''
  , charSet:'UTF-8'
  , visitorNamespace: ''
  , pageName: '' //required
  , pageURL: '' //required
  , referrer: ''
  , currencyCode: ''
  , purchaseID: ''
  , variableProvider: ''
  , channel: ''
  , server: ''
  , pageType: ''
  , transactionID: ''
  , campaign: ''
  , state: ''
  , zip: ''
  , events: ''
  , products: ''
  , hier1: ''
  , hier2: ''
  , hier3: ''
  , hier4: ''
  , hier5: ''
  , prop1: ''
  , prop2: ''
  , prop3: ''
  , prop4: ''
  , prop5: ''
  , prop6: ''
  , prop7: ''
  , prop8: ''
  , prop9: ''
  , prop10: ''
  , prop11: ''
  , prop12: ''
  , prop13: ''
  , prop14: ''
  , prop15: ''
  , prop16: ''
  , prop17: ''
  , prop18: ''
  , prop19: ''
  , prop20: ''
  , prop21: ''
  , prop22: ''
  , prop23: ''
  , prop24: ''
  , prop25: ''
  , prop26: ''
  , prop27: ''
  , prop28: ''
  , prop29: ''
  , prop30: ''
  , prop31: ''
  , prop32: ''
  , prop33: ''
  , prop34: ''
  , prop35: ''
  , prop36: ''
  , prop37: ''
  , prop38: ''
  , prop39: ''
  , prop40: ''
  , prop41: ''
  , prop42: ''
  , prop43: ''
  , prop44: ''
  , prop45: ''
  , prop46: ''
  , prop47: ''
  , prop48: ''
  , prop49: ''
  , prop50: ''
  , prop51: ''
  , prop52: ''
  , prop53: ''
  , prop54: ''
  , prop55: ''
  , prop56: ''
  , prop57: ''
  , prop58: ''
  , prop59: ''
  , prop60: ''
  , prop61: ''
  , prop62: ''
  , prop63: ''
  , prop64: ''
  , prop65: ''
  , prop66: ''
  , prop67: ''
  , prop68: ''
  , prop69: ''
  , prop70: ''
  , prop71: ''
  , prop72: ''
  , prop73: ''
  , prop74: ''
  , prop75: ''
  , eVar1: ''
  , eVar2: ''
  , eVar3: ''
  , eVar4: ''
  , eVar5: ''
  , eVar6: ''
  , eVar7: ''
  , eVar8: ''
  , eVar9: ''
  , eVar10: ''
  , eVar11: ''
  , eVar12: ''
  , eVar13: ''
  , eVar14: ''
  , eVar15: ''
  , eVar16: ''
  , eVar17: ''
  , eVar18: ''
  , eVar19: ''
  , eVar20: ''
  , eVar21: ''
  , eVar22: ''
  , eVar23: ''
  , eVar24: ''
  , eVar25: ''
  , eVar26: ''
  , eVar27: ''
  , eVar28: ''
  , eVar29: ''
  , eVar30: ''
  , eVar31: ''
  , eVar32: ''
  , eVar33: ''
  , eVar34: ''
  , eVar35: ''
  , eVar36: ''
  , eVar37: ''
  , eVar38: ''
  , eVar39: ''
  , eVar40: ''
  , eVar41: ''
  , eVar42: ''
  , eVar43: ''
  , eVar44: ''
  , eVar45: ''
  , eVar46: ''
  , eVar47: ''
  , eVar48: ''
  , eVar49: ''
  , eVar50: ''
  , eVar51: ''
  , eVar52: ''
  , eVar53: ''
  , eVar54: ''
  , eVar55: ''
  , eVar56: ''
  , eVar57: ''
  , eVar58: ''
  , eVar59: ''
  , eVar60: ''
  , eVar61: ''
  , eVar62: ''
  , eVar63: ''
  , eVar64: ''
  , eVar65: ''
  , eVar66: ''
  , eVar67: ''
  , eVar68: ''
  , eVar69: ''
  , eVar70: ''
  , eVar71: ''
  , eVar72: ''
  , eVar73: ''
  , eVar74: ''
  , eVar75: ''
  , list1: ''
  , list2: ''
  , list3: ''
  , timestamp: ''
  , profileID: ''
  , ssl: false
  , debugTracking: false
  , autoTimestamp: false
  };
  
  return s;
}

//Version As String
function getResolution() {
  return atv.device.preferredVideoFormat;
}

//Version As String
function getFirmwareVersion() {
  return atv.device.softwareVersion;
}

function sendRequest(s, contextData) {

  var url = 'http';
  var prefix = '';
  //check for ssl && set inital prefix value if available
  if (s.ssl) {
    url = url + 's';
    prefix = s.trackingServerSecure;
  } else {
    prefix = s.trackingServer;
  }
  
  url = url + '://';
  
  //prepare the prefix if tracking server is not specified
  if (prefix === '') {
    //fix up the dc variable, default to SJO
    s.dc = s.dc.toLowerCase();
    if (s.dc === 'dc2' || s.dc === '122') {
      s.dc = '122';
    } else {
      s.dc = '112';
    }
    
    if (s.visitorNamespace !== '') {
      prefix = s.visitorNamespace + '.' + s.dc + '.207.net';
    } else {
      prefix = s.account + '.' + s.dc + '.207.net';
    }
  }
  
  url = url + prefix + '/b/ss/' + s.account + '/0/BRS-0.7/?AQB=1&ndh=1&';
  
  //build the query string
  var qs = '';
  var value = '';
  for (var o in s) {
    
    if (s.debugTracking) {
      value = '';
      if (typeof(s[o]) === 'string' && s[o] !== '') {
        value = s[o];
      }
    }
    
    if (o === 'visitorid' && s[o] !== '') {
      qs = qs + 'vid=' + encodeURI(s[o]) + '&';
    } else if (o === 'pageurl' && s[o].length > 0) {
      qs = qs + 'g=' + encodeURI(s[o].substring(0, 255)) + '&';
    } else if (o === 'referrer' && s[o] !== '') {
      qs = qs + 'r=' + encodeURI(s[o].substring(0, 255)) + '&';
    } else if (o === 'events' && s[o] !== '') {
      qs = qs + 'ev=' + encodeURI(s[o]) + '&';
    } else if (o === 'visitormigrationkey' && s[o] !== '') {
      qs = qs + 'vmt=' + s[o] + '&';
    } else if (o === 'vmk' && s[o] !== '') {
      qs = qs + 'vmt=' + s[o] + '&';
    } else if (o === 'visitormigrationserver' && s[o] !== '' && s.ssl === false) {
      qs = qs + 'vmf=' + s[o] + '&';
    } else if (o === 'visitormigrationserversecure' && s[o] !== '' && s.ssl === true) {
      qs = qs + 'vmf=' + s[o] + '&';
    } else if (o === 'timestamp') {
      if (s.autoTimestamp) {
        var dt = new Date();
        dt.mark();
        //autoTimestamp needs encodeURI?
        qs = qs + 'ts=' + encodeURI(dt.asSeconds().toStr()) + '&';
      } else if (s[o] !== '') {
        qs = qs + 'ts=' + encodeURI(s[o]) + '&';
      }
    } else if (o === 'pagename' && s[o] !== '') {
      qs = qs + 'pageName=' + encodeURI(s[o]) + '&';
    } else if (o === 'pagetype' && s[o] !== '') {
      qs = qs + 'gt=' + encodeURI(s[o]) + '&';
    } else if (o === 'products' && s[o] !== '') {
      qs = qs + 'pl=' + encodeURI(s[o]) + '&';
    } else if (o === 'purchaseid' && s[o] !== '') {
      qs = qs + 'pi=' + encodeURI(s[o]) + '&';
    } else if (o === 'server' && s[o] !== '') {
      qs = qs + 'sv=' + encodeURI(s[o]) + '&';
    } else if (o === 'charset' && s[o] !== '') {
      qs = qs + 'ce=' + s[o] + '&';
    } else if (o === 'visitornamespace' && s[o] !== '') {
      qs = qs + 'ns=' + s[o] + '&';
    } else if (o === 'currencycode' && s[o] !== '') {
      qs = qs + 'cc=' + s[o] + '&';
    } else if (o === 'channel' && s[o] !== '') {
      qs = qs + 'ch=' + encodeURI(s[o]) + '&';
    } else if (o === 'transactionid' && s[o] !== '') {
      qs = qs + 'xact=' + encodeURI(s[o]) + '&';
    } else if (o === 'campaign' && s[o] !== '') {
      qs = qs + 'v0=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'prop' && s[o] !== '') {
      qs = qs + 'c' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'evar' && s[o] !== '') {
      qs = qs + 'v' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'list' && s[o] !== '') {
      qs = qs + 'l' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'hier' && s[o] !== '') {
      qs = qs + 'h' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o === 'linktype' && s[o] !== '') {
      qs = qs + 'pe=lnk_' + s[o] + '&';
    } else if (o === 'linkurl' && s[o] !== '') {
      qs = qs + 'pev1=' + encodeURI(s[o]) + '&';
    } else if (o === 'linkname' && s[o] !== '') {
      qs = qs + 'pev2=' + encodeURI(s[o]) + '&';
    } else if (o === 'state' && s[o] !== '') {
      qs = qs + 'state=' + encodeURI(s[o]) + '&';
    } else if (o === 'zip' && s[o] !== '') {
      qs = qs + 'zip=' + encodeURI(s[o]) + '&';
    } else if (o === 'profileID' && s[o] !== '') {
      qs = qs + 'mtp=' + encodeURI(s[o]) + '&';
    }
  }

  var contextvars = 0;
  
  var cqs = 'c.' + '&';
  for (var key in contextData) {
    cqs = cqs + encodeURIComponent(key) + '=' + encodeURIComponent(contextData[key]) + '&';
    contextvars = contextvars + 1;
    if (s.debugTracking) {
      console.log('debug', 'siteCatalytic', 'context data', contextData[key], key);
    }
  }
  cqs = cqs + '.c' + '&';
  
  if (contextvars > 0) {
    qs = qs + cqs;
  }
  //cap the query string
  url = url + qs + 'AQE=1';
    
  //send the request
  if (s.debugTracking) {
    console.log('debug', 'siteCatalytic', 'send catalytic request: ' + url);
  }

  //write code to send requrest using Ajax
  var ajax = new ATVUtils.Ajax({
    url: url,
    headers: {},
    success: function(xhr) {
      console.log('successfully send omniture tracking request');
    },
    failure: function(status, xhr) {
      console.log('Failure sending request: ', status, xhr);
    }
  });

  
  return 1;
};
define("aetn/appletv/AppMeasurement", function(){});

