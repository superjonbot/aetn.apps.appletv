<?php

return array(
	'brand' => array(
		'history' => 'HISTORY',
		'ae' => 'A&E',
		'mlt' => 'Lifetime',
		'fyi' => 'FYI'
	),
	'menu_title' => array(
		'history' => 'History',
		'ae' => 'A&E',
		'mlt' => 'Lifetime',
		'fyi' => 'FYI'
	),
	'logos' => array(
		'history' => array(
			'720' 	=> '/images/history/history@720.png',
			'1080'	=> '/images/history/history@1080.png'
		),
		'ae' => array(			
			'720' 	=> '/images/ae/aetv@720.png',
			'1080'	=> '/images/ae/aetv@1080.png'
		),
		'mlt' => array(
			'720' 	=> '/images/mlt/lifetime@720.png',
			'1080'	=> '/images/mlt/lifetime@1080.png'
		),
		'fyi' => array(
			'720' 	=> '/images/fyi/fyi@720.png',
			'1080'	=> '/images/fyi/fyi@1080.png'
		)
	),
	'about' => array(
      'history' => "Get access to full episodes and exclusive videos from all your favorite HISTORY series, as well as award-winning topical videos, all available whenever you want. New video is added every day, so you'll always have something to watch. HISTORY Made Every Day. Now on your Apple TV.",
      'mlt' => "Watch your favorite Lifetime programming whenever you want on Apple TV! Check out full episodes of your favorite shows, full movies, video clips and more.",
      'ae' => "Watch all your favorite A&E shows on your Apple TV. Get unlimited access to full episodes and clips, all available whenever you want. New content is added every day, so you'll always have something different to watch!"
    ),
    'firstTierTVEProviders' => array('Brighthouse', 'Cablevision', 'Comcast', 'DTV', 'TWC', 'Verizon', 'telus_auth-gateway_net', 'ShawGo', 'Dish'),
	'nav' => array(
		
		'history' => array(
			array(
			 	'title' => "HISTORY",
			  	'url'   => "/section/shows"
		    ),
		  	array(
				'title' => "H2",
				'url'   => "/section/h2"
		  	),
		  	array(
				'title' => "Topics",
				'url'   => "/section/topics"
		  	),
		  	array(
				'title' => "Watchlist",
				'url'   => "/watchlist"
		  	),
		  	array(
				'title' => "Search",
				'url'   => "/search"
		  	),
		  	array(
				'title' => "Settings",
				'url'   => "/settings/index"
		  	)
		),

		'ae' => array(
		  	array(
			 	'title' => "A&amp;E",
			  	'url'   => "/section/shows"
		    ),
		  	array(
				'title' => "Watchlist",
				'url'   => "/watchlist"
		  	),
		  	array(
				'title' => "Search",
				'url'   => "/search"
		  	),
		  	array(
				'title' => "Settings",
				'url'   => "/settings/index"
		  	)	
		),
			
		'mlt' => array(
			array(
			 	'title' => "Lifetime",
			  	'url'   => "/section/shows"
		    ),
		  	array(
				'title' => "Movies",
				'url'   => "/section/movies"
		  	),
		  	array(
				'title' => "Watchlist",
				'url'   => "/watchlist"
		  	),
		  	array(
				'title' => "Search",
				'url'   => "/search"
		  	),
		  	array(
				'title' => "Settings",
				'url'   => "/settings/index"
		  	)
		),

		'fyi' => array(
		  	array(
			 	'title' => "FYI",
			 	'accessibilityTitle' => "F.Y.I",
			  	'url'   => "/section/shows"
		    ),
		  	array(
				'title' => "Watchlist",
				'url'   => "/watchlist"
		  	),
		  	array(
				'title' => "Search",
				'url'   => "/search"
		  	),
		  	array(
				'title' => "Settings",
				'url'   => "/settings/index"
		  	)	
		),
	)
);