<?php

return array(
    'home'   => (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] === 'qaplayer.aetndigital.com') ? 'http://' . $_SERVER['SERVER_NAME'] . '/pservice/appletv' : isset($_SERVER['SERVER_NAME']) ? 'http://' . $_SERVER['SERVER_NAME'] : '',
    'menu_title' => array(
        'history' => 'HISTORY QA',
        'ae' => 'A&E QA',
        'mlt' => 'Lifetime QA',
        'fyi' => 'FYI QA'
    ),
    'assets' => array(
        'history' => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.history.qa.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.history.qa.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.history.qa.js'
        ),
        'ae'      => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.ae.qa.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.ae.qa.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.ae.qa.js'
        ),
        'mlt'     => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.mlt.qa.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.mlt.qa.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.mlt.qa.js'
        ),
        'fyi'      => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.fyi.qa.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.fyi.qa.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.fyi.qa.js'
        ),
    ),
    'app'    => array(
        'history' => array(
            'version'   => '1.1',
            'bundle'    => 'history',
            'merchant'  => 'com.aenetworks.history.appletv.stage'
        ),
        'ae' => array(
            'version'   => '1.1',
            'bundle'    => 'ae',
            'merchant'  => 'com.aenetworks.appletv.stage'
        ),
        'mlt' => array(
            'version'   => '1.1',
            'bundle'    => 'mlt',
            'merchant'  => 'com.aenetworks.lifetime.appletv.stage'
        ),
        'fyi' => array(
            'version'   => '1.1',
            'bundle'    => 'fyi',
            'merchant'  => 'com.aenetworks.fyi.appletv.stage'
        )
    ),
    'mpx'   => array(
        'history' => array(
            'titleFeed'     => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/just-added2/history'
        ),
        'ae' => array(
            'titleFeed'     => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles2',
            'justAddedFeed' => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/just-added2/ae'
        ),
        'mlt' => array(
            'titleFeed'     => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/just-added2/mlt'
        ),
        'fyi' => array(
            'titleFeed'     => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/just-added2/fyi'
        )
    ),
    'common' => array(
        'topicsFeed'        =>  'http://www.history.com/feed/topics/title/appletv',
        'moviesFeed'        =>  'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/movies/mlt',
        'showsFeed'         =>  array(
            'history'  =>  'http://wombatapi.aetv.com/shows',
            'mlt'      =>  'http://wombatapi.aetv.com/shows',
            'ae'       =>  'http://wombatapi.aetv.com/shows2',
            'fyi'      =>  'https://mobileservices-a.akamaihd.net/jservice/wombattpservice/shows'
        ), 
        'featuredFeed'  =>  'http://wombatapi.aetv.com/features'
    ),
    'acsEndPointURL' => array(
        'history'   => 'http://search-thc7-watchapp-appletv-33wsf44mnwngr3ii5pb5kc6pje.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'ae'        => 'http://search-aetv7-watchapp-appletv-hnuq6lqufoy42cynvhjvkxrs5m.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'mlt'       => 'http://search-mlt7-watchapp-appletv-scqcwr4rt2hlfdsg2apxrqoydu.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'fyi'      =>  'http://search-fyi7-watchapp-k6kqkpujtvxtbq5lx43h6jqhne.us-east-1.cloudsearch.amazonaws.com/2011-02-01'
    ),
    'tpTokenAuthUrl' => 'https://servicesaetn-a.akamaihd.net/jservice/video/components/get-signed-signature'    
);