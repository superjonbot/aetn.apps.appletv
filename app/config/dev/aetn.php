<?php

return array(
    'home'   => (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] === 'devplayer.aetndigital.com') ? 'http://' . $_SERVER['SERVER_NAME'] . '/pservice/appletv' : isset($_SERVER['SERVER_NAME']) ? 'http://' . $_SERVER['SERVER_NAME'] : '',
    'menu_title' => array(
        'history' => 'HISTORY Dev',
        'ae' => 'A&E Dev',
        'mlt' => 'Lifetime Dev',
        'fyi' => 'F Y I Dev'
    ),
    'assets' => array(
        'history' => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.history.dev.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.history.dev.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.history.dev.js'
        ),
        'ae'      => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.ae.dev.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.ae.dev.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.ae.dev.js'
        ),
        'mlt'     => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.mlt.dev.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.mlt.dev.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.mlt.dev.js'
        ),
        'fyi'      => array(
            'mainjs' => '/js/build/compiled/aetn.appletv.main.fyi.dev.js',
            'appjs'  => '/js/build/compiled/aetn.appletv.app.fyi.dev.js',
            'authjs' => '/js/build/compiled/aetn.appletv.auth.fyi.dev.js'
        ),
    ),
    'app'    => array(
        'history' => array(
            'version'   => '1.1',
            'bundle'    => 'history',
            'merchant'  => 'com.aenetworks.history.appletv.dev'
        ),
        'ae' => array(
            'version'   => '1.1',
            'bundle'    => 'ae',
            'merchant'  => 'com.aenetworks.appletv.dev'
        ),
        'mlt' => array(
            'version'   => '1.1',
            'bundle'    => 'mlt',
            'merchant'  => 'com.aenetworks.lifetime.appletv.dev'
        ),
        'fyi' => array(
            'version'   => '1.1',
            'bundle'    => 'fyi',
            'merchant'  => 'com.aenetworks.fyi.appletv.dev'
        )
    ),
    'mpx'   => array(
        'history' => array(
            'titleFeed'     => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/just-added2/history'
        ),
        'ae' => array(
            'titleFeed'     => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles2',
            'justAddedFeed' => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/just-added2/ae'
        ),
        'mlt' => array(
            'titleFeed'     => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/just-added2/mlt'
        ),
        'fyi' => array(
            'titleFeed'     => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles',
            'justAddedFeed' => 'http://devmobileservices.aetndigital.com/jservice/wombattpservice/just-added2/fyi'
        )
    ),
    'common' => array(
        'topicsFeed'        =>  'http://www.history.com/feed/topics/title/appletv',
        'moviesFeed'        =>  'http://devmobileservices.aetndigital.com/jservice/wombattpservice/movies/mlt',
        'showsFeed'         =>  array(
            'history'  =>  'http://wombatapi.aetv.com/shows',
            'mlt'      =>  'http://wombatapi.aetv.com/shows',
            'ae'       =>  'http://wombatapi.aetv.com/shows2',
            'fyi'      =>  'http://devmobileservices.aetndigital.com/jservice/wombattpservice/shows'
        ), 
        'featuredFeed'  =>  'http://watchappapi.aetv.com/features'
    ),
    'acsEndPointURL' => array(
        'history'   => 'http://search-thc7-watchapp-appletv-33wsf44mnwngr3ii5pb5kc6pje.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'ae'        => 'http://search-aetv7-watchapp-appletv-hnuq6lqufoy42cynvhjvkxrs5m.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'mlt'       => 'http://search-mlt7-watchapp-appletv-scqcwr4rt2hlfdsg2apxrqoydu.us-east-1.cloudsearch.amazonaws.com/2011-02-01',
        'fyi'      =>  'http://search-fyi7-watchapp-k6kqkpujtvxtbq5lx43h6jqhne.us-east-1.cloudsearch.amazonaws.com/2011-02-01'
    ),
    'tpTokenAuthUrl' => 'https://servicesaetn-a.akamaihd.net/jservice/video/components/get-signed-signature'    
);