<?php

class HomeController extends BaseController
{   
    public function bag($site)
    {
        $content = View::make(
            'home.bag',
            array(
                'menu_title'  => htmlspecialchars(Config::get('aetn.menu_title.' . $site), ENT_QUOTES, 'UTF-8'),
                'logos'       => Config::get('aetn.logos.' . $site),
                'app'         => Config::get('aetn.app.' . $site),
                'home'        => Config::get('aetn.home'),
                'js'          => Config::get('aetn.assets.' . $site . '.appjs'),
            )
        );

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function nav($site, $navItems = null)
    {       
        $content =  View::make('home.nav', array(
            'home'       => Config::get('aetn.home'),
            'js'         => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'       => $site,
            'navItems'   => ($navItems)? $navItems : Config::get('aetn.nav.' . $site)
        ));

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function navNoWatchlist($site) 
    {
        $navItems = Config::get('aetn.nav.' . $site);
        $cleanupNavs = array();
        
        foreach ($navItems as $nav) {
            if ($nav['title'] !== 'Watchlist') 
                array_push($cleanupNavs, $nav);
        }
        return $this->nav($site, $cleanupNavs);
    }

    public function section($site, $section, $mvpd = null)
    { 
        switch ($section)
        {
            case 'shows':
                $showService = new Show();
                if ($site === 'history')
                    $shows = $showService->getShows($site, 'appletv', 'History');
                else
                    $shows = $showService->getShows($site, 'appletv', null);

                $content = $this->makeSectionView('home.section', $shows, $site, 'Shows', $mvpd);

                $featureService = new Featured();
                $showcaseItems = $featureService->get($site, 'appletv');

                $justAddedService = new JustAdded();
                $justAddedVideos = $justAddedService->get($site, 'appletv');
                break;

            case 'h2' :
                $showService = new Show();
                $shows = $showService->getShows($site, 'appletv', 'H2');
                
                $content = $this->makeSectionView('home.section', $shows, $site, 'H2 Shows', $mvpd);
                break;

            case 'topics':
                $topicsService = new Topics();
                $topics = $topicsService->getTopics('appletv');

                $content = $this->makeSectionView('home.topics', $topics, $site, 'Topics', $mvpd);
                break;

            case 'movies':
                $movieService = new Movies( $site, 'appletv');
                $args = array(
                    'deviceId' => 'appletv');
                if ($mvpd === 'isInRetailDemoMode') {
                    $args['is_behind_wall'] = 'false';
                }

                $movies = $movieService->getMovies($site, $args);

                $content = $this->makeSectionView('home.movies', $movies, $site, 'Movies', $mvpd);
                break;
        }

        if (isset($showcaseItems)) {
            $content = $content->with('showcaseItems', $showcaseItems);
        }
         
        if (isset($justAddedVideos)) {
            $content = $content->with('justAddedVideos', $justAddedVideos);
        }
         
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }


    private function makeSectionView($template, $items, $site, $heading, $mvpd)
    {
        
        $mvpdLogoService = new MvpdLogo();
        $mvpdLogo = $mvpdLogoService->getMvpdLogo($mvpd, $site);

        $reloadUrl = Request::url();
        if ($mvpd !== null) {
            $reloadUrl = str_replace('/' . $mvpd, '', $reloadUrl);
        }

        $content = View::make($template, array(
            'home'      => Config::get('aetn.home'),
            'js'        => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'      => $site,
            'items'     => $items,
            'heading'   => $heading,
            'mvpd'      => ($mvpd) ? $mvpd : false,
            'mvpdLogo'  => $mvpdLogo,
            'reloadUrl' => $reloadUrl
        ));

        return $content;
    }

}