<?php

class WatchListController extends BaseController
{
    public function indexAll( $site, $skipList = null, $mvpd = null)
    {
        $mvpdLogoService = new MvpdLogo();
        $mvpdLogo = $mvpdLogoService->getMvpdLogo($mvpd, $site);

        $reloadUrl = Config::get('aetn.home').'/'.$site.'/watchlist';

        $content = View::make('watchlist.index', array(
          'home'     => Config::get('aetn.home'),
          'js'       => Config::get('aetn.assets.' . $site . '.mainjs'),
          'site'     => $site,
          'hideList' => $skipList,
          'mvpdLogo' => $mvpdLogo,
          'reloadUrl' => $reloadUrl
        ));
        
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response; 
    }

    public function index ( $site, $mvpd = null) 
    {
      return $this->indexAll($site, null, $mvpd);
    }

    public function emptyList( $site, $mvpd = null)
    {
        $mvpdLogoService = new MvpdLogo();
        $mvpdLogo = $mvpdLogoService->getMvpdLogo($mvpd, $site);

        $content = View::make('watchlist.empty', array(
          'home'     => Config::get('aetn.home'),
          'js'       => Config::get('aetn.assets.' . $site . '.mainjs'),
          'site'     => $site,
          'mvpdLogo' => $mvpdLogo
        ));

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response; 
    }

    public function emptyQueueList( $site, $mvpd = null  )
    {
        return $this->indexAll($site, 'queue', $mvpd);
    }

    public function emptyProgressList($site, $mvpd = null )
    {
        return $this->indexAll($site, 'progress', $mvpd);
    }
}