<?php

class ShowController extends BaseController
{
	public function allSeasons($site, $show_name, $mvpd = null) {
		$show             = new Show();
        $showData         = $show->getShow($site, $show_name);
        $seasons = $show->getShowSeasons($site, $show_name, 'appletv', $mvpd);


        $viewData = array(
          'showData'  => $showData[0],
          'seasons'   => $seasons,
          'home'      => Config::get('aetn.home'),
          'js'        => Config::get('aetn.assets.' . $site . '.mainjs'),
          'site'      => $site,
          'showName'  => $show_name,
          'mvpd'      => $mvpd
        );

        $content  = View::make('shows.show-navigation', $viewData);
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;

	}

	public function seasonVideos($site, $show_name, $season_no, $other_type, $mvpd = null) {
		return $this->season($site, $show_name, $season_no, null, $other_type, $mvpd);
	}

	public function season($site, $show_name, $season_no, $videoId, $other_type, $mvpd = null) {
		$show = new Show();
        $showData = $show->getShow($site, $show_name);
        $seasonData = $show->getSeasonVideos($site, $show_name, $season_no, 'appletv', $mvpd);

        $pos = 0;
        if (isset($videoId)) {
            $videoInfo = explode('-', $videoId);
            for ($i = 0; $i < count($seasonData['episodes']); $i++) {
                if ((string) $seasonData['episodes'][$i]['thePlatformId'] === $videoInfo[0]) {
                    $pos = $i;
                    break;
                }
            }
            if ($videoInfo[1] === 'new' && $pos < (count($seasonData['episodes']) - 1)) {
                $pos += 1;
            }
        }


        $viewData = array(
            'seasonData'  => $seasonData,
            'showName'    => $show_name,
            'showImage'   => $showData[0]['gridImageURL2x'],
            'home'        => Config::get('aetn.home'),
            'js'          => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'        => $site,
            'episodes'    => $seasonData['episodes'],
            'clips'       => $seasonData['clips'],
            'season_no'   => $season_no,
            'hasOthers'   => ($other_type === 'OtherSeasons')? true : false,
            'mvpd'        => $mvpd ? $mvpd : false,
            'pos'         => (isset($pos))? $pos : 0,
            'reloadUrl'   => Config::get('aetn.home').'/'.$site.'/show/',
            'network'     => preg_replace ( '(&| )', '', $showData[0]['network'] )
        );

        $content = View::make('shows.season', $viewData);
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;

	}

	public function defaultSeason($site, $show_name, $video_info, $mvpd = null) {
		$show = new Show();
        $videoId = null;
        if (isset($video_info) && $video_info !== 'default' && $video_info !== 'episodes' && $video_info !== 'clips') {
            //passed video info is: season-videoid-vidoeToUse-mvpd
            $videoInfo = explode('-', $video_info);
            $lastVideoSeason = $videoInfo[0]; 
            $videoId = $videoInfo[1] . '-' . $videoInfo[2];
        } 
        $showData = $show->getShow($site, $show_name);
        $seasons = $show->getShowSeasons($site, $show_name, 'appletv', $mvpd);
        

        if (isset($lastVideoSeason)) {
          $pos_season = 0;
          for ($i = 0; $i < count($seasons); $i++) {
            if ($lastVideoSeason === $seasons[$i]) {
              $pos_season = $i;
              break;
            }
          }
          $seasonNum = $seasons[$pos_season];
        }
        
        if (!isset($seasonNum)) {
          if (empty($showData[0]['tuneInInfo'])) { //use oldest season
            if (count($seasons) === 0) {
                return $this->error();
            }
            $seasonNum = $seasons[0];
            if ($seasons[0] === '0' && count($seasons) > 1) {
                $seasonNum = $seasons[1];
            } else {
                $seasonNum = $seasons[0];
            }
          } else { //use most current season
            $seasonNum = $seasons[count($seasons) - 1];
          }
        }

        $otherType = 'OtherSeasons';
        //deal with only one season show
        if (count($seasons) === 1){
        	$otherType = "None";
        }


        return $this->season($site, $show_name, $seasonNum,  $videoId, $otherType, $mvpd);
	}

    

}