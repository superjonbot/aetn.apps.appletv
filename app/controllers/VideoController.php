<?php

class VideoController extends BaseController
{
    public function index($site, $mpxId, $mvpd = null)
    {
        $videoService = new Video();
        $video = $videoService->getVideoDetail($site, $mpxId, $mvpd);

        if (is_null($video)) {
            
            return $this->error();

        }

        $videoObj = $this->buildVideoObj($video);
        $video['season'] = isset($video['season']) ? $video['season'] : '0';

        $subtitle = '';
        if ($video['tvNtvMix'] === 'Show') {
            if( isset($video['season']) && $video['season'] !== '0')
                $subtitle .= 'Season ' . $video['season'];

            if (isset($video['episode']) && $video['episode'] !== '0')
                $subtitle .= ' / Episode ' . $video['episode'];
        }
        
        $relatedVideos = $videoService->getRelatedVideos($site, $video, $mvpd);


        foreach($relatedVideos as &$relatedVideo) {

            $relatedVideoObj = $this->buildVideoObj($relatedVideo);
            
            $relatedVideo['showEpisodeNo'] = 
                (isset($relatedVideo['longForm']) &&  $relatedVideo['longForm'] === 'true') 
                && (isset($relatedVideo['episode']) && $relatedVideo['episode'] !== '0')
                && (isset($relatedVideo['tvNtvMix']) && $relatedVideo['tvNtvMix'] !== 'Movie') ?
                true : false;

            if (isset($relatedVideo['isNext']))
                error_log("found isNext");
            if (isset($relatedVideo['isNext']) && $relatedVideo['isNext']) {
                $video['nextVideoId'] = $relatedVideo['thePlatformId'];
            }
        }

        $mvpdLogoService = new MvpdLogo();   
        $mvpdLogo = $mvpdLogoService->getMvpdLogo($mvpd, $site);

        $reloadUrl = Request::url();
        if ($mvpd !== null) {
            $reloadUrl = str_replace('/' . $mvpd, '', $reloadUrl);
        }


        $content = View::make('video.detail', array(
            'home'          => Config::get('aetn.home'),
            'js'            => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'          => $site,
            'video'         => $video,
            'subtitle'      => $subtitle,
            'relatedVideos' => $relatedVideos,
            'mvpd'          => $mvpd ? $mvpd : false,
            'mvpdLogo'      => $mvpdLogo,
            'reloadUrl'     => $reloadUrl,
            'videoType'     => $video['tvNtvMix'] === 'Movie'? 'movie' : ( isset($video['longForm']) &&  $video['longForm'] === 'true' ? 'episode' : 'clip' ),  //Currently not used in the view
            'relatedText'   => $video['tvNtvMix'] === 'Movie'? 'Other Movies' : ($video['longForm'] === 'true' ? 'Other Episodes' : 'Other Clips')
        ));

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function showVideo($site, $showname, $mpxId, $mvpd = null)
    {
        return $this->index($site, $mpxId, $mvpd);
    }

    public function videoJSON($site, $mpxId) 
    {
        $videoService = new Video();
        $video = $videoService->getVideoDetail($site, $mpxId);

        $videoData = $this->buildVideoObj($video);

        $response = Response::json($videoData);
        $response->header('Content-Type', 'application/json');

        return $response;
    }


    private function buildVideoObj($video) {

        $videoObj = $video;

        $videoObj['network']  = htmlentities($video['network']);
        $videoObj['mediaURL'] = str_replace('switch=hls', 'switch=hlsx', $video['playURL_HLS']);
        $videoObj['mediaURL'] = str_replace('medium_video_s3', 'high_video_s3', $videoObj['mediaURL']);

        if(isset($video['programID'])) {
            $videoObj['programID'] = $video['programID'];
        }
        if(isset($video['season'])) {
            $videoObj['season'] = $video['season'];
        }
        if(isset($video['episode'])) {
            $videoObj['episode'] = $video['episode'];
        }
        if(isset($video['chapters'])) { 
            $videoObj['chapters'] =  $video['chapters'];
        }
        if(isset($video['seriesName'])) {
            $videoObj['seriesName'] = $video['seriesName'];
        }
        if(isset($video['nextVideoId']) && $video['nextVideoId'] > 0) {
            $videoObj['nextVideoId'] = $video['nextVideoId'];
        }

        return $videoObj; 
    }

}