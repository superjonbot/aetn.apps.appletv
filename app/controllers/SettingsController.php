<?php

class SettingsController extends BaseController
{
    public function index($site, $mvpd = null)
    {
        $reloadUrl = Request::url();
        if ($mvpd !== null) {
            $reloadUrl = str_replace('/' . $mvpd, '', $reloadUrl);
        }
        $content = View::make('settings.index', array(
            'home'          => Config::get('aetn.home'),
            'js'            => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'          => $site,
            'mvpd'          => $mvpd ? $mvpd : false,
            'reloadUrl'     => $reloadUrl
        ));
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function about($site)
    {
        $brand = Config::get('aetn.brand.'.$site);
        $about = Config::get('aetn.about.'.$site);

        $content = View::make('settings.about', array (
            'brand'     => $brand,
            'text'      => $about
        ));

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function faq($site)
    {
        $brand    = Config::get('aetn.brand.'.$site);
        $content  = View::make('settings.faq', array ( 'brand' => $brand ) );
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function privacyPolicy()
    {
        $content  = View::make('settings.privacy');
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function termsConditions()
    {
        $content  = View::make('settings.terms');
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function adChoices()
    {
        $content  = View::make('settings.adchoices');
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function contactUs()
    {
        $content  = View::make('settings.contactus');
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }
}