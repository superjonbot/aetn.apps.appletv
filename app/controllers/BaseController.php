<?php

class BaseController extends Controller
{
	public function error() {

        $content  = View::make('error');
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }
}