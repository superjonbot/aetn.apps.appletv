<?php

class TVEController extends BaseController
{
    public function activate($site)
    {
        $content = View::make('tve.activation', array(
            'home'     => Config::get('aetn.home'),
            'js'       => Config::get('aetn.assets.' . $site . '.authjs'),
            'site'     => $site
        ));
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function deactivate($site)
    {
        $brand   = Config::get('aetn.brand.' . $site);
        $content = View::make('tve.deactivate', array(
            'home'     => Config::get('aetn.home'),
            'js'       => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'     => $site,
            'brand'    => $brand
        ));
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function success($site)
    {
        $brand   = Config::get('aetn.brand.' . $site);
        $content = View::make('tve.success', array(
            'home'     => Config::get('aetn.home'),
            'js'       => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'     => $site,
            'brand'    => $brand
        ));
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function fail($site)
    {
        $brand   = Config::get('aetn.brand.' . $site);
        $content = View::make('tve.fail', array(
            'home'     => Config::get('aetn.home'),
            'js'       => Config::get('aetn.assets.' . $site . '.mainjs'),
            'site'     => $site,
            'brand'    => $brand
        ));
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }
}