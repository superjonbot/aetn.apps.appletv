<?php

class SearchController extends BaseController
{
    public function index($site, $mvpd = null)
    {
        $mvpdLogoService = new MvpdLogo();   
        $mvpdLogo = $mvpdLogoService->getMvpdLogo($mvpd, $site);

        $content = View::make('search.search', array(
            'home'     => Config::get('aetn.home'),
            'site'     => $site,
            'mvpdLogo' => $mvpdLogo,
            'freeOnly' => (isset($mvpd) && ($mvpd === 'isInRetailDemoMode'))? true : false
        ));
           
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function searchResults($site, $freeOnly = false)
    {
        $searchTerm = Input::get('searchTerm');

        $searchService = new Search( $site );
        $searchResults = $searchService->getSearchResults($searchTerm, $freeOnly);
        
        $content = View::make('search.results', 
            array(
                'home'         => Config::get('aetn.home'),
                'js'           => Config::get('aetn.assets.' . $site . '.mainjs'),
                'site'         => $site,
                'searchTerm'   => $searchTerm,
                'showResults'  => $searchResults['showResults'],
                'videoResults' => $searchResults['videoResults']
            )
        );

        $response = Response::make($content, 200);
        $response->header('Content-Type', 'application/xml');
        return $response;
    }

    public function searchResultsFreeOnly($site) {

        return $this->searchResults($site, true);
    }
}