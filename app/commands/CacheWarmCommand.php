<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CacheWarmCommand extends Command {

	protected $name = 'cache:warm';

	protected $description = 'Warm apple tv cache.';

	protected $site = 'history';

	protected $home = '';

	public function __construct()
	{
		parent::__construct();
	}

    protected function getOptions()
    {
        return array(
            array('site', 'history', InputOption::VALUE_REQUIRED, 'Site: history, ae, mlt, fyi'),
            array('home', 'https://appletv.aetndigital.com', InputOption::VALUE_OPTIONAL, 'Home URL')
        );
    }

	public function fire()
	{
		$this->homeParam = $this->option('home');

		if ( $this->homeParam ) {
			$this->home = $this->homeParam;
		}
		else {
			$this->home = Config::get('aetn.home');
		}

		$this->site = $this->option( 'site' );
		$shows_feed = Config::get( 'aetn.common.showsFeed.' . $this->site ) . "/{$this->site}";

		$this->info( "{$this->home}/{$this->site}/section/shows" );
		Http::get( "{$this->home}/{$this->site}/section/shows", array('proxy' => '127.0.0.1:80') );

		switch( $this->site )
		{
			case 'history':
				$this->info( "{$this->home}/{$this->site}/h2" );
				$this->info( "{$this->home}/{$this->site}/topics" );

				Http::get( "{$this->home}/{$this->site}/h2", array('proxy' => '127.0.0.1:80') );
				Http::get( "{$this->home}/{$this->site}/topics", array('proxy' => '127.0.0.1:80') );
				$this->processShowFeed( $shows_feed );
				break;

			case 'ae':
				$this->processShowFeed( $shows_feed );
				break;

			case 'mlt':
				$this->info( "{$this->home}/{$this->site}/movies" );
				Http::get( "{$this->home}/{$this->site}/movies", array('proxy' => '127.0.0.1:80') );
				$this->processShowFeed( $shows_feed );
				break;
				
			case 'fyi':
				$this->processShowFeed( $shows_feed );
				break;
		}
	}

	public function processShowFeed( $url )
	{
		$this->info( $url );

		$response = Http::get( $url );

		if( $response[ 'status' ] !== 200 )
		{
			$this->error( "Error Fetching $url" );
			return false;
		}

		$shows = json_decode( $response[ 'body' ] );

		foreach( $shows as $show )
		{
			$show_name = rawurlencode( $show->showID );
			
			$this->info( "{$this->home}/{$this->site}/show/{$show_name}/default" );
			$this->info( "{$this->home}/{$this->site}/show/{$show_name}/all" );

			Http::get( "{$this->home}/{$this->site}/show/{$show_name}/default", array('proxy' => '127.0.0.1:80') );
			Http::get( "{$this->home}/{$this->site}/show/{$show_name}/all", array('proxy' => '127.0.0.1:80') );

			$this->processEpisodeFeed( $show_name, $this->buildFeedUrl($show_name, $show->episodeFeedURL, 'episode') );
			$this->processClipFeed( $show_name, $this->buildFeedUrl($show_name, $show->clipFeedURL, 'clip') );
		}
	}

	public function buildFeedUrl( $show_name, $feedUrl, $videotype )
	{ 
		if ( $feedUrl !== '') {
			return $feedUrl;
		}
		else {
			$titleFeed = Config::get('aetn.mpx.' . $this->site . '.titleFeed');
			return $titleFeed . '/' . $videotype . '/' . $this->site . '?show_name=' . $show_name;
		}
	}

	public function processEpisodeFeed( $show_name, $url )
	{
		$this->info( $url );

		$response = Http::get( $url );

		if( $response[ 'status' ] !== 200 )
		{
			$this->error( "Error Fetching $url" );
			return false;
		}

		$episodes = json_decode( $response[ 'body' ] );

		$seasons = array();

		foreach( $episodes->Items as $episode )
		{
			if( isset( $episode->season ) )
				$seasons[ $episode->season ] = 1;
		}

		foreach( $seasons as $season => $value )
		{
			$this->info( "{$this->home}/{$this->site}/show/{$show_name}/season/{$season}/none" );
			Http::get( "{$this->home}/{$this->site}/show/{$show_name}/season/{$season}/none", array('proxy' => '127.0.0.1:80')  );
		}
	}

	public function processClipFeed( $show_name, $url )
	{
		$this->info( $url );

		$response = Http::get( $url );

		if( $response[ 'status' ] !== 200 )
		{
			$this->error( "Error Fetching $url" );
			return false;
		}

		$clips = json_decode( $response[ 'body' ] );

		$seasons = array();

		foreach( $clips->Items as $clip )
		{
			if( isset( $clip->season ) )
				$seasons[ $clip->season ] = 1;
		}

		foreach( $seasons as $season => $value )
		{
			$this->info( "{$this->home}/{$this->site}/show/{$show_name}/season/{$season}/clips" );
			Http::get( "{$this->home}/{$this->site}/show/{$show_name}/season/{$season}/clips", array('proxy' => '127.0.0.1:80')  );
		}
	}
}
