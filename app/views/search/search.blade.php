<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <body>
    <search id = "com.aetn.search">
      <header>
        <simpleHeader accessibilityLabel = "Search"> 
          <title><![CDATA[Search]]></title> 
          <image id = "brand_logo" required="true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></image> 
        </simpleHeader>
      </header>
      @if ($freeOnly)
      <baseURL>{{ $home }}/{{ $site }}/search-results-free?searchTerm=</baseURL>
      @else
      <baseURL>{{ $home }}/{{ $site }}/search-results?searchTerm=</baseURL>
      @endif
    </search>
  </body>
</atv>