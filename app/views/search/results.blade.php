<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <searchResults id = "com.aetn.searchResults">
        <menu>
            <sections>
            @if (count($showResults) > 0)
              <menuSection>
                <header>
                  <horizontalDivider alignment="center" accessibilityLabel = "TV shows">
                    <title>TV Shows</title>
                  </horizontalDivider>
                </header>
                <items>
                  @foreach ($showResults as $index => $show)
                  <twoLineMenuItem
                    id = "list_{{ $index }}"
                    accessibilityLabel = "{{ htmlspecialchars($show['title'], ENT_QUOTES, 'UTF-8') }}"
                    onSelect = "atv.loadURL(aetn.globalContext.urlPath + '{{ $show['linkUrl']}}' )" 
                    onPlay = "atv.loadURL(aetn.globalContext.urlPath + '{{ $show['linkUrl']}}')"
                    >
                    <label><![CDATA[{{ $show['title'] }}]]></label>
                    <image>{{ $show['thumbnail'] }}</image>
                    <defaultImage>resource://16x9.png</defaultImage>
                  </twoLineMenuItem>
                  @endforeach
                </items>
              </menuSection>
            @endif
              <menuSection>
                <header>
                  <horizontalDivider alignment="center" accessibilityLabel = "Videos">
                    <title>Videos</title>
                  </horizontalDivider>
                </header>
                <items>
                    @foreach ($videoResults as $index => $video)
                    <twoLineMenuItem
                        id = "list_{{ $index }}"
                        accessibilityLabel = "{{ htmlspecialchars($video['title'], ENT_QUOTES, 'UTF-8') }}"
                        onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/video/{{ $video['thePlatformId'] }}')" 
                        onPlay   = "aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
                      >
                      <label><![CDATA[{{ $video['title'] }}]]></label>
                      <label2><![CDATA[{{ $video['seriesName'] }}]]></label2>
                      <image>{{ $video['thumbnail'] }}</image>
                      <defaultImage>resource://16x9.png</defaultImage>
                    </twoLineMenuItem>
                    @endforeach
                </items>
              </menuSection>
            </sections>
          </menu>
    </searchResults>
  </body>
</atv>