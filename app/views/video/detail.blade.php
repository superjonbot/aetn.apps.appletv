<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <itemDetail 
        id = "com.aetn.videodetail_{{ $video['thePlatformId'] }}_{{ $video['seriesNameForAnalytics']}}"
        volatile = "true"
        onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.videodetail', document, '{{ $reloadUrl }}' )"
      >
      <title><![CDATA[{{ $video['title'] }}]]></title>
      <subtitle><![CDATA[{{ $subtitle }}]]></subtitle>
      <rightImage id = "brand_logo" required="true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></rightImage>
      
      <summary><![CDATA[{{ $video['description'] }}]]></summary>
      <image style="squarePoster">{{ $video['stillImageURL'] }}</image>
      <!--<footnote>© The Studio, Inc.  All Rights Reserved.</footnote>-->
      <table>
        <columnDefinitions>
          <columnDefinition width="18" alignment="left">
            <title>Runtime</title>
          </columnDefinition>
          <columnDefinition width="24" alignment="left">
            <title>Rating</title>
          </columnDefinition>
          @if (isset($video['longForm']) &&  $video['longForm'] === 'true')
            @if (isset($video['airDate']) && $video['airDate'] !== '')
              <columnDefinition width="27" alignment="left">
                <title>Aired on</title>
              </columnDefinition>
              <columnDefinition width="27" alignment="left">
                <title>Available Until</title>
              </columnDefinition>
            @endif
          @endif
        </columnDefinitions>
        <rows id="{{ ($video['isBehindWall'] === 'true' && empty($mvpd)) ? '' : 'video_detail_rows' }}">          
          <row>
            <label>{{ (isset($video['totalVideoDuration']) && $video['totalVideoDuration'] !== '') ? round($video['totalVideoDuration']/60000) . ' min' : 'NA'}} </label>
            <label>{{ (isset($video['rating']) && $video['rating'] !== '' ) ? strtoupper($video['rating']) : 'NA' }}</label>
            @if (isset($video['longForm']) &&  $video['longForm'] === 'true')
              @if (isset($video['airDate']) && $video['airDate'] !== '')
                <label>{{ date('M d, Y', strtotime($video['airDate'])) }}</label>
                <label>{{ (isset($video['expirationDate']) && $video['expirationDate'] !== '') ? date('M d, Y', strtotime($video['expirationDate'])) : 'NA' }}</label>
              @endif
            @endif
          </row>
        </rows>
      </table>
      <centerShelf>
        <shelf id = "centerShelf_{{$video['thePlatformId']}}" columnCount="4" center="true">
          <sections>
            <shelfSection>
              <items>

                @if ($video['isBehindWall'] === 'true' && $mvpd === false)
                  <actionButton
                      id = "signin"
                      accessibilityLabel = "Sign In"
                      onSelect = "aetnUtils.initiateSignIn('{{ $video['thePlatformId'] }}')"
                      onPlay = "aetnUtils.initiateSignIn('{{ $video['thePlatformId'] }}')"
                    >
                    <title>Sign In</title>
                    <image required="true" src720 = "{{ $home }}/images/Signin.png" src1080 = "{{ $home }}/images/Signin1080.png"/>
                    <focusedImage required="true" src720 = "{{ $home }}/images/SigninFocused.png" src1080 = "{{ $home }}/images/SigninFocused1080.png"/>
                  </actionButton>
                @else
                  <actionButton 
                    id = "play" 
                    accessibilityLabel = "Play"
                    onSelect = "aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
                    onPlay   = "aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
                    >
                    <title>Play</title>
                    <image>resource://Play.png</image>
                    <focusedImage>resource://PlayFocused.png</focusedImage>
                  </actionButton>
                @endif

                @if ($mvpd !== 'isInRetailDemoMode')
                  <actionButton 
                    id = "add" 
                    accessibilityLabel = "Add to watch list" 
                    onSelect = "
                      require(['aetn/appletv/ui/watchlist'], function(Watchlist){
                        var watchlist = new Watchlist('com.aetn.videoDetail');
                        watchlist.handleWatchlistButtonClicked(
                          '{{ htmlspecialchars(addSlashes($video['title']), ENT_QUOTES, 'UTF-8') }}', 
                          '{{ $video['thePlatformId'] }}',
                          '{{ $video['stillImageURL'] }}',
                          '{{ isset($video['seriesName']) ? htmlspecialchars(addSlashes($video['seriesName']), ENT_QUOTES, 'UTF-8') : '' }}',
                          '{{ $video['isBehindWall'] }}'
                        );
                      })"
                    >
                    <title>Add</title>
                    <image>resource://Queue.png</image>
                    <focusedImage>resource://QueueFocused.png</focusedImage>
                  </actionButton>
                @endif

              </items>
            </shelfSection>
          </sections>
        </shelf>
      </centerShelf>

      @if (count($relatedVideos) > 0)
      <divider>
        <smallCollectionDivider alignment = "left" accessibilityLabel = "{{ $relatedText }}">
          <title><![CDATA[{{ $relatedText }}]]>
          </title>
        </smallCollectionDivider>
      </divider>
      <bottomShelf>
        <shelf id = "bottomShelf" columnCount = "4">
          <sections>
            <shelfSection>
              <items>
                @foreach ( $relatedVideos as $index => $relatedVideo)               
                  <squarePoster
                      id = "shelf_item_{{ $index }}" 
                      alwaysShowTitles = "true" 
                      accessibilityLabel = "{{ htmlspecialchars($relatedVideo['title'], ENT_QUOTES, 'UTF-8')  }}" 
                      related = "true" 
                      onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/video/{{ $relatedVideo['thePlatformId'] }}')"
                      onPlay = "aetnUtils.loadVideo('{{ $relatedVideo['thePlatformId'] }}', '{{ $relatedVideo['isBehindWall'] }}')"
                    >
                    <title><![CDATA[{{ ($relatedVideo['showEpisodeNo'] ? $relatedVideo['episode'] . ". " : "") . $relatedVideo['title'] }}]]></title>
                    @if(isset($relatedVideo['stillImageURL']))
                      <image src720 = "{{ $relatedVideo['stillImageURL'] }}" src1080 = "{{ $relatedVideo['stillImageURL'] }}"/>
                    @endif
                    <defaultImage>resource://16x9.png</defaultImage>
                  </squarePoster>
                @endforeach
              </items>
            </shelfSection>
          </sections>
        </shelf>
      </bottomShelf>
      @endif
    </itemDetail>
  </body>
</atv>