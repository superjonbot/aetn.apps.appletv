<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <scroller
      id = "com.aetn.watchlist"
      volatile = "true"
      onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.watchlist', document, '{{ $reloadUrl }}' )"
      >
      
      <header>
        <simpleHeader accessibilityLabel = "Watchlist" horizontalAlignment = "right">
          <title><![CDATA[Watchlist]]></title>
          <image id = "brand_logo" required = "true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></image>
        </simpleHeader>
      </header>
      
      <items id = "watchProgress">

        @if (empty($hideList) || $hideList !== 'progress')
        <collectionDivider id = "playdivider" alignment = "left" accessibilityLabel = "Continue Watching">
          <title>Continue Watching</title>
        </collectionDivider>
        <grid id = "grid_inprogress" columnCount = "4">
          <items id = 'inprogress'>
            <sixteenByNinePoster id = "placeholder" alwaysShowTitles = "true" >
              <title>No item in Queue</title>
              <defaultImage>resource://Square.png</defaultImage>
            </sixteenByNinePoster>
          </items>
        </grid>
        @endif
        
        @if (empty($hideList) || $hideList !== 'queue')
        <collectionDivider id = "queuedivider" alignment = "left" accessibilityLabel = "Watchlist Queue">
          <title>Queue</title>
        </collectionDivider>
        <grid id = "grid_queue" columnCount = "4">
          <items id = "queue">
            <sixteenByNinePoster id = "placeholder" alwaysShowTitles = "true" >
              <title>No item in Queue</title>
              <defaultImage>resource://Square.png</defaultImage>
            </sixteenByNinePoster>
          </items>
        </grid>
        @endif
      </items>

    </scroller>
  </body>
</atv>