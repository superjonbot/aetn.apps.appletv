<?xml version = "1.0" encoding = "UTF-8"?>
<atv> 
  <head>
    <script src = "{{ $home . $js }}"/>
  </head> 
  <body> 
    <listWithPreview
      id = "com.aetn.season" 
      volatile = "true" 
      onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.season', document, '{{ $reloadUrl }}', '{{ str_replace("'","\'",$showName) }}')"
      >
      <header id="{{ $network }}_show"> 
        <simpleHeader accessibilityLabel = "{{ htmlspecialchars($showName, ENT_QUOTES, 'UTF-8') }}"> 
          <title><![CDATA[{{ $showName }}]]></title> 
          <subtitle>{{($season_no === '0') ? 'All Seasons' : 'Season ' . $season_no}}</subtitle> 
        </simpleHeader> 
      </header> 
      <menu> 
        <initialSelection id = 'initialSelection'> 
          <row>{{ $pos }}</row> 
          @if ($hasOthers)
          <section>1</section> 
          @else
          <section>0</section>
          @endif
          <relevanceDate>2020-01-01T00:00:00Z</relevanceDate> 
        </initialSelection> 
        <sections> 
          @if ($hasOthers)
            <menuSection> 
              <items> 
                <oneLineMenuItem 
                  id = "other-seasons" 
                  accessibilityLabel = "Other Seasons" 
                  onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($showName) }}/all')"
                  >
                  <label>Other Seasons</label> 
                  <accessories> 
                    <arrow /> 
                  </accessories> 
                  <preview> 
                    <mediaPreview> 
                      <image src = "{{ $showImage }}" /> 
                    </mediaPreview> 
                  </preview> 
                </oneLineMenuItem> 
              </items> 
            </menuSection> 
          @endif
          @if (!empty($episodes))
          <menuSection> 
          
          <header> 
            <textDivider> 
              <title>Episodes</title> 
            </textDivider> 
          </header> 
          <items>
          
            @foreach ($episodes as $index => $video)
                <twoLineEnhancedMenuItem 
                    id = "menu_item_{{ $video['thePlatformId'] }}"
                    onSelect = "
                    document.getElementById('initialSelection').getElementByTagName('row').textContent = '{{ $index }}';
                    aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($showName) }}/video/{{ $video['thePlatformId'] }}')"
                    onPlay   = "
                    document.getElementById('initialSelection').getElementByTagName('row').textContent = '{{ $index }}';
                    aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
                  >
                  <defaultImage>resource://16x9.png</defaultImage>
                  <image src = "{{ isset($video['stillImageURL']) ? $video['stillImageURL'] : '' }}" />
                  @if( 
                      (isset($video['longForm']) &&  $video['longForm'] === 'true') && 
                      (isset($video['episode']) && $video['episode'] !== "0") 
                     )
                    <ordinal>{{ $video['episode'] }}</ordinal>
                  @endif       
                  <label><![CDATA[{{ $video['title'] }}]]></label> 
                  <accessories> 
                    @if ($video['isBehindWall'] === 'true' && empty($mvpd))
                      <lock />
                    @endif
                  </accessories>
                  <preview> 
                    <keyedPreview>
                      <title><![CDATA[{{ $video['title'] }}]]></title> 
                      <summary><![CDATA[{{ $video['description'] }}]]></summary>
                      <image src = "{{ isset($video['stillImageURL']) ? $video['stillImageURL'] : '' }}" />
                      <metadataKeys>
                        <label>Rating</label> 
                        <label>Runtime</label> 
                        @if (isset($video['longForm']) && $video['longForm'] === 'true')
                          <label>Aired on</label>
                          @if (isset($video['expirationDate']))
                            <label>Available Until</label>
                          @endif
                        @endif
                      </metadataKeys>
                      <metadataValues> 
                        <label>{{ isset($video['rating']) ? strtoupper($video['rating']) : 'NA' }}</label> 
                        <label>{{ isset($video['totalVideoDuration']) ? round($video['totalVideoDuration']/60000) : 'NA' }} min</label> 
                        @if (isset($video['longForm']) &&  $video['longForm'] === 'true')
                          <label>{{ isset($video['airDate']) ? date('M d, Y', strtotime($video['airDate'])) : 'NA' }}</label>
                          @if ( isset($video['expirationDate']))
                            <label>{{ date('M d, Y', strtotime($video['expirationDate'])) }}</label>
                          @endif
                        @endif
                      </metadataValues>
                    </keyedPreview>
                  </preview> 
                  </twoLineEnhancedMenuItem>
              @endforeach
            </items>
          </menuSection>
          @endif

          @if (!empty($clips))
          <menuSection> 
          
          <header> 
            <textDivider> 
              <title>Clips</title> 
            </textDivider> 
          </header> 
          <items>
          
            @foreach ($clips as $index => $video)
                <twoLineEnhancedMenuItem 
                    id = "menu_item_{{ $video['thePlatformId'] }}"
                    onSelect = "
                    document.getElementById('initialSelection').getElementByTagName('row').textContent = '{{ $index + count($episodes)}}';
                    aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($showName) }}/video/{{ $video['thePlatformId'] }}')"
                    onPlay   = "
                    document.getElementById('initialSelection').getElementByTagName('row').textContent = '{{ $index + count($episodes) }}';
                    aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
                  >
                  <defaultImage>resource://16x9.png</defaultImage>
                  <image src = "{{ isset($video['stillImageURL']) ? $video['stillImageURL'] : '' }}" />
                  @if( 
                      (isset($video['longForm']) &&  $video['longForm'] === 'true') && 
                      (isset($video['episode']) && $video['episode'] !== "0") 
                     )
                    <ordinal>{{ $video['episode'] }}</ordinal>
                  @endif       
                  <label><![CDATA[{{ $video['title'] }}]]></label> 
                  <preview> 
                    <keyedPreview>
                      <title><![CDATA[{{ $video['title'] }}]]></title> 
                      <summary><![CDATA[{{ $video['description'] }}]]></summary>
                      <image src = "{{ isset($video['stillImageURL']) ? $video['stillImageURL'] : '' }}" />
                      <metadataKeys>
                        <label>Rating</label> 
                        <label>Runtime</label> 
                        @if (isset($video['longForm']) && $video['longForm'] === 'true')
                          <label>Aired on</label>
                          @if (isset($video['expirationDate']))
                            <label>Available Until</label>
                          @endif
                        @endif
                      </metadataKeys>
                      <metadataValues> 
                        <label>{{ isset($video['rating']) ? strtoupper($video['rating']) : 'NA' }}</label> 
                        <label>{{ isset($video['totalVideoDuration']) ? round($video['totalVideoDuration']/60000) : 'NA' }} min</label> 
                        @if (isset($video['longForm']) &&  $video['longForm'] === 'true')
                          <label>{{ isset($video['airDate']) ? date('M d, Y', strtotime($video['airDate'])) : 'NA' }}</label>
                          @if ( isset($video['expirationDate']))
                            <label>{{ date('M d, Y', strtotime($video['expirationDate'])) }}</label>
                          @endif
                        @endif
                      </metadataValues>
                    </keyedPreview>
                  </preview> 
                  </twoLineEnhancedMenuItem>
              @endforeach
            </items>
          </menuSection>
          @endif
          
        </sections> 
      </menu> 
    </listWithPreview> 
  </body> 
</atv>