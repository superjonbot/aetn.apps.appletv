<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <listByNavigation id = "com.aetn.show-navigation">
      <header>
        <tabWithTitle accessibilityLabel = "{{ htmlspecialchars($showData['detailTitle'], ENT_QUOTES, 'UTF-8') }}">
          <title><![CDATA[{{ $showData['detailTitle'] }}]]></title> 
        </tabWithTitle>
      </header>

      <menu>
        <sections>
          <menuSection>
            <items>
              @foreach ($seasons as $season)
              <oneLineMenuItem 
                id = 'menu_item_season{{ $season }}'
                onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($showName) }}/season/{{ $season }}/OtherSeasons')"
                >
                <label>{{ $season === '0' ? 'All Seasons' : 'Season ' . $season }}</label> 
                <accessories> 
                  <arrow /> 
                </accessories> 
                <preview> 
                  <keyedPreview>
                    <title><![CDATA[{{ $showData['detailTitle'] }}]]></title> 
                    <summary><![CDATA[{{ $showData['shortDetailDescription'] }}]]></summary>
                    <image src = "{{ $showData['gridImageURL2x'] }}" /> 
                    @if (isset($showData['tuneInInfo']))
                    <metadataKeys>
                      <label>Tune In</label>
                    </metadataKeys>
                    <metadataValues>
                      <label>{{ $showData['tuneInInfo'] }}</label>
                    </metadataValues>
                    @endif
                  </keyedPreview>
                </preview> 
              </oneLineMenuItem>
              @endforeach

              @if (isset($showData['itunesURL']) && $showData['itunesURL'] !== '')
              <oneLineMenuItem 
                id = "menu_item_season{{ $season }}" 
                onSelect = "aetnUtils.loadURLWithMVPD('{{ htmlspecialchars($showData['itunesURL'], ENT_QUOTES, 'UTF-8') }}')"
                >
                <label>Buy on iTunes</label> 
                <accessories> 
                  <arrow /> 
                </accessories> 
                <preview> 
                  <keyedPreview>
                    <title><![CDATA[{{ $showData['detailTitle'] }}]]></title> 
                    <summary><![CDATA[{{ $showData['shortDetailDescription'] }}]]></summary>
                    <image src = "{{ $showData['gridImageURL2x'] }}" /> 
                    @if (isset($showData['tuneInInfo']))
                    <metadataKeys>
                      <label>Tune In</label>
                    </metadataKeys>
                    <metadataValues>
                      <label>{{ $showData['tuneInInfo'] }}</label>
                    </metadataValues>
                    @endif
                  </keyedPreview>
                </preview> 
              </oneLineMenuItem>
              @endif
            </items>
          </menuSection>
        </sections>
      </menu>
    </listByNavigation>
  </body>
</atv>