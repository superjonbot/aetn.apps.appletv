<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <scroller
      id = "{{ $heading === 'Shows' ? 'com.aetn.shows' : 'com.aetn.h2shows' }}"
      volatile = "true" 
      onVolatileReload = "aetnUtils.handleVolatileReload('{{ $heading === 'Shows' ? 'com.aetn.shows' : 'com.aetn.h2shows' }}', document)"
      >
      
      <header>
        <simpleHeader accessibilityLabel = "{{ $heading }} Showcase" horizontalAlignment = "right">
          <title><![CDATA[{{ ($heading !== 'Shows') ? $heading : 'Featured' }}]]></title>
          <image id = "brand_logo" required = "true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></image>
        </simpleHeader>
      </header>

      <items>
        @if (isset($showcaseItems))
        <showcase accessibilityLabel = "Featured Items" id = "featured">
          <initialSelection>
            <indexPath>
              <index>0</index>
            </indexPath>
          </initialSelection>
          <items>
          @foreach ($showcaseItems as $index => $showcaseItem)
            
            @if ($showcaseItem['feature']['linkType'] === 's')
              <showcasePoster 
                id = "featured_item_{{ $index }}"
                accessibilityLabel = "{{ htmlspecialchars($showcaseItem['feature']['title'], ENT_QUOTES, 'UTF-8') }}" 
                onSelect = "aetnUtils.loadFeaturedURL({{ $index + 1}}, aetn.globalContext.urlPath + '/show/{{ rawurlencode($showcaseItem['feature']['linkRef']) }}/default')"
                onPlay = "aetnUtils.loadFeaturedURL({{ $index + 1}}, aetn.globalContext.urlPath + '/show/{{ rawurlencode($showcaseItem['feature']['linkRef']) }}/default')"
                >

            @elseif($showcaseItem['feature']['linkType'] === 'e')    
              <showcasePoster 
                id = "featured_item_{{ $index }}"
                accessibilityLabel = "{{ htmlspecialchars($showcaseItem['feature']['title'], ENT_QUOTES, 'UTF-8') }}" 
                onSelect = "aetnUtils.loadFeaturedURL({{$index + 1}}, aetn.globalContext.urlPath + '/video/{{ $showcaseItem['feature']['linkRef'] }}')"
                onPlay = "aetnUtils.loadFeaturedVideo('Featured:{{ $index + 1}}', '{{ $showcaseItem['feature']['linkRef'] }}', 'false')"
                >
            @endif
            
            <image src720 = "{{ $showcaseItem['feature']['appleTvSDImageUrl'] }}" src1080 = "{{ $showcaseItem['feature']['appleTvHDImageUrl'] }}" />
            <defaultImage>resource://Poster.png</defaultImage>
            </showcasePoster>
          @endforeach
          </items>
        </showcase>
        @endif
        
        @if (isset($justAddedVideos))
        <collectionDivider 
          alignment = "left" 
          accessibilityLabel = "Latest Episodes"
          >
          <title><![CDATA[Latest Episodes]]></title>
        </collectionDivider>

        <shelf id = "shelf_justadded" columnCount = "5">
          <sections>
            <shelfSection>
              <items>
                @foreach ($justAddedVideos as $index => $video)
                  @if ($mvpd !== 'isInRetailDemoMode' || ($mvpd === 'isInRetailDemoMode' && $video['isBehindWall'] === 'false'))
                  <sixteenByNinePoster 
                      id = "justAdded_{{ $index }}"
                      alwaysShowTitles = "true"
                      accessibilityLabel = "{{ htmlspecialchars($video['title'], ENT_QUOTES, 'UTF-8') }}" 
                      onSelect = "aetnUtils.loadFeaturedURL('Recent:{{$index + 1}}', aetn.globalContext.urlPath + '/video/{{ $video['thePlatformId']}}')"
                      onPlay   = "aetnUtils.loadFeaturedVideo('Recent:{{ $index + 1}}', '{{ $video['thePlatformId'] }}', 'false')"
                    >
                    <title><![CDATA[@if(isset($video['seriesName'])){{ $video['seriesName']}}@endif]]></title>
                    <subtitle><![CDATA[@if(isset($video['season']))S{{$video['season']}}@endif @if(isset($video['episode']))E{{$video['episode']}}@endif {{ $video['title'] }}]]></subtitle>
                    <image src720 = "{{ $video['thumbnailImageURL'] }}" src1080 = "{{ $video['thumbnailImageURL'] }}"/>
                    <defaultImage>resource://16x9.png</defaultImage>
                  </sixteenByNinePoster>
                  @endif
                @endforeach
              </items>
            </shelfSection>
          </sections>
        </shelf>
        @endif

        <collectionDivider alignment = "left" accessibilityLabel = "Shows">
          <title>{{ $heading }}</title>
        </collectionDivider>
        
        <grid id = "grid_1" columnCount = "4">
          <items>

            @foreach ($items as $index => $item)
              <fourByThreePoster 
                id = "grid_item_{{ $index }}"
                accessibilityLabel = '{{ str_replace("'", "", $item['showID']) }}'
                showReflection = "true" 
                onSelect = "
                    var mvpd = atv.localStorage.getItem('mvpd');
                    var seriesname = '{{str_replace("'", "", $item['showID']) }}'.replace(/ /g, '');
                    var lastVideoForShow = atv.localStorage.getItem(seriesname);
                    if (lastVideoForShow) {
                      var info =  lastVideoForShow.season + '-' + 
                                  lastVideoForShow.thePlatformId + '-' +
                                  ((lastVideoForShow.finished) ? 'new' : 'current');
                      aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($item['showID']) }}/' + info);
                    } else {
                      aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/show/{{ rawurlencode($item['showID']) }}/default');
                    }"
                onPlay = ""
                >
                <title><![CDATA[{{ $item['detailTitle'] }}]]></title>
                <image src720 = "{{ $item['gridImageURL2x'] }}" src1080 = "{{ $item['gridImageURL2x'] }}"/>
                <defaultImage>resource://16x9.png</defaultImage>
              </fourByThreePoster>
            @endforeach

          </items>
        </grid>
      </items>
    </scroller>
  </body>
</atv>