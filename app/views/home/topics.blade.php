<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <scroller
      id = "com.aetn.topics"
      volatile = "true" 
      onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.topics', document)"
      >

      <header>
        <simpleHeader accessibilityLabel = "Topics" horizontalAlignment = "right">
          <title><![CDATA[{{$heading}}]]></title>
          <image id = "brand_logo" required = "true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></image>
        </simpleHeader>
      </header>

      <items>
        @foreach ($items as $topic_index => $topic)

          <collectionDivider alignment = "left" accessibilityLabel = "{{ htmlspecialchars($topic['title'], ENT_QUOTES, 'UTF-8') }}">
            <title>{{ htmlspecialchars($topic['title']) }}</title>
          </collectionDivider>

          <shelf id = "shelf_{{ $topic_index }}" columnCount = "5">
            <sections>
              <shelfSection>
                <items>
                  @foreach ($topic['videos'] as $video_index => $video)
                    <sixteenByNinePoster 
                        id = "topic_item_{{ $topic_index }}_{{ $video_index }}"
                        alwaysShowTitles = "true"
                        accessibilityLabel = "{{ htmlspecialchars($video['title'], ENT_QUOTES, 'UTF-8') }}" 
                        onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/video/{{ $video['title_ID']}}')"
                        onPlay   = "aetnUtils.loadVideo('{{ $video['title_ID'] }}', 'false')"
                      >
                      <title><![CDATA[{{ $video['title'] }}]]></title>
                      <image src720 = "{{ $video['thumbnailImageURL'] }}" src1080 = "{{ $video['thumbnailImageURL'] }}"/>
                      <defaultImage>resource://16x9.png</defaultImage>
                    </sixteenByNinePoster>
                  @endforeach
                </items>
              </shelfSection>
            </sections>
          </shelf>

      	@endforeach
      </items>
    </scroller>
  </body>
</atv>