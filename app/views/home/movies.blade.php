<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
	</head>
  <body>
    <scroller 
      id = "com.aetn.movies"
      volatile = "true" 
      onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.movies', document)"
      >

      <header>
        <simpleHeader accessibilityLabel = "Movies Showcase" horizontalAlignment = "right">
          <title><![CDATA[{{ $heading }}]]></title>
          <image id = "brand_logo" required = "true" src720 = "{{ $home.$mvpdLogo }}@720.png" src1080 = "{{ $home.$mvpdLogo }}@1080.png"></image>
        </simpleHeader>
      </header>

      <items>

        <collectionDivider alignment = "left" accessibilityLabel = "Movies">
          <title>{{ $heading }}</title>        </collectionDivider>
        
        <grid id = "grid_1" columnCount = "5">
          <items>
          @foreach ($items as $index => $video)
  		      <sixteenByNinePoster
              id = "movie_item_{{ $index }}"
              accessibilityLabel = "{{ htmlspecialchars($video['title'], ENT_QUOTES, 'UTF-8') }}"
              alwaysShowTitles = "true"
              showReflection = "true" 
              onSelect = "aetnUtils.loadURLWithMVPD(aetn.globalContext.urlPath + '/video/{{ $video['thePlatformId'] }}')"
              onPlay   = "aetnUtils.loadVideo('{{ $video['thePlatformId'] }}', '{{ $video['isBehindWall'] }}')"
              >
              <title><![CDATA[{{ $video['title'] }}]]></title>
              <image src720 = "{{ $video['stillImageURL'] }}" src1080 = "{{ $video['stillImageURL'] }}"/>
              <defaultImage>resource://16x9.png</defaultImage>
            </sixteenByNinePoster>
          @endforeach
          </items>
        </grid>

      </items>

    </scroller>
  </body>
</atv>