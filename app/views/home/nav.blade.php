<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <viewWithNavigationBar
      id = "com.aetn.navigation-bar" 
      volatile = "true"
      onNavigate = "
        var e = event;
        require(['aetn/appletv/ui/navbar'], function(Navbar){
          var navbar = new Navbar();
          navbar.handleNavigate(e, 'com.aetn.navigation-bar');
        });" 
      onVolatileReload = "
        var e = event;
        require(['aetn/appletv/ui/navbar'], function(Navbar){
          var navbar = new Navbar();
          navbar.handleVolatileReload(e, document, 'com.aetn.navigation-bar');
        });">
      <navigation id = "nav" currentIndex = "0">
        @foreach ($navItems as $index => $navItem) 
        <navigationItem id = "{{ strtolower($navItem['title']) }}" 
        @if( !empty($navItem['accessibilityTitle'])) accessibilityLabel = "{{ $navItem['accessibilityTitle'] }}" @endif>
          <title>{{ $navItem['title'] }}</title>
          <url>{{ $home }}/{{ $site }}{{ $navItem['url'] }}</url>
        </navigationItem>
        @endforeach
      </navigation>
    </viewWithNavigationBar>
  </body>
</atv>