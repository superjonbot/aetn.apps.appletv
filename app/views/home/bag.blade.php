<?xml version = "1.0" encoding = "UTF-8"?>
<plist version = "1.0">
  <dict>
    <key>javascript-url</key>
    <string>{{ $home . $js }}</string>
    <key>auth-type</key>
    <string>js</string>
    <key>enabled</key>
    <string>YES</string>
    <key>menu-title</key>
    <string>{{ $menu_title }}</string>
    <key>merchant</key>
    <string>{{ $app['merchant'] }}</string>
    <key>menu-icon-url</key>
    <dict>
      <key>720</key>
      <string>{{ $home . $logos['720'] }}</string>
      <key>1080</key>
      <string>{{ $home . $logos['1080'] }}</string>
    </dict>
    <key>menu-icon-url-version</key>
    <string>1.5</string>
    <key>screensaver</key>
    <array>
      <dict>
        <key>preview-image</key>
        <dict>
          <key>720</key>
          <dict>
            <key>image</key>
            <string>{{ $home . $logos['720'] }}</string>
          </dict>
          <key>1080</key>
          <dict>
            <key>image</key>
            <string>{{ $home . $logos['1080'] }}</string>
          </dict>
        </dict>
        <key>canonical-name</key>
        <string>{{ $menu_title }}</string>
      </dict>
    </array>
  </dict>
</plist>