<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <body>
    <dialog id = "com.aetn.settings.about">
      <title><![CDATA[About {{ $brand }} ]]></title>
      <description><![CDATA[ {{ $text }} ]]></description>
    </dialog>
  </body>
</atv>