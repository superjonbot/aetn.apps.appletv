<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <head>
    <script src = "{{ $home . $js }}"/>
  </head>
  <body>
    <listWithPreview 
      id = "com.aetn.settings"
      volatile = "true"
      onVolatileReload = "aetnUtils.handleVolatileReload('com.aetn.settings', document)"
      >
      <header>
        <simpleHeader accessibilityLabel = "Settings">
          <title><![CDATA[Settings]]></title>
        </simpleHeader>
      </header>      
      <menu>
        <initialSelection>
          <row>0</row>
          <relevanceDate>1970-01-01T00:00:00Z</relevanceDate>
        </initialSelection>
        <sections>
          <menuSection>
            <items>
              @if ($mvpd !== 'isInRetailDemoMode')
                @if ($mvpd !== false)
                <oneLineMenuItem id = "activation" accessibilityLabel = "Sign out" onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/tve/deactivate')" onPlay = "">
                  <label>Sign out</label>
                </oneLineMenuItem>
                @else
                <oneLineMenuItem id = "activation" accessibilityLabel = "Sign in to Provider" onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/tve/activate')" onPlay = "">
                  <label>Sign in to Provider</label>
                </oneLineMenuItem>
                @endif
              @endif
              <oneLineMenuItem id = "faq" accessibilityLabel = "FAQ"  onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/settings/faq')" onPlay = "">
                <label>FAQ</label>
              </oneLineMenuItem>
              <oneLineMenuItem id = "privacy" accessibilityLabel = "Privacy Policy"  onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/settings/privacy-policy')" onPlay = "">
                <label>Privacy Policy</label>
              </oneLineMenuItem>
              <oneLineMenuItem id = "terms" accessibilityLabel = "Terms &amp; Conditions"  onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/settings/terms-conditions')" onPlay = "">
                <label>Terms &amp; Conditions</label>
              </oneLineMenuItem>
              <oneLineMenuItem id = "contact" accessibilityLabel = "Contact Us" onSelect = "atv.loadURL(aetn.globalContext.urlPath + '/settings/contact-us')" onPlay = "">
                <label>Contact Us</label>
              </oneLineMenuItem>
            </items>
          </menuSection>
        </sections>
      </menu>
    </listWithPreview>
  </body>
</atv>