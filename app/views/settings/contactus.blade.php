<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <body>
    <dialog id = "com.aetn.settings.contactus">
      <title>Contact Us</title>
      <description>
         <![CDATA[If you have any questions or comments about the app, please email us at watchapps@aenetworks.com. We'd love to hear your feedback.]]>
      </description>
    </dialog>
  </body>
</atv>