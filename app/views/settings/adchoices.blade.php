<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <body>
    <scrollingText id = "com.aetn.settings.adchoices" initialSelection="1">
      <title>Ad Choices</title>
      <text>
         <![CDATA[
Online Behavioral Advertising
A+E works with online advertising companies to provide you with relevant and useful advertisements on our websites.  These advertisements are tailored for you based on information collected across multiple websites that you visit in order to predict your preferences and to show you ads that are most likely to be of interest to you.  This type of ad customization is called “online behavioral advertising,” or “interest-based” advertising, and is enabled on our Websites through Web Beacons or Cookies.  We use online advertising as a way to help support the free content, products and services that you use online.

For more information about online behavioral advertising and browser controls to enhance your privacy, you can visit www.aboutads.info. 


How do I Opt-Out of Online Behavioral Advertising?
A+E understands that you may choose to opt-out of online behavioral advertising served on our websites or on other sites that you visit.  You can visit the Consumer Choice Page at www.aboutads.info/choices to see your opt out choices from receiving interest-based advertising from some or all participating companies. 

You can also opt-out of ads served by Vindico by visiting their site at http://vindicogroup.com/vindico-cookie-opt-out and FreeWheel by visiting their website at  http://www.freewheel.tv/privacy. Please be aware that even if you opt out, you will still see ads online, and in some cases data may be collected about your browsing activity, but companies will not use this information to select the ads you see online.   


Self-Regulatory Program for Online Behavioral Advertising
The Self-Regulatory Program for Online Behavioral Advertising is an effort by many of the nation's largest media and marketing trade associations to give consumers more information and choices about the advertising they receive online. The Self-Regulatory Program for Online Behavioral Advertising has established seven principles for online advertising, to which A+E adheres.   For more information about these principles, please visit http://www.aboutads.info/principles. 

To learn more about our practices, the information that we collect and our security procedures to safeguard your information, please read our Privacy Policy.

         ]]>
      </text>
    </scrollingText>
  </body>
</atv>