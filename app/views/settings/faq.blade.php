<?xml version = "1.0" encoding = "UTF-8"?>
<atv>
  <body>
    <scrollingText id = "com.aetn.settings.faq" initialSelection="1">
      <title>FAQ</title>
      <text> <![CDATA[
Q: What is the {{ $brand }} app?
A: The {{ $brand }} app allows you to watch your favorite {{ $brand }} programming - full episodes, clips, movies and more - on your Apple TV.

Q: How much does it cost to use the {{ $brand }} app?
A: The {{ $brand }} app is totally free to use. However, to access the entire catalog of programming, you will need to verify your cable TV or satellite TV subscription by logging in through your provider.

Q: How often is new video added to the {{ $brand }} app?
A: There will be new episodes, clips and movies added to the {{ $brand }} app nearly every day!

Q: Why isn't my favorite show available in the app?
A: We would love to put all of our programming into the {{ $brand }} app, but sometimes (rarely) our contracts don't allow for that. You can send your requests to watchapps@aenetworks.com.

Q: How quickly does a new episode get added after it airs on TV?
A: Generally speaking, new episodes will be available the morning after they air on {{ $brand }}.

Q: Do I have to log in to use the {{ $brand }} app?
A: No, you don't have to log in to use the {{ $brand }} app. There will always be lots to watch for viewers who don't log in.

Q: What's the difference between watching while logged in and watching while not logged in?
A: Viewers who verify their subscription to a cable or satellite TV provider get access to a deeper catalog of video content. If you cannot log in because your provider is not supported or you don't know your password, you'll still have lots to watch.

Q: Where do I get my log-in information?
A: Your log-in information comes from your local cable company.

Q: What cable providers support the {{ $brand }} app?
A: Subscribers to DirecTV, Verizon (FiOS), and Cablevision (Optimum) can log in to the app. We will be adding more providers soon.

Q: What countries are able to use the {{ $brand }} app.
A: The {{ $brand }} app is currently available for use only in the United States.

Q: Does the {{ $brand }} app support closed captioning?
A: Programming that originally aired after October 31, 2012 on television has closed captioning available. Tap the CC button on the player to turn captions on. You can enable closed captioning from Settings on your Apple TV.

Q: How can I contact {{ $brand }} if I need support for the {{ $brand }} app?
A: You can get in touch with {{ $brand }} at watchapps@aenetworks.com]]>
      </text>
    </scrollingText>
  </body>
</atv>