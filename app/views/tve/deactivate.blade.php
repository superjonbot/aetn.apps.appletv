<atv> 
  <head> 
      <script src = "{{ $home . $js }}"/> 
  </head> 
  <body> 
    <optionDialog id = "com.aetn.deactivate"> 
      <header> 
        <simpleHeader accessibilityLabel = "Sign Out: Signing out this device will unlink it from your Cable account. You can sign in later so you can continue to enjoy access to all {{ htmlspecialchars($brand, ENT_QUOTES, 'UTF-8') }} content.]]"> 
          <title>
            <![CDATA[Sign Out]]>
          </title> 
        </simpleHeader> 
      </header> 
      <description>
        <![CDATA[By signing out, you will unlink your device from your TV Provider account. You can sign in again later and continue to enjoy full access to all {{$brand}} content.]]>
      </description> 
      <menu> 
        <initialSelection> 
          <row>1</row> 
        </initialSelection> 
        <sections> 
          <menuSection> 
            <items> 
              <oneLineMenuItem
                id = "signout" 
                accessibilityLabel = "Sign Out" 
                onSelect = "
                  require(['aetn/appletv/tve/activate'], function(Activate){
                    var activate = new Activate('com.aetn.deactivate');
                  });"
                >
                <label>Sign Out</label> 
              </oneLineMenuItem> 
              <oneLineMenuItem 
                id = "cancel" 
                accessibilityLabel = "Cancel" 
                onSelect = "atv.unloadPage()" 
                onPlay = "atv.unloadPage()"
                >
                <label>Cancel</label> 
              </oneLineMenuItem> 
            </items> 
          </menuSection> 
        </sections> 
      </menu> 
    </optionDialog> 
  </body> 
</atv>