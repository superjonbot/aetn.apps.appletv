<atv> 
  <head> 
      <script src = "{{ $home . $js }}"/> 
  </head> 
  <body> 
    <optionDialog id = "com.aetn.fail"> 
      <header> 
        <simpleHeader accessibilityLabel = "Confirmation of the activation code has timed out. Try again"> 
          <title>
            <![CDATA[Failure!]]>
          </title> 
        </simpleHeader> 
      </header> 
      <description>
        <![CDATA[Confirmation of the activation code has timed out. Try again]]>
      </description> 
      <menu> 
        <initialSelection> 
          <row>0</row> 
        </initialSelection> 
        <sections> 
          <menuSection> 
            <items> 
              <oneLineMenuItem
                id = "ok" 
                accessibilityLabel = "ok" 
                onSelect = "atv.unloadPage()" 
                onPlay = "atv.unloadPage()"
                >
                <label>OK</label> 
              </oneLineMenuItem> 
            </items> 
          </menuSection> 
        </sections> 
      </menu> 
    </optionDialog> 
  </body> 
</atv>