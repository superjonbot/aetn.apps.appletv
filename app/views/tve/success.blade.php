<atv> 
  <head> 
      <script src = "{{ $home . $js }}"/> 
  </head> 
  <body> 
    <optionDialog id = "com.aetn.success"> 
      <header> 
        <simpleHeader accessibilityLabel = "Your TV provider account has been successfully linked to the {{ htmlspecialchars($brand, ENT_QUOTES, 'UTF-8') }} Apple TV App. Happy viewing!"> 
          <title>
            <![CDATA[Success!]]>
          </title> 
        </simpleHeader> 
      </header> 
      <description>
        <![CDATA[Your TV provider account has been successfully linked to the {{$brand}} Apple TV app. Happy viewing!]]>
      </description> 
      <menu> 
        <initialSelection> 
          <row>0</row> 
        </initialSelection> 
        <sections> 
          <menuSection> 
            <items> 
              <oneLineMenuItem
                id = "ok" 
                accessibilityLabel = "ok" 
                onSelect = "atv.unloadPage()" 
                onPlay = "atv.unloadPage()"
                >
                <label>OK</label> 
              </oneLineMenuItem> 
            </items> 
          </menuSection> 
        </sections> 
      </menu> 
    </optionDialog> 
  </body> 
</atv>