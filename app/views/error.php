<atv> 
  <head> 
  </head> 
  <body> 
    <optionDialog id = "com.aetn.success"> 
      <header> 
        <simpleHeader accessibilityLabel = "Sorry, the video is no longer available."> 
          <title>
            <![CDATA[ ]]>
          </title> 
        </simpleHeader> 
      </header> 
      <description>
        Sorry, the video is no longer available.
      </description> 
      <menu> 
        <initialSelection> 
          <row>0</row> 
        </initialSelection> 
        <sections> 
          <menuSection> 
            <items> 
              <oneLineMenuItem
                id = "ok" 
                accessibilityLabel = "ok" 
                onSelect = "atv.unloadPage()" 
                onPlay = "atv.unloadPage()"
                >
                <label>OK</label> 
              </oneLineMenuItem> 
            </items> 
          </menuSection> 
        </sections> 
      </menu> 
    </optionDialog> 
  </body> 
</atv>