<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$sites = 'history|fyi|mlt|ae';

Route::get('/{site}/bag.plist', 'HomeController@bag')->where('site', $sites);

// HomeController
Route::get('/{site}/nav/nowatchlist/{mvpd?}', 'HomeController@navNoWatchlist')->where('site', $sites);
Route::get('/{site}/nav/{mvpd?}', 'HomeController@nav')->where('site', $sites);
Route::get('/{site}/section/{page}/{mvpd?}', 'HomeController@section')->where('site', $sites);

// WatchListController
Route::get('/{site}/watchlist/empty/{mvpd?}', 'WatchListController@emptyList')->where('site', $sites);
Route::get('/{site}/watchlist/queueempty/{mvpd?}', 'WatchListController@emptyQueueList')->where('site', $sites);
Route::get('/{site}/watchlist/progressempty/{mvpd?}', 'WatchListController@emptyProgressList')->where('site', $sites);
Route::get('/{site}/watchlist/{mvpd?}', 'WatchListController@index')->where('site', $sites);

// SearchController
Route::get('/{site}/search/{mvpd?}', 'SearchController@index')->where('site', $sites);
Route::get('/{site}/search-results', 'SearchController@searchResults')->where('site', $sites);
Route::get('/{site}/search-results-free', 'SearchController@searchResultsFreeOnly')->where('site', $sites);


// SettingsController
Route::get('/{site}/settings/index/{mvpd?}', 'SettingsController@index')->where('site', $sites);
Route::get('/{site}/settings/about/{mvpd?}', 'SettingsController@about')->where('site', $sites);
Route::get('/{site}/settings/faq/{mvpd?}', 'SettingsController@faq')->where('site', $sites);
Route::get('/{site}/settings/privacy-policy/{mvpd?}', 'SettingsController@privacyPolicy')->where('site', $sites);
Route::get('/{site}/settings/terms-conditions/{mvpd?}', 'SettingsController@termsConditions')->where('site', $sites);
Route::get('/{site}/settings/ad-choices/{mvpd?}', 'SettingsController@adChoices')->where('site', $sites);
Route::get('/{site}/settings/contact-us/{mvpd?}', 'SettingsController@contactUs')->where('site', $sites);

// TVEController
Route::get('/{site}/tve/activate', 'TVEController@activate')->where('site', $sites);
Route::get('/{site}/tve/deactivate', 'TVEController@deactivate')->where('site', $sites);
Route::get('/{site}/tve/success/{mvpd?}', 'TVEController@success')->where('site', $sites);
Route::get('/{site}/tve/fail', 'TVEController@fail')->where('site', $sites);


// Season Landing
Route::get('/{site}/show/{show_name}/season/{season_number}/{other_type}/{mvpd?}', 'ShowController@seasonVideos')->where('site', $sites);
Route::get('/{site}/show/{show_name}/all/{mvpd?}', 'ShowController@allSeasons')->where('site', $sites);


// VideoController
Route::get('/{site}/show/{show_name}/video/{mpx_id}/{mvpd?}', 'VideoController@showVideo')->where('site', $sites);

//Show Landing
Route::get('/{site}/show/{show_name}/{video_info?}/{mvpd?}', 'ShowController@defaultSeason')->where('site', $sites);

Route::get('/{site}/video/json/{mpx_id}', 'VideoController@videoJSON')->where('site', $sites);
Route::get('/{site}/video/{mpx_id}/{mvpd?}', 'VideoController@index')->where('site', $sites);

Route::any('{all}', function($uri) {
    App::abort(404);
})->where('all', '.*');


