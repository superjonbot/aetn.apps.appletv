<?php

class Mpx extends BaseService
{

    public function __construct( $site = 'history' ) 
    {
        $this->site_local = $site;
    }

    public function get( $args = array() )
    {   

        $defaults = array(
          'videoId'     => -1,
          'seriesName' => '',
          'season' => '',
          'videoType' => 'Episode',
          'adapterParams'  => '',
          'isBehindWall' => null,
          'range' => '1-100000'
        );

        $args = array_merge( $defaults, $args );

        $feedUrl = Config::get( 'aetn.mpx.' . $this->site_local . '.titleFeed' );

        if (is_null( $feedUrl )) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }

        if ( $args[ 'videoId' ] > 0 ) {
            $feedUrl .= '?byID=' . $args[ 'videoId' ];
            return $this->getData( $feedUrl );
        }

        $feedUrl .= '?range=' . $args[ 'range' ] . '&byCustomValue=';

        if ( ( !is_null( $args[ 'isBehindWall' ] ) ) && ( !strrpos(strtolower($args[ 'adapterParams' ]), 'ca') ) ) {
            $feedUrl .= '{isBehindWall}{' . ( $args[ 'isBehindWall' ] ? 'true' : 'false' ) . '},';
        }

        if ( strlen($args[ 'seriesName' ] ) > 0 ) {
            if ($this->site_local === 'mlt')
                $feedUrl .= '{seriesNameGlobal}{' . $args[ 'seriesName' ] . '},';
            else
                $feedUrl .= '{seriesName}{' . $args[ 'seriesName' ] . '},';
        }

        if (strlen($args[ 'season' ] ) > 0 ) {
            $feedUrl .= '{season}{'. $args[ 'season' ]. '},';
        }

        if ( strlen($args[ 'videoType' ] ) > 0 ) {
            $feedUrl .= '{mrssLengthType}{' . $args[ 'videoType' ] . '}';
        }

        if ( strlen($args[ 'adapterParams' ] ) > 0 ) {
            $feedUrl .= '&adapterParams=' . $args[ 'adapterParams' ];
        }

        error_log('mpx feedurl '.$feedUrl);
        return $this->getData( $feedUrl );
    }

    public function getMovies( $args = array() )
    {   

        $defaults = array(
            'videoId'     => -1,
            'videoType' => 'Movie',
            'isBehindWall' => null,
            'range' => '1-100000'
        );

        $args = array_merge( $defaults, $args );

        $feedUrl = Config::get('aetn.mpx.'.$this->site_local.'.titleFeed');
        
        if (is_null($feedUrl)) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }

        if ( $args[ 'videoId' ] > 0 ) {
            $feedUrl .= '/' . $args[ 'videoId' ];
            return $this->getData( $feedUrl );
        }

        $feedUrl .= '?range=' . $args[ 'range' ] . '&byCustomValue=';

        if (!is_null($args[ 'isBehindWall' ])) {
            $feedUrl .= '{isBehindWall}{' . $args[ 'isBehindWall' ] . '},';
        }

        if ( strlen($args[ 'videoType' ] ) > 0 ) {
            $feedUrl .= '{mrssLengthType}{' . $args[ 'videoType' ] . '}';
        }

        return $this->getData( $feedUrl );
    }

    public function getJustAdded( $args = array() )
    {   

        $defaults = array(
          'videoId'     => -1,
          'adapterParams'  => '',
          'isBehindWall' => null,
          'range' => '1-100000'
        );

        $args = array_merge( $defaults, $args );

        $feedUrl = Config::get( 'aetn.mpx.' . $this->site_local . '.justAddedFeed' );
        
        if (is_null($feedUrl)) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }

        if ( $args[ 'videoId' ] > 0 ) {
            $feedUrl .= '/' . $args[ 'videoId' ];
            return $this->getData( $feedUrl );
        }

        $feedUrl .= '?range=' . $args[ 'range' ] . '&byCustomValue=';

        if ( ( !is_null( $args[ 'isBehindWall' ] ) ) && ( !strrpos(strtolower($args[ 'adapterParams' ]), 'ca') ) ) {
            $feedUrl .= '{isBehindWall}{' . ( $args[ 'isBehindWall' ] ? 'true' : 'false' ) . '},';
        }

        if ( strlen($args[ 'adapterParams' ] ) > 0 ) {
            $feedUrl .= '&adapterParams=' . $args[ 'adapterParams' ];
        }


        return $this->getData( $feedUrl );
    }
}