<?php

class JustAdded
{

    public function __construct() 
    {
        
    }

    public function get( $site = 'history', $deviceId = 'appletv' )
    {
        $tpService = new TpService( $site );

        $res = $tpService->getJustAdded( array( 'deviceId' => $deviceId ) );
        $obj = json_decode( $res, true );

        return $obj[ 'Items' ];
    }
}