<?php

class MvpdLogo {
    public function getMvpdLogo($mvpd, $site) {
    	
    	//first tier providers have their own logo
        if (in_array($mvpd, Config::get('aetn.firstTierTVEProviders'))){
            $mvpdLogo = '/images/' . $site . '/' . $mvpd;
        } else {
        	$mvpdLogo = '/images/' . $site . '/logo';
        }

        return $mvpdLogo;
    }
}