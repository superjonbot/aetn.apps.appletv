<?php

class BaseService
{

	public function __construct( ) 
	{
	}

	protected function getData( $url = '' )  
	{
	  	
	  	$response =  Http::get( $url );
	  	
		if ( $response[ 'status' ] === 200 ) {
			return $response[ 'body' ];
		} else {
			array(
      		'error'	=> 'Error while data retrieval from MPX.',
      		'errorcode' => $response[ 'status' ],
      		'url'	=> $url
        	);
		}
	}

}