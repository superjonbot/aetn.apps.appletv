<?php

class Movies extends BaseService{

	public function getMovies( $site = 'mlt', $args)
	{	
		$defaults = array(
            'deviceId' => 'appletv'
        );

        $args = array_merge($defaults, $args);
        
		$feedUrl = Config::get('aetn.common.moviesFeed');
		if (is_null($feedUrl)) {
			return array(
            	'error'	=> 'FeedURL missing.'
        	);
		}

		if (!is_null($args[ 'deviceId' ])) {
			$feedUrl .= '?deviceId=' . $args[ 'deviceId' ];
		}

		if (isset($args[ 'is_behind_wall' ])) {
			$feedUrl .= '&is_behind_wall='. $args[ 'is_behind_wall' ];
		}

		$result = json_decode($this->getData($feedUrl), true);
		return $result['Items'];

	}

}