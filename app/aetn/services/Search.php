<?php

class Search extends BaseService
{

  public function __construct( $site = 'history' ) 
  {
	  $this->site_local = $site;
  }

  public function getSearchResults( $searchTerm, $freeOnly = false )
  {
    $searchUrl = Config::get('aetn.acsEndPointURL.' . $this->site_local) 
      . "/search?return-fields=title,thumbnail_image_url,grid_image_url,is_long_form,is_behind_wall,series_title,is_hd,season,episode,platform_id,video_url_hls,video_url_pd&q=&size=10000&start=0&bq=(or%20description:%27" 
      . urlencode($searchTerm). "%27%20title:%27" . urlencode($searchTerm) . "%27)";

    error_log($searchUrl);
    $results = json_decode($this->getData($searchUrl), true);
    $searchResults = array();

    $showResults = array();
    $videoResults = array();

    foreach( $results['hits']['hit'] as $hit ) {
      if (!isset($hit['data']['is_hd']) || $this->extractData($hit['data']['is_hd']) === 'false') {
        continue;
      }

      if ($freeOnly) {
        if (isset($hit['data']['is_behind_wall']) && $this->extractData($hit['data']['is_behind_wall']) === 'true') {
          continue;
        }
      }

      $title = $this->extractData($hit['data']['title']);
      $isEpisode = $this->extractData($hit['data']['is_long_form']);

      if ( $this->isSearchResultShow($hit) ) {
        //error_log("is show = ".$title);
        if ($freeOnly) {
            $item = array(
              "title" =>  htmlentities($title),
              "linkUrl" => '/show/'.rawurlencode($title).'/default/isInRetailDemoMode',
              'thumbnail' => $this->extractData($hit['data']['grid_image_url'])
            );
          
        } else {
          $item = array(
            "title" =>  htmlentities($title),
            "linkUrl" => '/show/'.rawurlencode($title).'/default',
            'thumbnail' => $this->extractData($hit['data']['grid_image_url'])
          );
        }

        array_push($showResults, $item);

      } else {//not show, videos
      
        $item = array(
          "id" => $hit['id'],
          "title" => htmlentities($title),
          "isEpisode" => ($isEpisode === 1)? true : false,
          "season" => $this->extractData($hit['data']['season']),
          "episode" => $this->extractData($hit['data']['episode']),
          "thumbnail" => $this->extractData($hit['data']['thumbnail_image_url']),
          "thePlatformId" => $this->extractData($hit['data']['platform_id']),
          "videoPdUrl" => $this->extractData($hit['data']['video_url_pd']),
          "seriesName" => $this->extractData($hit['data']['series_title']),
          "isBehindWall" => $this->extractData($hit['data']['is_behind_wall'])
        );
        
        array_push($videoResults, $item);
      }

    }

    $searchResults['showResults'] = $showResults;
    $searchResults['videoResults'] = $videoResults;
    
    //error_log("show result size: ".count($showResults));
    return $searchResults;
  }

  private function isSearchResultShow( $hit )
  {
    //error_log("ID = ".$hit['id']);
    if (count($hit['data']['video_url_hls']) === 0) 
      return true;
    else 
      return false;
  }

  private function extractData ($data)
  {
    if (count($data) > 0) {
      return $data[0];
    }

    return '';
  }

}
