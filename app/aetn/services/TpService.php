<?php

class TpService extends BaseService
{

    public function __construct( $site = 'history' ) 
    {
        $this->site_local = $site;
    }

    public function get( $args = array() )
    {   

        $defaults = array(
          'videoId'     => -1,
          'seriesName' => '',
          'season' => '',
          'videoType' => 'episode',
          'deviceId' => 'appletv'
        );

        $args = array_merge( $defaults, $args );

        $feedUrl = Config::get( 'aetn.mpx.' . $this->site_local . '.titleFeed' );

        if (is_null( $feedUrl )) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }

        $feedUrl .= '/' . $args['videoType'] . '/' . $this->site_local;

        $queryParams = array();

        if( $args[ 'videoId' ] > 0) {
            $queryParams['title_id'] = $args['videoId'];
        }

        if ( strlen($args[ 'seriesName' ] ) > 0 ) {
            $queryParams['show_name'] = urldecode($args['seriesName']);
        }

        if (strlen($args[ 'season' ] ) > 0 ) {
            $queryParams['season'] = $args['season'];
        }

        if (strlen($args[ 'deviceId' ] ) > 0 ) {
             $queryParams['deviceId'] = $args['deviceId']; 
        }

        if (strlen($args[ 'range' ] ) > 0 ) {
            $queryParams['range'] = $args['range'];
        }

        if (isset($args[ 'is_behind_wall' ])) {
            $queryParams['is_behind_wall'] = $args['is_behind_wall'];
        }

        if(count($queryParams) > 0){
            $queryStr = http_build_query($queryParams);
            $feedUrl .= '?' . $queryStr;
        }

        error_log('mpx feedurl '.$feedUrl);
        return $this->getData( $feedUrl );
    }

    public function getJustAdded( $args = array())
    {
        $defaults = array(
            'deviceId' => 'appletv'
        );

        $args = array_merge( $defaults, $args );

        $feedUrl = Config::get( 'aetn.mpx.' . $this->site_local . '.justAddedFeed' );

        $queryParams = array();
        if (strlen($args[ 'deviceId' ] ) > 0 ) {
             $queryParams['deviceId'] = $args['deviceId']; 
        }

        if(count($queryParams) > 0){
            $queryStr = http_build_query($queryParams);
            $feedUrl .= '?' . $queryStr;
        }

        if (is_null( $feedUrl )) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }
        error_log('getJustAdded   '.$feedUrl);

        return $this->getData( $feedUrl );
    }

}