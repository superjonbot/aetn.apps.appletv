<?php

class Wombat extends BaseService{

	public function __construct( $site = 'history' ) 
	{
		$this->site_local = $site;
	}

	public function getFeatured( $args = array() )
	{	
		$defaults = array(
            'deviceId'	=> 'appletv'
        );

        $args = array_merge( $defaults, $args );

		$feedUrl = Config::get('aetn.common.featuredFeed');		

		if (is_null($feedUrl)) {
			return array(
            	'error'	=> 'FeedURL missing.'
        	);
		}

		$feedUrl .= '/' . $this->site_local;

		if (!is_null($args[ 'deviceId' ])) {
			$feedUrl .= '?deviceId=' . $args[ 'deviceId' ];
		}

		return $this->getData( $feedUrl );
		
	}

	public function getShows( $args = array() )
	{	

		$defaults = array(
            'deviceId'	=> 'appletv',
            'showName'	=> ''
        );

        $args = array_merge( $defaults, $args );

		$feedUrl = Config::get('aetn.common.showsFeed.'.$this->site_local);
		

		if (is_null($feedUrl)) {
			return array(
            	'error'	=> 'FeedURL missing.'
        	);
		}

		$feedUrl .= '/' . $this->site_local;

		if (!is_null($args[ 'deviceId' ])) {
			$feedUrl .= '?deviceId=' . urlencode($args[ 'deviceId' ]);
		}

		if ( strlen($args[ 'showName' ] ) > 0 ) {
			$feedUrl .= '&showName=' . urlencode($args[ 'showName' ]);
		}

		error_log("show name: ".$feedUrl);

		return $this->getData( $feedUrl );
		
	}

}