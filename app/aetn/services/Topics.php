<?php

class Topics extends BaseService{

    public function getTopics( $deviceId = 'appletv' )
    {   
        $args = array(
            'deviceId' => $deviceId
        );

        $feedUrl = Config::get('aetn.common.topicsFeed');

        if (is_null($feedUrl)) {
            return array(
                'error' => 'FeedURL missing.'
            );
        }
        $topicsFeed = json_decode($this->getData( $feedUrl ), true);
        return $topicsFeed['items'];
    }

}