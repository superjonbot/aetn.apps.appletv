<?php 

class Http
{
    private function __construct()
    {
    }

    public static function request( $url, $method = 'GET', $args = array() )
    {
      $defaults = array(
        'timeout'     => 100,
        'redirection' => 5,
        'httpversion' => '1.0',
        'user-agent'  => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
        'blocking'    => true,
        'headers'     => array(),
        'cookies'     => array(),
        'body'        => null,
        'username'    => '',
        'password'    => '',
        'ttl'         => '60',
        'proxy'       => false,
      );

      $args = array_merge( $defaults, $args );

      $method = strtoupper( $method );
      if( $method !== 'GET' ) $method = 'POST';

      if( $method === 'GET')
      {            
          if( Cache::has($url) )
          {
            return unserialize( Cache::get( $url ) );
          }
      }

      $xhr = curl_init();

      $headers = array();

      foreach( $args[ 'headers' ] as $key => $value ) {
          array_push( $headers, "$key: $value" );
      }
      
      curl_setopt( $xhr, CURLOPT_URL, $url ); 
      curl_setopt( $xhr, CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $xhr, CURLOPT_HEADER, true );
      curl_setopt( $xhr, CURLOPT_TIMEOUT, $args[ 'timeout' ] ); 
      curl_setopt( $xhr, CURLOPT_MAXREDIRS, $args[ 'redirection' ] ); 
      curl_setopt( $xhr, CURLOPT_RETURNTRANSFER, $args[ 'blocking' ] ) ; 
      curl_setopt( $xhr, CURLOPT_FOLLOWLOCATION, true);

      if( $args[ 'username' ] !== '' && $args[ 'password' ] !== '' )
      {
          curl_setopt( $xhr, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
          curl_setopt( $xhr, CURLOPT_USERPWD, $args[ 'username' ] . ':' . $args[ 'password' ] );
      }

      if( $method === 'POST' ) 
      {
        curl_setopt( $xhr, CURLOPT_POST, true ); 
        curl_setopt( $xhr, CURLOPT_POSTFIELDS, $args[ 'data' ] ); 
      }

      if( $args[ 'proxy' ] !== false )
      {
        curl_setopt( $xhr, CURLOPT_PROXY, $args[ 'proxy' ] );
      }

      $response   = curl_exec( $xhr ); 

      if( $response === false ) return false;

      $status      = curl_getinfo( $xhr, CURLINFO_HTTP_CODE );
      $header_size = curl_getinfo( $xhr, CURLINFO_HEADER_SIZE );
      $header      = substr( $response, 0, $header_size );
      $body        = substr( $response, $header_size );

      curl_close( $xhr );

      $reponse_array = array(
          'body'      => $body,
          'header'    => $header,
          'status'    => $status
      );


      if( $method === 'GET' && $status === 200 )
      {
        Cache::put( $url, serialize( $reponse_array ), $args[ 'ttl' ] );
      }

      return $reponse_array;
    }

    public static function get( $url, $args = array() )
    {
      return self::request( $url, 'GET', $args );
    }

    public static function post( $url, $args = array() )
    {
      return self::request( $url, 'POST', $args );
    }
}


/*
 * $response = AETN_HTTP::get( 'http://www.yahoo.com' );
 *
 * $response =  array(
            'body'      => $body,
            'header'    => $header,
            'status'    => $status
        )
*/