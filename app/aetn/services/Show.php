<?php

class Show {

    public function getShows( $site = 'history', $deviceId = 'appletv', $filter = null, $mvpd = null )
    {
        $wombat = new Wombat( $site );

        $args = array(
            'deviceId' => $deviceId
        );

        $shows = json_decode($wombat->getShows( $args), true);

        usort($shows, function($a, $b){
            $a_title = (strpos(strtolower($a['showID']), 'the ') === 0)? substr(strtolower($a['showID']), 4) : strtolower($a['showID']);
            $b_title = (strpos(strtolower($b['showID']), 'the ') === 0)? substr(strtolower($b['showID']), 4) : strtolower($b['showID']);
            return $a_title > $b_title ;
        });
        
        $filteredShows = array();
        foreach( $shows as $show) {
            if (isset($show['hasNoVideo']) && $show['hasNoVideo'] === 'true') {
                continue;
            }

            if ($deviceId === 'appletv') {

                if (isset($show['hasNoHDVideo']) && $show['hasNoHDVideo'] === 'true') {
                    continue;
                }

                if ($filter === 'History' || $filter === 'H2') {
                    if ($show['network'] === $filter) {
                        array_push($filteredShows, $show);
                    }
                } else {
                    array_push($filteredShows, $show);
                }

            } else {
                if ($filter === 'History' || $filter === 'H2') {
                    if ($show['network'] === $filter) {
                        array_push($filteredShows, $show);
                    }
                } else {
                    array_push($filteredShows, $show);
                }
            }
        }

        return $filteredShows;
    }

    public function getShow( $site = 'history', $showName = 'Vikings', $deviceId = 'appletv', $mvpd = null )
    {
        $wombat = new Wombat( $site );

        $args = array();

        if ( strlen($showName) > 0 ) {
            $args = array(
                'showName' => $showName,
                'deviceId' => $deviceId
            );
        }

        return json_decode( $wombat->getShows( $args ), true );
    }

    public function getShowSeasons( $site = 'history', $showName = 'Vikings', $deviceId = 'appletv', $mvpd = null )
    {
        $args = array();

        if ( strlen($showName) > 0 ) {
            $args = array(
                'seriesName' => $showName,
                'deviceId' => $deviceId,
                'range' => '1-500'
            );
        }

        if (isset($mvpd) && $mvpd === 'isInRetailDemoMode') {
            $args['is_behind_wall'] = 'false';
        }        

        $tps = new TpService($site);
        //get all episodes
        $args['videoType'] = 'episode';
        $allEpisodeVideosResult = json_decode( $tps->get($args), true);
        //get all clips
        $args['videoType'] = 'clip';
        $allClipVideosResult = json_decode( $tps->get($args), true);

        if (!isset($allEpisodeVideosResult['Items']))
            $allEpisodeVideos = array();
        else
            $allEpisodeVideos = $allEpisodeVideosResult['Items'];

        if (!isset($allClipVideosResult['Items']))
            $allClipVideos = array();
        else 
            $allClipVideos = $allClipVideosResult['Items'];
        
        $allVideos = array_merge($allEpisodeVideos, $allClipVideos);

        $seasons = array();


        if (count($allVideos) > 0 ) {

            if (count($allVideos) > 0) {
                foreach ($allVideos as $video) {
                    if (!isset($video['season']) )
                        array_push($seasons, '0');
                    else
                        array_push($seasons, $video['season']);
                }
            }
        }
        $seasons = array_unique($seasons);

        sort($seasons, SORT_NUMERIC);
        return $seasons;
    }

    public function getSeasonVideos( $site = 'history', $showName = 'Vikings', $season_no = '1', $deviceId = 'appletv', $mvpd = null )
    {
        error_log("season: ".$season_no);
        //get episode videos for the season
        $episodes = $this->filterVideosForSeason($site, $showName, 'episode', $deviceId, $season_no, $mvpd);
        error_log("count = ".count($episodes));

        error_log("clip:".$season_no);
        $clips = $this->filterVideosForSeason($site, $showName, 'clip', $deviceId, $season_no, $mvpd);

        return array(
            'episodes' => $episodes,
            'clips' => $clips
            );
    }

    private function filterVideosForSeason($site, $showName, $videoType, $deviceId, $season_no, $mvpd) {
        $args = array(
                'seriesName' => $showName,
                'videoType' => $videoType,
                'deviceId' => $deviceId,
                'range' => '0-500'
        );

        if (isset($mvpd) && $mvpd === 'isInRetailDemoMode') {
            $args['is_behind_wall'] = 'false';
        }

        $tps = new TpService($site);
        $results = json_decode( $tps->get($args), true);
        $allVideos = $results['Items'];
        $matchedVideos = array();
        foreach ($allVideos as $video) {
            if (!isset($video['season']) ){
                $video['season'] = '0';
            }
            if ($video['season'] === $season_no ) {
        error_log("find one push");
                array_push($matchedVideos, $video);
            }
            
        }

        usort($matchedVideos, function($a, $b) {
            if (isset($a['episode']) && isset($b['episode']))
                return (int)$a['episode'] > (int)$b['episode'] ? 1 : -1;
            return -1;
        });


        return $matchedVideos;
    }

}