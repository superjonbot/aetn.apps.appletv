<?php

class Featured
{

    public function __construct() 
    {
        
    }

    public function get( $site = 'history', $deviceId = 'appletv' )
    {
        $wombat = new Wombat( $site );

        $res = $wombat->getFeatured( array( 'deviceId' => $deviceId ) );
        $obj = json_decode( $res, true );

        return $obj[ 'features' ];
    }
}