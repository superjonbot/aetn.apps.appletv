<?php

class Video {

	public function __construct() 
	{
		
	}

	public function get( $args = array() ){
		$defaults = array(
			'site'	=>	'history',
            'videoId'     => -1,
            'seriesName' => '',
            'season' => '',
            'videoType' => 'episode',
            'adapterParams'  => '',
            'isBehindWall' => null,
            'range' => '1-100000'
        );

        $args = array_merge( $defaults, $args );
		$tps = new TpService( $args[ 'site' ] );

		if ( strcasecmp($args[ 'videoType' ] , 'episode' ) === 0 ) {
			return $tps->get( $args );
		} elseif ( strcasecmp($args[ 'videoType' ] , 'clip' ) === 0 ) {
			return $tps->get( $args );
		} elseif ( strcasecmp($args[ 'videoType' ] , 'movie' ) === 0 ) {
			return $tps->getMovies( $args );
		} elseif ( strcasecmp($args[ 'videoType' ] , 'justadded' ) === 0 ) {
			return $tps->getJustAdded( $args );
		} else {
			return array(
				'error' => 'Not a valid videoType request.'
			);
		}
	}

	public function getByPlatformId( $site, $id ) {
        $args = array(
            'site' => $site,
            'videoId' => $id
        );
        $videos = array();
        $videoResult = $this->get($args);
        $parsedVideo = json_decode($videoResult, true);

        $numOfVideo = count($parsedVideo['Items']);

        if ($numOfVideo === 0) {
            return array(
                'error' => "No Video found for ".$id
            );
        }
        
        for ($i = 0; $i < $numOfVideo; $i++) {
            $video = $parsedVideo['Items'][$i];
            if (isset($video['cuePoints'])) {
                $video['chapters'] = $this->parseCuepoints($video['cuePoints']);
            } else
                $video['chapters'] = array();
                
            if (isset($video['seriesName'])) {
                $video['seriesNameForAnalytics'] = $video['seriesName'];
            } else if ($video['tvNtvMix'] === 'Movie') {
                $video['seriesNameForAnalytics'] = $video['title'];
            } else {
                $video['seriesNameForAnalytics'] = 'Noseriesname';
            }

            $video['seriesNameForAnalytics'] = str_replace(' ', '', $video['seriesNameForAnalytics']);
            $video['seriesNameForAnalytics'] = str_replace("'", "", $video['seriesNameForAnalytics']);
            array_push($videos, $video);           
        }
        return $videos;
	}

    public function getVideoDetail($site, $id, $mvpd = null) {

        $videos = $this->getByPlatformId($site, $id);
        if (is_null($videos) || count($videos) === 0) {
          return array(
                'error' => 'No Video found for '.$id
            );
        }
        if (count($videos) === 0) {
            return null;
        } else {
            //prepare data for upnext function.
            if ($videos[0]['longForm'] === 'true') { //get next video platformid for longform video
                $nextVideoId = $this->getNextVideoId($site, $videos[0], $mvpd);
                $videos[0]['nextVideoId'] = $nextVideoId;
            }else {//get playlist for shortform video

                $videos[0]['remainingVideos'] = $this->getRemainingVideos($site, $videos[0], $mvpd);
            }
            return $videos[0];
        }

    }

    private function getNextVideoId($site, $video, $mvpd) {
        $videos = $this->getRelated($site, $video, $mvpd);

        //find video position, return next video 
        foreach ($videos as $index=>$value) {
            if ($value['thePlatformId'] === $video['thePlatformId']) {
                $videoPos = $index;
                break;
            }
        }

        if (isset($videoPos) && isset($videos[$videoPos + 1])) {
            return $videos[$videoPos + 1]['thePlatformId'];
        }
        else
            return 0;
    }

    public function getRemainingVideos($site, $video, $mvpd) {
        $videos = $this->getRelated($site, $video, $mvpd);
        $remainingVideos = array();

        $include = false;

        foreach ($videos as $index=>$value) {
            if ($include) {
                $video = array("id" => $value['thePlatformId'],
                    "playURL_HLS" => $value['playURL_HLS'],
                    "title" => $value["title"],
                    "description" => $value["description"],
                    "image" => $value["thumbnailImageURL"],
                    "isBehindWall" => $value["isBehindWall"]);
                array_push($remainingVideos, $video);
            }

            if (!$include && $value['thePlatformId'] === $video['thePlatformId']) {
                $include = true;
            }
        }

        return $remainingVideos;
    }

    private function getRelated($site, $video, $mvpd) {
        if (strpos($video['siteUrl'],'/movies/') !== false) { //movie
            //use all movies as related video
            $movieService = new Movies();
            $args = array();
            if ($mvpd === 'isInRetailDemoMode') {
                $args['is_behind_wall'] = 'false';
            }
            $videos = $movieService->getMovies($site, $args);

        } else if (!empty($video['seriesName'])) { //show video
            $videoSeason = (isset($video['season']))? $video['season'] : '0';
            $show = new Show();
            $allVideos = $show->getSeasonVideos($site, $video['seriesName'], $videoSeason, 'appletv', $mvpd);

            $videos = ($video['longForm'] === 'true')? $allVideos['episodes'] : $allVideos['clips'];
            
        } else { //topic videos
            $topicsService = new Topics();
            $topics = $topicsService->getTopics();
            $videos = array();
            //find the hub where this video belongs, and return all videos in the same hub as related videos
            for ($i = 0; $i < count($topics); $i++) {
                $hasTheVideo = false;
                $hubVideos = $topics[$i]['videos'];
                for ($j = 0; $j < count($hubVideos); $j++) {
                    //print_r($hubVideos[$j]['title_ID']); 
                    if ( $hubVideos[$j]['title_ID'] === $video['thePlatformId'] ) {
                        $hasTheVideo = true;
                        $videoPos = $j;
                        break;
                    }
                }
                if ($hasTheVideo) {
                    foreach ($hubVideos as $hubVideo) {
                        $hubVideo['thePlatformId'] = $hubVideo['title_ID'];
                        $hubVideo['stillImageURL'] = $hubVideo['thumbnailImageURL'];
                        $hubVideo['network'] = 'History';
                        $hubVideo['isBehindWall'] = 'false';
                        array_push($videos, $hubVideo);                        
                    }
                    break;
                }
            }

        }
        return $videos;
    }

    /* Get Related Videos for the video detail page.
    parameters:  site, the video object
    */
    public function getRelatedVideos($site, $video, $mvpd) {
        if (strpos($video['siteUrl'],'/movies/') !== false) { //movie
         //use all movies as related video
            $movieService = new Movies();
            $args = array();
            if ($mvpd === 'isInRetailDemoMode') {
                $args['is_behind_wall'] = 'false';
            }
            $videos = $movieService->getMovies($site, $args);
            
            //find out current video index position in the videos array
            foreach ($videos as $index=>$value) {

                if ($value['thePlatformId'] === $video['thePlatformId']) {
                    //unset($videos[$index]);
                    $videoPos = $index;
                    break;
                }
            }

        } else if (!empty($video['seriesName'])) { //show video
          
            $videoSeason = (isset($video['season']))? $video['season'] : '0';
            $show = new Show();
            $allVideos = $show->getSeasonVideos($site, $video['seriesName'], $videoSeason, 'appletv', $mvpd);

            $videos = ($video['longForm'] === 'true')? $allVideos['episodes'] : $allVideos['clips'];
            
            foreach ($videos as $index=>$value) {
                if ($value['thePlatformId'] === $video['thePlatformId']) {
                    $videoPos = $index;
                    break;
                }
            }


        } else { //topic videos
            $topicsService = new Topics();
            $topics = $topicsService->getTopics();
            $videos = array();
            //find the hub where this video belongs, and return all videos in the same hub as related videos
            for ($i = 0; $i < count($topics); $i++) {
                $hasTheVideo = false;
                $hubVideos = $topics[$i]['videos'];
                for ($j = 0; $j < count($hubVideos); $j++) {
                    //print_r($hubVideos[$j]['title_ID']); 
                    if ( $hubVideos[$j]['title_ID'] === $video['thePlatformId'] ) {
                        $hasTheVideo = true;
                        $videoPos = $j;
                        break;
                    }
                }
                if ($hasTheVideo) {
                    foreach ($hubVideos as $hubVideo) {
                        $hubVideo['thePlatformId'] = $hubVideo['title_ID'];
                        $hubVideo['stillImageURL'] = $hubVideo['thumbnailImageURL'];
                        $hubVideo['network'] = 'History';
                        $hubVideo['isBehindWall'] = 'false';
                        array_push($videos, $hubVideo);                        
                    }
                    break;
                }
            }
        }

        $related = array();
        if (!isset($videoPos)) {
            if (isset($videos))
                $related = array_slice($videos, 0, 4);
        } else if (count($videos) < 5) {
            unset($videos[$videoPos]);
            $related = $videos;
        } else {
            
            if (count($videos) === $videoPos + 1) {
                array_push($related, $videos[$videoPos - 4], $videos[$videoPos - 3], $videos[$videoPos - 2], $videos[$videoPos - 1]);
            } else if (count($videos) === $videoPos + 2) {
                array_push($related, $videos[$videoPos - 3], $videos[$videoPos - 2], $videos[$videoPos - 1], $videos[$videoPos + 1]);
            } else if ($videoPos === 0) {
                array_push($related, $videos[$videoPos + 1], $videos[$videoPos + 2], $videos[$videoPos + 3], $videos[$videoPos + 4]);
            } else if ($videoPos === 1) {
                array_push($related, $videos[$videoPos - 1], $videos[$videoPos + 1], $videos[$videoPos + 2], $videos[$videoPos + 3]);
            } else {
                array_push($related, $videos[$videoPos - 2], $videos[$videoPos - 1], $videos[$videoPos + 1], $videos[$videoPos + 2]);
            }

        } 

        return $related;       

    }

    private function parseCuepoints($cuepoints) {
        if (strlen($cuepoints)  === 0)
            return array();

        $chapters = array();
        $chapters[0] = 0;
        $points = explode(',', $cuepoints);        
        foreach($points as $point) {
            $timeArray = explode(':', $point);
            $hour = (int)$timeArray[0];
            $minute = (int)$timeArray[1];
            $second = (int)$timeArray[2];
            $totalSeconds = $hour * 3600 + $minute * 60 + $second;
            $chapters[count($chapters)] = $totalSeconds; 

        }

        return $chapters;

    }

}